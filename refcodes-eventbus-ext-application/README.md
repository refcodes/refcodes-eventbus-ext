# README #

> The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers.

## What is this repository for? ##

***This artifact extends the [`refcodes-eventbus`](https://www.metacodes.pro/refcodes/refcodes-eventbus) toolkit with add-on functionality as of being provided by the [`refcodes-properties`](https://www.metacodes.pro/refcodes/refcodes-properties) toolkit, lifecycle-support and the like required for stand alone application development.***

## Getting started ##

> Please refer to the [refcodes-eventbus: Observer + Publish/Subscribe = Message broker](https://www.metacodes.pro/refcodes/refcodes-eventbus) documentation for an up-to-date and detailed description on the usage of this artifact. 

## How do I get set up? ##

To get up and running, include the following dependency (without the three dots "...") in your `pom.xml`:

```
<dependencies>
	...
	<dependency>
		<artifactId>refcodes-eventbus-ext-application</artifactId>
		<groupId>org.refcodes</groupId>
		<verseventbusn>1.3.1</verseventbusn>
	</dependency>
	...
</dependencies>
```

The artifact is hosted directly at [Maven Central](http://search.maven.org). Jump straight to the source codes at [Bitbucket](https://bitbucket.org/refcodes/refcodes-eventbus-ext/src/master/refcodes-eventbus-ext-application). Read the artifact's javadoc at [javadoc.eventbus](http://www.javadoc.eventbus/doc/org.refcodes/refcodes-eventbus-ext-application).

## Contribution guidelines ##

* Writing tests
* Code review
* Adding functionality
* Fixing bugs

## Who do I talk to? ##

* Siegfried Steiner (steiner@refcodes.org)

## Terms and conditions ##

The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) group of artifacts is published under some open source licenses; covered by the  [`refcodes-licensing`](https://bitbucket.org/refcodes/refcodes-licensing) ([`org.refcodes`](https://bitbucket.org/refcodes) group) artifact - evident in each artifact in question as of the `pom.xml` dependency included in such artifact.