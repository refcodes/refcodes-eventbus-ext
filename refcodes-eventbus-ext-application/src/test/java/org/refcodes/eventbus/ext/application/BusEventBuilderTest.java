// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.eventbus.ext.application;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.refcodes.properties.PropertiesBuilderImpl;
import org.refcodes.runtime.SystemProperty;

public class BusEventBuilderTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public enum Action {
		MESSAGE, EXCEPTION, PAYLOAD, PROPERTIES
	}

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private int _exceptionCount = 0;
	private int _payloadCount = 0;
	private int _messageCount = 0;
	private int _propertiesCount = 0;

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testBusEventBuilder1() {
		_exceptionCount = 0;
		_payloadCount = 0;
		_messageCount = 0;
		_propertiesCount = 0;
		final ApplicationBus theBus = new ApplicationBus();
		theBus.onException( this::onException );
		theBus.onPayload( String.class, this::onPayload );
		theBus.onMessage( this::onMessage );
		theBus.onProperties( this::onProperties );
		theBus.publishMessage().withAction( Action.MESSAGE ).withAlias( "MessageAlias" ).withChannel( "MessageChannel" ).withGroup( "MessageGroup" ).withMessage( "Hello world!" ).withPublisherType( BusEventBuilderTest.class ).withSource( theBus ).withUniversalId( "0" ).build();
		theBus.publishException().withAction( Action.EXCEPTION ).withAlias( "ExceptionAlias" ).withChannel( "ExceptionChannel" ).withGroup( "ExceptionGroup" ).withException( new RuntimeException( "Hello world!" ) ).withPublisherType( BusEventBuilderTest.class ).withSource( theBus ).withUniversalId( "1" ).build();
		theBus.publishProperties().withAction( Action.PROPERTIES ).withAlias( "PropertiesAlias" ).withChannel( "PropertiesChannel" ).withGroup( "PropertiesGroup" ).withProperties( new PropertiesBuilderImpl().withPut( "text", "Hello world!" ) ).withPublisherType( BusEventBuilderTest.class ).withSource( theBus ).withUniversalId( "2" ).build();
		theBus.publishPayload().withAction( Action.PAYLOAD ).withAlias( "PayloadAlias" ).withChannel( "PayloadChannel" ).withGroup( "PayloadGroup" ).withPayload( "Hello world!" ).withPublisherType( BusEventBuilderTest.class ).withSource( theBus ).withUniversalId( "3" ).build();
		assertEquals( 1, _exceptionCount );
		assertEquals( 1, _payloadCount );
		assertEquals( 1, _messageCount );
		assertEquals( 1, _propertiesCount );
	}

	@Test
	public void testBusEventBuilder2() {
		_exceptionCount = 0;
		_payloadCount = 0;
		_messageCount = 0;
		_propertiesCount = 0;
		final ApplicationBus theBus = new ApplicationBus();
		theBus.onException( this::onException );
		theBus.onPayload( String.class, this::onPayload );
		theBus.onMessage( this::onMessage );
		theBus.onProperties( this::onProperties );
		theBus.publishMessage().withAction( Action.MESSAGE ).withAlias( "MessageAlias" ).withChannel( "MessageChannel" ).withGroup( "MessageGroup" ).withMessage( "Hello world!" ).withPublisherType( BusEventBuilderTest.class ).withSource( theBus ).withUniversalId( "0" );
		theBus.publishException().withAction( Action.EXCEPTION ).withAlias( "ExceptionAlias" ).withChannel( "ExceptionChannel" ).withGroup( "ExceptionGroup" ).withException( new RuntimeException( "Hello world!" ) ).withPublisherType( BusEventBuilderTest.class ).withSource( theBus ).withUniversalId( "1" );
		theBus.publishProperties().withAction( Action.PROPERTIES ).withAlias( "PropertiesAlias" ).withChannel( "PropertiesChannel" ).withGroup( "PropertiesGroup" ).withProperties( new PropertiesBuilderImpl().withPut( "text", "Hello world!" ) ).withPublisherType( BusEventBuilderTest.class ).withSource( theBus ).withUniversalId( "2" );
		theBus.publishPayload().withAction( Action.PAYLOAD ).withAlias( "PayloadAlias" ).withChannel( "PayloadChannel" ).withGroup( "PayloadGroup" ).withPayload( "Hello world!" ).withPublisherType( BusEventBuilderTest.class ).withSource( theBus ).withUniversalId( "3" );
		assertEquals( 0, _exceptionCount );
		assertEquals( 0, _payloadCount );
		assertEquals( 0, _messageCount );
		assertEquals( 0, _propertiesCount );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	public void onMessage( MessageBusEvent aEvent ) {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( aEvent.getClass().getSimpleName() + ": " + aEvent.toString() );
		}
		assertEquals( Action.MESSAGE, aEvent.getAction() );
		assertNotNull( aEvent.getMetaData() );
		assertEquals( "MessageAlias", aEvent.getMetaData().getAlias() );
		assertEquals( "MessageChannel", aEvent.getMetaData().getChannel() );
		assertEquals( "MessageGroup", aEvent.getMetaData().getGroup() );
		assertEquals( "0", aEvent.getMetaData().getUniversalId() );
		assertEquals( "Hello world!", aEvent.getMessage() );
		assertEquals( BusEventBuilderTest.class, aEvent.getMetaData().getPublisherType() );
		assertNotNull( aEvent.getSource() );
		assertEquals( ApplicationBus.class, aEvent.getSource().getClass() );
		_messageCount++;
	}

	public void onException( ExceptionBusEvent aEvent ) {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( aEvent.getClass().getSimpleName() + ": " + aEvent.toString() );
		}
		assertEquals( Action.EXCEPTION, aEvent.getAction() );
		assertNotNull( aEvent.getMetaData() );
		assertEquals( "ExceptionAlias", aEvent.getMetaData().getAlias() );
		assertEquals( "ExceptionChannel", aEvent.getMetaData().getChannel() );
		assertEquals( "ExceptionGroup", aEvent.getMetaData().getGroup() );
		assertEquals( "1", aEvent.getMetaData().getUniversalId() );
		assertNotNull( aEvent.getException() );
		assertEquals( "Hello world!", aEvent.getException().getMessage() );
		assertEquals( BusEventBuilderTest.class, aEvent.getMetaData().getPublisherType() );
		assertNotNull( aEvent.getSource() );
		assertEquals( ApplicationBus.class, aEvent.getSource().getClass() );
		_exceptionCount++;
	}

	public void onProperties( PropertiesBusEvent aEvent ) {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( aEvent.getClass().getSimpleName() + ": " + aEvent.toString() );
		}
		assertEquals( Action.PROPERTIES, aEvent.getAction() );
		assertNotNull( aEvent.getMetaData() );
		assertEquals( "PropertiesAlias", aEvent.getMetaData().getAlias() );
		assertEquals( "PropertiesChannel", aEvent.getMetaData().getChannel() );
		assertEquals( "PropertiesGroup", aEvent.getMetaData().getGroup() );
		assertEquals( "2", aEvent.getMetaData().getUniversalId() );
		assertNotNull( aEvent.getProperties() );
		assertEquals( "Hello world!", aEvent.getProperties().get( "text" ) );
		assertEquals( BusEventBuilderTest.class, aEvent.getMetaData().getPublisherType() );
		assertNotNull( aEvent.getSource() );
		assertEquals( ApplicationBus.class, aEvent.getSource().getClass() );
		_propertiesCount++;
	}

	public void onPayload( PayloadBusEvent<String> aEvent ) {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( aEvent.getClass().getSimpleName() + ": " + aEvent.toString() );
		}
		assertEquals( Action.PAYLOAD, aEvent.getAction() );
		assertNotNull( aEvent.getMetaData() );
		assertEquals( "PayloadAlias", aEvent.getMetaData().getAlias() );
		assertEquals( "PayloadChannel", aEvent.getMetaData().getChannel() );
		assertEquals( "PayloadGroup", aEvent.getMetaData().getGroup() );
		assertEquals( "3", aEvent.getMetaData().getUniversalId() );
		assertEquals( "Hello world!", aEvent.getPayload() );
		assertEquals( BusEventBuilderTest.class, aEvent.getMetaData().getPublisherType() );
		assertNotNull( aEvent.getSource() );
		assertEquals( ApplicationBus.class, aEvent.getSource().getClass() );
		_payloadCount++;
	}
}
