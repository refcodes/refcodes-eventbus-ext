// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.eventbus.ext.application;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.refcodes.component.LifecycleException;
import org.refcodes.component.LifecycleStatus;
import org.refcodes.eventbus.DispatchStrategy;
import org.refcodes.runtime.SystemProperty;

public class ApplicationBusTest implements LifecycleBusObserver {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	private int _startCount = 0;
	private int _initCount = 0;
	private int _resumeCount = 0;
	private int _pauseCount = 0;
	private int _stopCount = 0;
	private int _destroyCount = 0;
	private int _exceptionCount = 0;
	private int _payloadCount = 0;

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testApplicationBus1() {
		final ApplicationBus theBus = new ApplicationBus();
		theBus.onStart( this::onStart );
		theBus.onException( this::onException );
		theBus.onException( LifecycleStatus.UNKNOWN, this::onException );
		theBus.onLifecycle( this );
		theBus.onPayload( this::onPayload );
		theBus.onPayload( "channel7", this::onPayload );
		theBus.publishInitialize();
		theBus.publishStart( DispatchStrategy.CASCADE );
		theBus.publishPause();
		theBus.publishResume( DispatchStrategy.CASCADE );
		theBus.publishStop();
		theBus.publishDestroy();
		assertEquals( 1, _initCount );
		assertEquals( 2, _startCount );
		assertEquals( 1, _pauseCount );
		assertEquals( 1, _resumeCount );
		assertEquals( 1, _stopCount );
		assertEquals( 1, _destroyCount );
		theBus.publishException( new RuntimeException() );
		theBus.publishException( LifecycleStatus.UNKNOWN, new RuntimeException() );
		assertEquals( 3, _exceptionCount );
		theBus.publishPayload( "Hello from channel 4" );
		theBus.publishPayload( "Hello from channel 7", "channel7" );
		assertEquals( 3, _payloadCount );
	}

	@Test
	public void testApplicationBus2() throws LifecycleException {
		final ApplicationBus theBus = new ApplicationBus();
		theBus.onStart( this::onStart );
		theBus.onException( this::onException );
		theBus.onException( LifecycleStatus.UNKNOWN, this::onException );
		theBus.onLifecycle( this );
		theBus.onPayload( this::onPayload );
		theBus.onPayload( "channel7", this::onPayload );
		theBus.initialize();
		theBus.start( DispatchStrategy.CASCADE );
		theBus.pause();
		theBus.resume( DispatchStrategy.CASCADE );
		theBus.stop();
		theBus.destroy();
		assertEquals( 1, _initCount );
		assertEquals( 2, _startCount );
		assertEquals( 1, _pauseCount );
		assertEquals( 1, _resumeCount );
		assertEquals( 1, _stopCount );
		assertEquals( 1, _destroyCount );
		theBus.publishException( new RuntimeException() );
		theBus.publishException( LifecycleStatus.UNKNOWN, new RuntimeException() );
		assertEquals( 3, _exceptionCount );
		theBus.publishPayload( "Hello from channel 4" );
		theBus.publishPayload( "Hello from channel 7", "channel7" );
		assertEquals( 3, _payloadCount );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	@Override
	public void onStart( StartBusEvent aEvent ) {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( aEvent.getClass().getSimpleName() + ": " + aEvent.toString() );
		}
		_startCount++;
	}

	@Override
	public void onInitialize( InitializeBusEvent aEvent ) {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( aEvent.getClass().getSimpleName() + ": " + aEvent.toString() );
		}
		_initCount++;
	}

	@Override
	public void onResume( ResumeBusEvent aEvent ) {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( aEvent.getClass().getSimpleName() + ": " + aEvent.toString() );
		}
		_resumeCount++;
	}

	@Override
	public void onPause( PauseBusEvent aEvent ) {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( aEvent.getClass().getSimpleName() + ": " + aEvent.toString() );
		}
		_pauseCount++;
	}

	@Override
	public void onStop( StopBusEvent aEvent ) {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( aEvent.getClass().getSimpleName() + ": " + aEvent.toString() );
		}
		_stopCount++;
	}

	@Override
	public void onDestroy( DestroyBusEvent aEvent ) {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( aEvent.getClass().getSimpleName() + ": " + aEvent.toString() );
		}
		_destroyCount++;
	}

	public void onException( ExceptionBusEvent aEvent ) {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( aEvent.getClass().getSimpleName() + ": " + aEvent.toString() );
		}
		_exceptionCount++;
	}

	public void onPayload( PayloadBusEvent<?> aEvent ) {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( aEvent.getClass().getSimpleName() + ": " + aEvent.toString() );
		}
		_payloadCount++;
	}
}
