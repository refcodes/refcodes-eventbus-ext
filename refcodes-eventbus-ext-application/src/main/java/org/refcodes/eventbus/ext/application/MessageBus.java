// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.eventbus.ext.application;

import static org.refcodes.eventbus.ext.application.ApplicationBusSugar.*;

import org.refcodes.eventbus.DispatchStrategy;
import org.refcodes.eventbus.EventDispatcher;
import org.refcodes.observer.EventMatcher;
import org.refcodes.observer.EventMetaData;
import org.refcodes.observer.Observable;
import org.refcodes.observer.Observer;

/**
 * The {@link MessageBus} extends the {@link ApplicationBus} with convenience
 * functionality common to everyday application and service development
 * regarding dispatching of message {@link String} instances.
 */
public interface MessageBus extends EventDispatcher<ApplicationBusEvent, Observer<ApplicationBusEvent>, ApplicationBusEventMatcher, EventMetaData, String> {

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link MessageBusEvent} instances with the given attributes.
	 *
	 * @param aPublisherType The type of the event publisher.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onMessage( Class<?> aPublisherType, Observer<MessageBusEvent> aObserver ) {
		return subscribe( MessageBusEvent.class, isPublisherTypeOf( aPublisherType ), aObserver );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link MessageBusEvent} instances with the given attributes.
	 *
	 * @param aAction The action property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onMessage( Enum<?> aAction, Class<?> aPublisherType, Observer<MessageBusEvent> aObserver ) {
		return subscribe( MessageBusEvent.class, and( actionEqualWith( aAction ), isPublisherTypeOf( aPublisherType ) ), aObserver );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link MessageBusEvent} instances with the given attributes. Your
	 * {@link Observable} may be of the required type!
	 *
	 * @param aAction The action property.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onMessage( Enum<?> aAction, Observer<MessageBusEvent> aObserver ) {
		return subscribe( MessageBusEvent.class, actionEqualWith( aAction ), aObserver );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link MessageBusEvent} instances with the given attributes.
	 *
	 * @param aAction The action property.
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onMessage( Enum<?> aAction, String aChannel, Observer<MessageBusEvent> aObserver ) {
		return subscribe( MessageBusEvent.class, and( actionEqualWith( aAction ), channelEqualWith( aChannel ) ), aObserver );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link MessageBusEvent} instances with the given attributes.
	 *
	 * @param aAction The action property.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onMessage( Enum<?> aAction, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, Observer<MessageBusEvent> aObserver ) {
		return subscribe( MessageBusEvent.class, and( actionEqualWith( aAction ), aliasEqualWith( aAlias ), groupEqualWith( aGroup ), channelEqualWith( aChannel ), uidIdEqualWith( aUid ), isPublisherTypeOf( aPublisherType ) ), aObserver );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link MessageBusEvent} instances.
	 *
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onMessage( Observer<MessageBusEvent> aObserver ) {
		return subscribe( MessageBusEvent.class, aObserver );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link MessageBusEvent} instances with the given attributes.
	 *
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onMessage( String aChannel, Observer<MessageBusEvent> aObserver ) {
		return subscribe( MessageBusEvent.class, channelEqualWith( aChannel ), aObserver );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link MessageBusEvent} instances with the given attributes.
	 *
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onMessage( String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, Observer<MessageBusEvent> aObserver ) {
		return subscribe( MessageBusEvent.class, and( aliasEqualWith( aAlias ), groupEqualWith( aGroup ), channelEqualWith( aChannel ), uidIdEqualWith( aUid ), isPublisherTypeOf( aPublisherType ) ), aObserver );
	}

	/**
	 * Fires an event with the properties set for the
	 * {@link MessageBusEvent.Builder} instance upon finishing of by invoking
	 * {@link MessageBusEvent.Builder#build()}.
	 * 
	 * @return The {@link MessageBusEvent} class' builder.
	 */
	default MessageBusEvent.Builder publishMessage() {
		MessageBusEvent.Builder theBuilder = new MessageBusEvent.Builder() {
			@Override
			public MessageBusEvent build() {
				MessageBusEvent theEvent = super.build();
				publishEvent( theEvent );
				return theEvent;
			}
		};
		return theBuilder;
	}

	/**
	 * Fires an event with the properties set for the
	 * {@link MessageBusEvent.Builder} instance upon finishing of by invoking
	 * {@link MessageBusEvent.Builder#build()}.
	 * 
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 * 
	 * @return The {@link MessageBusEvent} class' builder.
	 */
	default MessageBusEvent.Builder publishMessage( DispatchStrategy aStrategy ) {
		MessageBusEvent.Builder theBuilder = new MessageBusEvent.Builder() {
			@Override
			public MessageBusEvent build() {
				MessageBusEvent theEvent = super.build();
				publishEvent( theEvent, aStrategy );
				return theEvent;
			}
		};
		return theBuilder;
	}

	/**
	 * Publishes an event with the provided message and the given attributes.
	 * 
	 * @param aAction The action which the event represents.
	 * @param aMessage The message to be carried by the event.
	 */
	default void publishMessage( Enum<?> aAction, String aMessage ) {
		publishEvent( new MessageBusEvent( aAction, aMessage, getClass(), (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided message and the given attributes.
	 * 
	 * @param aAction The action which this represents.
	 * @param aMessage The message to be carried by the event.
	 * @param aPublisherType The type of the event publisher.
	 */
	default void publishMessage( Enum<?> aAction, String aMessage, Class<?> aPublisherType ) {
		publishEvent( new MessageBusEvent( aAction, aMessage, aPublisherType, (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided message and the given attributes.
	 * 
	 * @param aAction The action which this represents.
	 * @param aMessage The message to be carried by the event.
	 * @param aPublisherType The type of the event publisher.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default void publishMessage( Enum<?> aAction, String aMessage, Class<?> aPublisherType, DispatchStrategy aStrategy ) {
		publishEvent( new MessageBusEvent( aAction, aMessage, aPublisherType, (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided message and the given attributes.
	 * 
	 * @param aAction The action which the event represents.
	 * @param aMessage The message to be carried by the event.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default void publishMessage( Enum<?> aAction, String aMessage, DispatchStrategy aStrategy ) {
		publishEvent( new MessageBusEvent( aAction, aMessage, (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided message and the given attributes.
	 * 
	 * @param aAction The action which the event represents.
	 * @param aMessage The message to be carried by the event.
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 */
	default void publishMessage( Enum<?> aAction, String aMessage, EventMetaData aEventMetaData ) {
		publishEvent( new MessageBusEvent( aAction, aMessage, aEventMetaData, (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided message and the given attributes.
	 * 
	 * @param aAction The action which the event represents.
	 * @param aMessage The message to be carried by the event.
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default void publishMessage( Enum<?> aAction, String aMessage, EventMetaData aEventMetaData, DispatchStrategy aStrategy ) {
		publishEvent( new MessageBusEvent( aAction, aMessage, aEventMetaData, (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided message and the given attributes.
	 * 
	 * @param aAction The action which this represents.
	 * @param aMessage The message to be carried by the event.
	 * @param aChannel The channel name on which the event is receivable.
	 */
	default void publishMessage( Enum<?> aAction, String aMessage, String aChannel ) {
		publishEvent( new MessageBusEvent( aAction, aMessage, aChannel, getClass(), (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided message and the given attributes.
	 * 
	 * @param aAction The action which this represents.
	 * @param aMessage The message to be carried by the event.
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default void publishMessage( Enum<?> aAction, String aMessage, String aChannel, DispatchStrategy aStrategy ) {
		publishEvent( new MessageBusEvent( aAction, aMessage, aChannel, getClass(), (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided message for the given attributes.
	 * 
	 * @param aAction The action which this represents.
	 * @param aMessage The message to be carried by the event.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 */
	default void publishMessage( Enum<?> aAction, String aMessage, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType ) {
		publishEvent( new MessageBusEvent( aAction, aMessage, aAlias, aGroup, aChannel, aUid, aPublisherType, (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided message and the given attributes.
	 * 
	 * @param aAction The action which this represents.
	 * @param aMessage The message to be carried by the event.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default void publishMessage( Enum<?> aAction, String aMessage, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, DispatchStrategy aStrategy ) {
		publishEvent( new MessageBusEvent( aAction, aMessage, aAlias, aGroup, aChannel, aUid, aPublisherType, (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided message and the given attributes.
	 *
	 * @param aMessage The message to be carried by the event.
	 */
	default void publishMessage( String aMessage ) {
		publishEvent( new MessageBusEvent( aMessage, getClass(), (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided message and the given attributes.
	 * 
	 * @param aMessage The message to be carried by the event.
	 * @param aPublisherType The type of the event publisher.
	 */
	default void publishMessage( String aMessage, Class<?> aPublisherType ) {
		publishEvent( new MessageBusEvent( aMessage, aPublisherType, (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided message and the given attributes.
	 * 
	 * @param aMessage The message to be carried by the event.
	 * @param aPublisherType The type of the event publisher.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default void publishMessage( String aMessage, Class<?> aPublisherType, DispatchStrategy aStrategy ) {
		publishEvent( new MessageBusEvent( aMessage, aPublisherType, (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided message and the given attributes.
	 *
	 * @param aMessage The message to be carried by the event.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default void publishMessage( String aMessage, DispatchStrategy aStrategy ) {
		publishEvent( new MessageBusEvent( aMessage, getClass(), (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided message and the given attributes.
	 * 
	 * @param aMessage The message to be carried by the event.
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 */
	default void publishMessage( String aMessage, EventMetaData aEventMetaData ) {
		publishEvent( new MessageBusEvent( aMessage, aEventMetaData, (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided message and the given attributes.
	 * 
	 * @param aMessage The message to be carried by the event.
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default void publishMessage( String aMessage, EventMetaData aEventMetaData, DispatchStrategy aStrategy ) {
		publishEvent( new MessageBusEvent( aMessage, aEventMetaData, (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided message and the given attributes.
	 * 
	 * @param aMessage The message to be carried by the event.
	 * @param aChannel The channel name on which the event is receivable.
	 */
	default void publishMessage( String aMessage, String aChannel ) {
		publishEvent( new MessageBusEvent( aMessage, aChannel, getClass(), (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided message and the given attributes.
	 * 
	 * @param aMessage The message to be carried by the event.
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default void publishMessage( String aMessage, String aChannel, DispatchStrategy aStrategy ) {
		publishEvent( new MessageBusEvent( aMessage, aChannel, (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided message and the given attributes.
	 * 
	 * @param aMessage The message to be carried by the event.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 */
	default void publishMessage( String aMessage, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType ) {
		publishEvent( new MessageBusEvent( aMessage, aAlias, aGroup, aChannel, aUid, aPublisherType, (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided message and the given attributes.
	 * 
	 * @param aMessage The message to be carried by the event.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default void publishMessage( String aMessage, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, DispatchStrategy aStrategy ) {
		publishEvent( new MessageBusEvent( aMessage, aAlias, aGroup, aChannel, aUid, aPublisherType, (ApplicationBus) this ), aStrategy );
	}
}
