// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.eventbus.ext.application;

import static org.refcodes.eventbus.ext.application.ApplicationBusSugar.*;

import java.util.concurrent.ExecutorService;

import org.refcodes.component.HandleGeneratorImpl;
import org.refcodes.component.InitializeException;
import org.refcodes.component.LifecycleMachine;
import org.refcodes.component.LifecycleStatus;
import org.refcodes.component.PauseException;
import org.refcodes.component.ResumeException;
import org.refcodes.component.StartException;
import org.refcodes.component.StopException;
import org.refcodes.eventbus.AbstractEventBus;
import org.refcodes.eventbus.DispatchStrategy;
import org.refcodes.eventbus.EventDispatcher;
import org.refcodes.exception.BugException;
import org.refcodes.observer.EventMetaData;
import org.refcodes.observer.Observer;

/**
 * The {@link ApplicationBus} extends the {@link EventDispatcher} with
 * convenience functionality common to everyday application and service
 * development as defined by the interfaces {@link PayloadBus},
 * {@link PropertiesBus}, {@link MessageBus}, {@link ExceptionBus} as well as
 * {@link LifecycleBus} on top of the {@link EventDispatcher}. To ensure
 * controlled {@link LifecycleBusObserver} bootstrapping (subscribed via
 * {@link #onLifecycle(LifecycleBusObserver)} and the like), the
 * {@link InitializeBusEvent} instances fired upon calls to methods such as
 * {@link #publishInitialize(String)} (and the like) are (if not stated
 * otherwise) dispatched with the {@link DispatchStrategy#CASCADE} strategy.
 * Same applies to service "shutdown" as of {@link #publishPause()},
 * {@link #publishStop()} or {@link #publishDestroy()} (and the like) which are
 * also dispatched by default with the {@link DispatchStrategy#CASCADE}
 * strategy. This means that the observer methods
 * {@link LifecycleBusObserver#onInitialize(InitializeBusEvent)},
 * {@link LifecycleBusObserver#onPause(PauseBusEvent)},
 * {@link LifecycleBusObserver#onStop(StopBusEvent)} and
 * {@link LifecycleBusObserver#onDestroy(DestroyBusEvent)} must exit the
 * invoking thread as soon as possible so not to block succeeding listener
 * methods. As initialization is processed in a controlled manner (as mentioned
 * above), starting or resuming the {@link LifecycleBusObserver} instances is
 * done with the {@link DispatchStrategy#PARALLEL} as we enter normal operation.
 * This means that the observer methods
 * {@link LifecycleBusObserver#onStart(StartBusEvent)} and
 * {@link LifecycleBusObserver#onResume(ResumeBusEvent)} do not need to exit the
 * invoking thread as they cannot block succeeding listener methods.
 */
public class ApplicationBus extends AbstractEventBus<ApplicationBusEvent, Observer<ApplicationBusEvent>, ApplicationBusEventMatcher, EventMetaData, String> implements PayloadBus, PropertiesBus, MessageBus, ExceptionBus, LifecycleBus {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final LifecycleMachine _lifecycleAutomation = new LifecycleMachine();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs the {@link ApplicationBus} with the {@link DispatchStrategy}
	 * being {@link DispatchStrategy#CASCADE} when publishing events.
	 */
	public ApplicationBus() {
		super( new HandleGeneratorImpl() );
	}

	/**
	 * Constructs the {@link ApplicationBus} with the {@link DispatchStrategy}
	 * being {@link DispatchStrategy#CASCADE} when publishing events.
	 * 
	 * @param isDaemon True when to create daemon dispatch {@link Thread}
	 *        instances (shutdown upon last application {@link Thread}
	 *        shutdown), else application {@link Thread} instances are created
	 *        for dispatch.
	 */
	public ApplicationBus( boolean isDaemon ) {
		super( isDaemon, new HandleGeneratorImpl() );
	}

	/**
	 * Constructs the {@link ApplicationBus} with the given
	 * {@link DispatchStrategy} when publishing events.
	 * 
	 * @param aDispatchStrategy The {@link DispatchStrategy} to be used when
	 *        publishing events.
	 */
	public ApplicationBus( DispatchStrategy aDispatchStrategy ) {
		super( aDispatchStrategy, new HandleGeneratorImpl() );
	}

	/**
	 * Constructs the {@link ApplicationBus} with the given
	 * {@link DispatchStrategy} when publishing events.
	 * 
	 * @param aDispatchStrategy The {@link DispatchStrategy} to be used when
	 *        publishing events.
	 * 
	 * @param isDaemon True when to create daemon dispatch {@link Thread}
	 *        instances (shutdown upon last application {@link Thread}
	 *        shutdown), else application {@link Thread} instances are created
	 *        for dispatch.
	 */
	public ApplicationBus( DispatchStrategy aDispatchStrategy, boolean isDaemon ) {
		super( aDispatchStrategy, isDaemon, new HandleGeneratorImpl() );
	}

	/**
	 * Constructs the {@link ApplicationBus} with the {@link DispatchStrategy}
	 * being {@link DispatchStrategy#CASCADE} when publishing events.
	 * 
	 * @param aDispatchStrategy The {@link DispatchStrategy} to be used when
	 *        publishing events.
	 * @param aExecutorService The {@link ExecutorService} to be used when
	 *        creating threads.
	 */
	public ApplicationBus( DispatchStrategy aDispatchStrategy, ExecutorService aExecutorService ) {
		super( aDispatchStrategy, new HandleGeneratorImpl(), aExecutorService );
	}

	/**
	 * Constructs the {@link ApplicationBus} with the {@link DispatchStrategy}
	 * being {@link DispatchStrategy#CASCADE} when publishing events.
	 * 
	 * @param aExecutorService The {@link ExecutorService} to be used when
	 *        creating threads.
	 */
	public ApplicationBus( ExecutorService aExecutorService ) {
		super( new HandleGeneratorImpl(), aExecutorService );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 * 
	 * Destroys the {@link ApplicationBus} after having invoked
	 * {@link #publishDestroy()}.
	 */
	@Override
	public void destroy() {
		_lifecycleAutomation.destroy();
		publishDestroy();
		super.destroy();

	}

	/**
	 * Destroys the {@link ApplicationBus} after having invoked
	 * {@link #publishDestroy(DispatchStrategy)} similar to the
	 * {@link #destroy()} method, though using the provided
	 * {@link DispatchStrategy}.
	 * 
	 * @param aStrategy aStrategy The {@link DispatchStrategy} to use when
	 *        dispatching the according event.
	 */
	public void destroy( DispatchStrategy aStrategy ) {
		_lifecycleAutomation.destroy();
		publishDestroy( aStrategy );
		super.destroy();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public LifecycleStatus getLifecycleStatus() {
		return _lifecycleAutomation.getLifecycleStatus();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Initializes the {@link ApplicationBus} after having invoked
	 * {@link #publishInitialize()}.
	 */
	@Override
	public void initialize() throws InitializeException {
		_lifecycleAutomation.initialize();
		publishInitialize();
	}

	/**
	 * Initializes the {@link ApplicationBus} after having invoked
	 * {@link #publishInitialize(DispatchStrategy)} similar to the
	 * {@link #initialize()} method, though using the provided
	 * {@link DispatchStrategy}.
	 * 
	 * @param aStrategy aStrategy The {@link DispatchStrategy} to use when
	 *        dispatching the according event.
	 * 
	 * @throws InitializeException thrown in case initializing a component
	 *         caused problems.
	 */
	public void initialize( DispatchStrategy aStrategy ) throws InitializeException {
		_lifecycleAutomation.initialize();
		publishInitialize( aStrategy );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isDestroyable() {
		return _lifecycleAutomation.isDestroyable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isDestroyed() {
		return _lifecycleAutomation.isDestroyed();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isInitalizable() {
		return _lifecycleAutomation.isInitalizable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isInitialized() {
		return _lifecycleAutomation.isInitialized();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isPausable() {
		return _lifecycleAutomation.isPausable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isPaused() {
		return _lifecycleAutomation.isPaused();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isResumable() {
		return _lifecycleAutomation.isResumable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isRunning() {
		return _lifecycleAutomation.isRunning();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isStartable() {
		return _lifecycleAutomation.isStartable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isStoppable() {
		return _lifecycleAutomation.isStoppable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isStopped() {
		return _lifecycleAutomation.isStopped();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onAction( Class<ApplicationBusEvent> aEventType, Enum<?> aAction, Observer<ApplicationBusEvent> aObserver ) {
		return subscribe( and( isEventTypeOf( aEventType ), actionEqualWith( aAction ) ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onAction( Enum<?> aAction, Observer<ApplicationBusEvent> aObserver ) {
		return subscribe( actionEqualWith( aAction ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onActions( Class<ApplicationBusEvent> aEventType, Observer<ApplicationBusEvent> aObserver, Enum<?>... aActions ) {
		final ApplicationBusEventMatcher[] theEqualWith = new ApplicationBusEventMatcher[aActions.length];
		for ( int i = 0; i < theEqualWith.length; i++ ) {
			theEqualWith[i] = actionEqualWith( aActions[i] );
		}
		return subscribe( and( isEventTypeOf( aEventType ), or( theEqualWith ) ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onActions( Observer<ApplicationBusEvent> aObserver, Enum<?>... aActions ) {
		final ApplicationBusEventMatcher[] theEqualWith = new ApplicationBusEventMatcher[aActions.length];
		for ( int i = 0; i < theEqualWith.length; i++ ) {
			theEqualWith[i] = actionEqualWith( aActions[i] );
		}
		return subscribe( or( theEqualWith ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onAlias( Class<ApplicationBusEvent> aEventType, String aName, Observer<ApplicationBusEvent> aObserver ) {
		return subscribe( and( isEventTypeOf( aEventType ), aliasEqualWith( aName ) ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onAlias( String aName, Observer<ApplicationBusEvent> aObserver ) {
		return subscribe( aliasEqualWith( aName ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onCatchAll( Observer<ApplicationBusEvent> aObserver ) {
		return subscribe( catchAll(), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onChannel( Class<ApplicationBusEvent> aEventType, String aChannel, Observer<ApplicationBusEvent> aObserver ) {
		return subscribe( and( isEventTypeOf( aEventType ), channelEqualWith( aChannel ) ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onChannel( String aChannel, Observer<ApplicationBusEvent> aObserver ) {
		return subscribe( channelEqualWith( aChannel ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onEvent( Class<?> aPublisherType, Observer<ApplicationBusEvent> aObserver ) {
		return subscribe( isPublisherTypeOf( aPublisherType ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onEvent( Class<ApplicationBusEvent> aEventType, Class<?> aPublisherType, Observer<ApplicationBusEvent> aObserver ) {
		return subscribe( and( isEventTypeOf( aEventType ), isPublisherTypeOf( aPublisherType ) ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onEvent( Class<ApplicationBusEvent> aEventType, Enum<?> aAction, Class<?> aPublisherType, Observer<ApplicationBusEvent> aObserver ) {
		return subscribe( and( isEventTypeOf( aEventType ), actionEqualWith( aAction ), isPublisherTypeOf( aPublisherType ) ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onEvent( Class<ApplicationBusEvent> aEventType, Enum<?> aAction, Observer<ApplicationBusEvent> aObserver ) {
		return subscribe( and( isEventTypeOf( aEventType ), actionEqualWith( aAction ) ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onEvent( Class<ApplicationBusEvent> aEventType, Enum<?> aAction, String aChannel, Observer<ApplicationBusEvent> aObserver ) {
		return subscribe( and( isEventTypeOf( aEventType ), actionEqualWith( aAction ), channelEqualWith( aChannel ) ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onEvent( Class<ApplicationBusEvent> aEventType, Enum<?> aAction, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, Observer<ApplicationBusEvent> aObserver ) {
		return subscribe( and( isEventTypeOf( aEventType ), actionEqualWith( aAction ), aliasEqualWith( aAlias ), groupEqualWith( aGroup ), channelEqualWith( aChannel ), uidIdEqualWith( aUid ), isPublisherTypeOf( aPublisherType ) ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onEvent( Class<ApplicationBusEvent> aEventType, String aChannel, Observer<ApplicationBusEvent> aObserver ) {
		return subscribe( and( isEventTypeOf( aEventType ), channelEqualWith( aChannel ) ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onEvent( Class<ApplicationBusEvent> aEventType, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, Observer<ApplicationBusEvent> aObserver ) {
		return subscribe( and( isEventTypeOf( aEventType ), aliasEqualWith( aAlias ), groupEqualWith( aGroup ), channelEqualWith( aChannel ), uidIdEqualWith( aUid ), isPublisherTypeOf( aPublisherType ) ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onEvent( Enum<?> aAction, Class<?> aPublisherType, Observer<ApplicationBusEvent> aObserver ) {
		return subscribe( and( actionEqualWith( aAction ), isPublisherTypeOf( aPublisherType ) ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onEvent( Enum<?> aAction, Observer<ApplicationBusEvent> aObserver ) {
		return subscribe( actionEqualWith( aAction ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onEvent( Enum<?> aAction, String aChannel, Observer<ApplicationBusEvent> aObserver ) {
		return subscribe( and( actionEqualWith( aAction ), channelEqualWith( aChannel ) ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onEvent( Enum<?> aAction, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, Observer<ApplicationBusEvent> aObserver ) {
		return subscribe( and( actionEqualWith( aAction ), aliasEqualWith( aAlias ), groupEqualWith( aGroup ), channelEqualWith( aChannel ), uidIdEqualWith( aUid ), isPublisherTypeOf( aPublisherType ) ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onEvent( String aChannel, Observer<ApplicationBusEvent> aObserver ) {
		return subscribe( channelEqualWith( aChannel ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onEvent( String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, Observer<ApplicationBusEvent> aObserver ) {
		return subscribe( and( aliasEqualWith( aAlias ), groupEqualWith( aGroup ), channelEqualWith( aChannel ), uidIdEqualWith( aUid ), isPublisherTypeOf( aPublisherType ) ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onGroup( Class<ApplicationBusEvent> aEventType, String aGroup, Observer<ApplicationBusEvent> aObserver ) {
		return subscribe( and( isEventTypeOf( aEventType ), channelEqualWith( aGroup ) ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onGroup( String aGroup, Observer<ApplicationBusEvent> aObserver ) {
		return subscribe( channelEqualWith( aGroup ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <EVT extends ApplicationBusEvent> String onType( Class<EVT> aEventType, Observer<EVT> aObserver ) {
		return subscribe( isEventTypeOf( aEventType ), ( (Observer<ApplicationBusEvent>) aObserver ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onUniversalId( Class<ApplicationBusEvent> aEventType, String aUid, Observer<ApplicationBusEvent> aObserver ) {
		return subscribe( and( isEventTypeOf( aEventType ), channelEqualWith( aUid ) ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String onUniversalId( String aUid, Observer<ApplicationBusEvent> aObserver ) {
		return subscribe( channelEqualWith( aUid ), aObserver );
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Pauses the {@link ApplicationBus} after having invoked
	 * {@link #publishPause()}.
	 */
	@Override
	public void pause() throws PauseException {
		_lifecycleAutomation.pause();
		publishPause();
	}

	/**
	 * Pauses the {@link ApplicationBus} after having invoked
	 * {@link #publishPause(DispatchStrategy)} similar to the {@link #pause()}
	 * method, though using the provided {@link DispatchStrategy}.
	 * 
	 * @param aStrategy aStrategy The {@link DispatchStrategy} to use when
	 *        dispatching the according event.
	 * 
	 * @throws PauseException thrown in case pausing a component caused
	 *         problems.
	 */
	public void pause( DispatchStrategy aStrategy ) throws PauseException {
		_lifecycleAutomation.pause();
		publishPause( aStrategy );
	}

	/**
	 * Fires an event with the properties set for the
	 * {@link ApplicationBusEvent.Builder} instance upon finishing of by
	 * invoking {@link ApplicationBusEvent.Builder#build()}.
	 * 
	 * @return The {@link ApplicationBusEvent} class' builder.
	 */
	public ApplicationBusEvent.Builder publishEvent() {
		ApplicationBusEvent.Builder theBuilder = new ApplicationBusEvent.Builder() {
			@Override
			public ApplicationBusEvent build() {
				ApplicationBusEvent theEvent = super.build();
				publishEvent( theEvent );
				return theEvent;
			}
		};
		return theBuilder;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void publishEvent( Class<?> aPublisherType ) {
		publishEvent( new ApplicationBusEvent( aPublisherType, this ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void publishEvent( Class<?> aPublisherType, DispatchStrategy aStrategy ) {
		publishEvent( new ApplicationBusEvent( aPublisherType, this ), aStrategy );
	}

	/**
	 * Fires an event with the properties set for the
	 * {@link ApplicationBusEvent.Builder} instance upon finishing of by
	 * invoking {@link ApplicationBusEvent.Builder#build()}.
	 * 
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 * 
	 * @return The {@link ApplicationBusEvent} class' builder.
	 */
	public ApplicationBusEvent.Builder publishEvent( DispatchStrategy aStrategy ) {
		ApplicationBusEvent.Builder theBuilder = new ApplicationBusEvent.Builder() {
			@Override
			public ApplicationBusEvent build() {
				ApplicationBusEvent theEvent = super.build();
				publishEvent( theEvent, aStrategy );
				return theEvent;
			}
		};
		return theBuilder;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void publishEvent( Enum<?> aAction ) {
		publishEvent( new ApplicationBusEvent( aAction, this ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void publishEvent( Enum<?> aAction, Class<?> aPublisherType ) {
		publishEvent( new ApplicationBusEvent( aAction, aPublisherType, this ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void publishEvent( Enum<?> aAction, Class<?> aPublisherType, DispatchStrategy aStrategy ) {
		publishEvent( new ApplicationBusEvent( aAction, aPublisherType, this ), aStrategy );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void publishEvent( Enum<?> aAction, DispatchStrategy aStrategy ) {
		publishEvent( new ApplicationBusEvent( aAction, this ), aStrategy );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void publishEvent( Enum<?> aAction, EventMetaData aEventMetaData ) {
		publishEvent( new ApplicationBusEvent( aAction, aEventMetaData, this ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void publishEvent( Enum<?> aAction, EventMetaData aEventMetaData, DispatchStrategy aStrategy ) {
		publishEvent( new ApplicationBusEvent( aAction, aEventMetaData, this ), aStrategy );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void publishEvent( Enum<?> aAction, String aChannel ) {
		publishEvent( new ApplicationBusEvent( aAction, aChannel, this ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void publishEvent( Enum<?> aAction, String aChannel, Class<?> aPublisherType ) {
		publishEvent( new ApplicationBusEvent( aAction, aChannel, aPublisherType, this ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void publishEvent( Enum<?> aAction, String aChannel, DispatchStrategy aStrategy ) {
		publishEvent( new ApplicationBusEvent( aAction, aChannel, this ), aStrategy );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void publishEvent( Enum<?> aAction, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType ) {
		publishEvent( new ApplicationBusEvent( aAction, aAlias, aGroup, aChannel, aUid, aPublisherType, this ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void publishEvent( Enum<?> aAction, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, DispatchStrategy aStrategy ) {
		publishEvent( new ApplicationBusEvent( aAction, aAlias, aGroup, aChannel, aUid, aPublisherType, this ), aStrategy );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void publishEvent( EventMetaData aEventMetaData ) {
		publishEvent( new ApplicationBusEvent( aEventMetaData, this ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void publishEvent( EventMetaData aEventMetaData, DispatchStrategy aStrategy ) {
		publishEvent( new ApplicationBusEvent( aEventMetaData, this ), aStrategy );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void publishEvent( String aChannel ) {
		publishEvent( new ApplicationBusEvent( aChannel, this ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void publishEvent( String aChannel, DispatchStrategy aStrategy ) {
		publishEvent( new ApplicationBusEvent( aChannel, this ), aStrategy );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void publishEvent( String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType ) {
		publishEvent( new ApplicationBusEvent( aAlias, aGroup, aChannel, aUid, aPublisherType, this ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void publishEvent( String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, DispatchStrategy aStrategy ) {
		publishEvent( new ApplicationBusEvent( aAlias, aGroup, aChannel, aUid, aPublisherType, this ), aStrategy );
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Resumes the {@link ApplicationBus} after having invoked
	 * {@link #publishResume()}.
	 */
	@Override
	public void resume() throws ResumeException {
		_lifecycleAutomation.resume();
		publishResume();
	}

	/**
	 * Resumes the {@link ApplicationBus} after having invoked
	 * {@link #publishResume(DispatchStrategy)} similar to the {@link #pause()}
	 * method, though using the provided {@link DispatchStrategy}.
	 * 
	 * @param aStrategy aStrategy The {@link DispatchStrategy} to use when
	 *        dispatching the according event.
	 * 
	 * @throws ResumeException thrown in case pausing a component caused
	 *         problems.
	 */
	public void resume( DispatchStrategy aStrategy ) throws ResumeException {
		_lifecycleAutomation.resume();
		publishResume( aStrategy );
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Starts the {@link ApplicationBus} after having invoked
	 * {@link #publishStart()}.
	 */
	@Override
	public void start() throws StartException {
		_lifecycleAutomation.start();
		publishStart();
	}

	/**
	 * Starts the {@link ApplicationBus} after having invoked
	 * {@link #publishStart(DispatchStrategy)} similar to the {@link #pause()}
	 * method, though using the provided {@link DispatchStrategy}.
	 * 
	 * @param aStrategy aStrategy The {@link DispatchStrategy} to use when
	 *        dispatching the according event.
	 * 
	 * @throws StartException thrown in case pausing a component caused
	 *         problems.
	 */
	public void start( DispatchStrategy aStrategy ) throws StartException {
		_lifecycleAutomation.start();
		publishStart( aStrategy );
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Stops the {@link ApplicationBus} after having invoked
	 * {@link #publishStop()}.
	 */
	@Override
	public void stop() throws StopException {
		_lifecycleAutomation.stop();
		publishStop();
	}

	/**
	 * Stops the {@link ApplicationBus} after having invoked
	 * {@link #publishStop(DispatchStrategy)} similar to the {@link #pause()}
	 * method, though using the provided {@link DispatchStrategy}.
	 * 
	 * @param aStrategy aStrategy The {@link DispatchStrategy} to use when
	 *        dispatching the according event.
	 * 
	 * @throws StopException thrown in case pausing a component caused problems.
	 */
	public void stop( DispatchStrategy aStrategy ) throws StopException {
		_lifecycleAutomation.stop();
		publishStop( aStrategy );
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <EVT extends ApplicationBusEvent> String subscribe( Class<EVT> aEventType, ApplicationBusEventMatcher aEventMatcher, Observer<EVT> aObserver ) {
		return subscribe( and( isEventTypeOf( aEventType ), aEventMatcher ), (Observer<ApplicationBusEvent>) aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <EVT extends ApplicationBusEvent> String subscribe( Class<EVT> aEventType, Observer<EVT> aObserver ) {
		return subscribe( and( isEventTypeOf( aEventType ) ), (Observer<ApplicationBusEvent>) aObserver );
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Dispatcher for {@link LifecycleBusEvent} instance to the according
	 * methods of a {@link LifecycleBusObserver}.
	 */
	static class LifecycleBusDispatcher implements ApplicationBusObserver {

		// /////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////

		private final LifecycleBusObserver _observer;

		// /////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Instantiates a new lifecycle bus dispatcher.
		 *
		 * @param aObserver the observer
		 */
		public LifecycleBusDispatcher( LifecycleBusObserver aObserver ) {
			_observer = aObserver;
		}

		// /////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void onEvent( ApplicationBusEvent aEvent ) {
			if ( aEvent instanceof InitializeBusEvent ) {
				_observer.onInitialize( (InitializeBusEvent) aEvent );
			}
			else if ( aEvent instanceof StartBusEvent ) {
				_observer.onStart( (StartBusEvent) aEvent );
			}
			else if ( aEvent instanceof PauseBusEvent ) {
				_observer.onPause( (PauseBusEvent) aEvent );
			}
			else if ( aEvent instanceof ResumeBusEvent ) {
				_observer.onResume( (ResumeBusEvent) aEvent );
			}
			else if ( aEvent instanceof StopBusEvent ) {
				_observer.onStop( (StopBusEvent) aEvent );
			}
			else if ( aEvent instanceof DestroyBusEvent ) {
				_observer.onDestroy( (DestroyBusEvent) aEvent );
			}
			else {
				throw new BugException( getClass().getName() + ": Cannot dispatch the event of type <" + aEvent.getClass().getName() + ">." );
			}

		}
	}
}
