// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.eventbus.ext.application;

import static org.refcodes.eventbus.ext.application.ApplicationBusSugar.*;

import org.refcodes.component.LifecycleComponent.LifecycleAutomaton;
import org.refcodes.component.LifecycleStatus;
import org.refcodes.eventbus.DispatchStrategy;
import org.refcodes.eventbus.ext.application.ApplicationBus.LifecycleBusDispatcher;

/**
 * The {@link LifecycleBus} extends the {@link ApplicationBus} with convenience
 * functionality common to everyday application and service development
 * regarding dispatching of {@link LifecycleStatus} signals such as
 * {@link InitializeBusEvent}, {@link StartBusEvent}, {@link PauseBusEvent},
 * {@link ResumeBusEvent}, {@link StopBusEvent} or {@link DestroyBusEvent}
 * instances. To ensure controlled {@link LifecycleBusObserver} bootstrapping
 * (subscribed via {@link #onLifecycle(LifecycleBusObserver)} and the like), the
 * {@link InitializeBusEvent} instances fired upon calls to methods such as
 * {@link #publishInitialize(String)} (and the like) are (if not stated
 * otherwise) dispatched with the {@link DispatchStrategy#CASCADE} strategy.
 * Same applies to service "shutdown" as of {@link #publishPause()},
 * {@link #publishStop()} or {@link #publishDestroy()} (and the like) which are
 * also dispatched by default with the {@link DispatchStrategy#CASCADE}
 * strategy. This means that the observer methods
 * {@link LifecycleBusObserver#onInitialize(InitializeBusEvent)},
 * {@link LifecycleBusObserver#onPause(PauseBusEvent)},
 * {@link LifecycleBusObserver#onStop(StopBusEvent)} and
 * {@link LifecycleBusObserver#onDestroy(DestroyBusEvent)} must exit the
 * invoking thread as soon as possible so not to block succeeding listener
 * methods. As initialization is processed in a controlled manner (as mentioned
 * above), starting or resuming the {@link LifecycleBusObserver} instances is
 * done with the {@link DispatchStrategy#PARALLEL} as we enter normal operation.
 * This means that the observer methods
 * {@link LifecycleBusObserver#onStart(StartBusEvent)} and
 * {@link LifecycleBusObserver#onResume(ResumeBusEvent)} do not need to exit the
 * invoking thread as they cannot block succeeding listener methods.
 */
public interface LifecycleBus extends InitializeBus, StartBus, PauseBus, ResumeBus, StopBus, DestroyBus, LifecycleAutomaton {

	/**
	 * Subscribes a {@link LifecycleBusObserver} for {@link LifecycleBusEvent}
	 * instances to be passed to the according {@link LifecycleBusObserver}
	 * methods.
	 *
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onLifecycle( LifecycleBusObserver aObserver ) {
		return subscribe( isEventTypeOf( LifecycleBusEvent.class ), new LifecycleBusDispatcher( aObserver ) );
	}

	/**
	 * Subscribes a {@link LifecycleBusObserver} for {@link LifecycleBusEvent}
	 * instances with the given attributes to be passed to the according
	 * {@link LifecycleBusObserver} methods.
	 *
	 * @param aPublisherType The type of the event publisher.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onLifecycle( Class<?> aPublisherType, LifecycleBusObserver aObserver ) {
		return subscribe( and( isEventTypeOf( LifecycleBusEvent.class ), isPublisherTypeOf( aPublisherType ) ), new LifecycleBusDispatcher( aObserver ) );
	}

	/**
	 * Subscribes a {@link LifecycleBusObserver} for {@link LifecycleBusEvent}
	 * instances with the given attributes to be passed to the according
	 * {@link LifecycleBusObserver} methods.
	 *
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onLifecycle( String aChannel, LifecycleBusObserver aObserver ) {
		return subscribe( and( isEventTypeOf( LifecycleBusEvent.class ), channelEqualWith( aChannel ) ), new LifecycleBusDispatcher( aObserver ) );
	}

	/**
	 * Subscribes a {@link LifecycleBusObserver} for {@link LifecycleBusEvent}
	 * instances with the given attributes to be passed to the according
	 * {@link LifecycleBusObserver} methods.
	 *
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onLifecycle( String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, LifecycleBusObserver aObserver ) {
		return subscribe( and( isEventTypeOf( LifecycleBusEvent.class ), aliasEqualWith( aAlias ), groupEqualWith( aGroup ), channelEqualWith( aChannel ), uidIdEqualWith( aUid ), isPublisherTypeOf( aPublisherType ) ), new LifecycleBusDispatcher( aObserver ) );
	}

	/**
	 * Subscribes a {@link LifecycleBusObserver} for {@link LifecycleBusEvent}
	 * instances with the given attributes to be passed to the according
	 * {@link LifecycleBusObserver} methods.
	 *
	 * @param aAction The action which this represents.
	 * @param aPublisherType The type of the event publisher.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onLifecycle( Enum<?> aAction, Class<?> aPublisherType, LifecycleBusObserver aObserver ) {
		return subscribe( and( isEventTypeOf( LifecycleBusEvent.class ), actionEqualWith( aAction ), isPublisherTypeOf( aPublisherType ) ), new LifecycleBusDispatcher( aObserver ) );
	}

	/**
	 * Subscribes a {@link LifecycleBusObserver} for {@link LifecycleBusEvent}
	 * instances with the given attributes to be passed to the according
	 * {@link LifecycleBusObserver} methods.
	 *
	 * @param aAction The action which this represents.
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onLifecycle( Enum<?> aAction, String aChannel, LifecycleBusObserver aObserver ) {
		return subscribe( and( isEventTypeOf( LifecycleBusEvent.class ), actionEqualWith( aAction ), channelEqualWith( aChannel ) ), new LifecycleBusDispatcher( aObserver ) );
	}

	/**
	 * Subscribes a {@link LifecycleBusObserver} for {@link LifecycleBusEvent}
	 * instances with the given attributes to be passed to the according
	 * {@link LifecycleBusObserver} methods.
	 *
	 * @param aAction The action which this represents.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onLifecycle( Enum<?> aAction, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, LifecycleBusObserver aObserver ) {
		return subscribe( and( isEventTypeOf( LifecycleBusEvent.class ), actionEqualWith( aAction ), aliasEqualWith( aAlias ), groupEqualWith( aGroup ), channelEqualWith( aChannel ), uidIdEqualWith( aUid ), isPublisherTypeOf( aPublisherType ) ), new LifecycleBusDispatcher( aObserver ) );
	}

	/**
	 * Subscribes a {@link LifecycleBusObserver} for {@link LifecycleBusEvent}
	 * instances with the given attributes to be passed to the according
	 * {@link LifecycleBusObserver} methods.
	 *
	 * @param aAction The action which this represents.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onLifecycle( Enum<?> aAction, LifecycleBusObserver aObserver ) {
		return subscribe( and( isEventTypeOf( LifecycleBusEvent.class ), actionEqualWith( aAction ) ), new LifecycleBusDispatcher( aObserver ) );
	}
}
