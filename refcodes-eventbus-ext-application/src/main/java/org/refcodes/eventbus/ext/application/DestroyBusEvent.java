// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.eventbus.ext.application;

import org.refcodes.component.LifecycleRequest;
import org.refcodes.component.ext.observer.DestroyRequestedEvent;
import org.refcodes.observer.EventMetaData;

/**
 * Implementation of the {@link LifecycleBusEvent} for
 * {@link LifecycleRequest#DESTROY}.
 */
public class DestroyBusEvent extends ApplicationBusEvent implements LifecycleBusEvent, DestroyRequestedEvent<ApplicationBus> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private DestroyBusEvent( Builder builder ) {
		this( builder.eventMetaData.build(), builder.source );
	}

	/**
	 * Instantiates a new destroy event.
	 *
	 * @param aEventMetaData the event Meta-Data
	 * @param aSource The according source (origin).
	 */
	public DestroyBusEvent( EventMetaData aEventMetaData, ApplicationBus aSource ) {
		super( LifecycleRequest.DESTROY, aEventMetaData, aSource );
	}

	/**
	 * Instantiates a new destroy event.
	 *
	 * @param aSource The according source (origin).
	 */
	public DestroyBusEvent( ApplicationBus aSource ) {
		super( LifecycleRequest.DESTROY, aSource );
	}

	/**
	 * Instantiates a new destroy event.
	 * 
	 * @param aChannel The channel for the {@link EventMetaData}.
	 * @param aSource The according source (origin).
	 */
	public DestroyBusEvent( String aChannel, ApplicationBus aSource ) {
		super( LifecycleRequest.DESTROY, new EventMetaData( aChannel ), aSource );
	}

	/**
	 * Instantiates a new destroy event.
	 * 
	 * @param aPublisherType The publisher type for the {@link EventMetaData}.
	 * @param aSource The according source (origin).
	 */
	public DestroyBusEvent( Class<?> aPublisherType, ApplicationBus aSource ) {
		super( LifecycleRequest.DESTROY, new EventMetaData( aPublisherType ), aSource );
	}

	/**
	 * Instantiates a new destroy event.
	 * 
	 * @param aAlias The alias for the {@link EventMetaData}.
	 * @param aGroup The group for the {@link EventMetaData}.
	 * @param aChannel The channel for the {@link EventMetaData}.
	 * @param aUid The Universal-TID for the {@link EventMetaData}.
	 * @param aPublisherType The publisher type for the {@link EventMetaData}.
	 * @param aSource The according source (origin).
	 */
	public DestroyBusEvent( String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, ApplicationBus aSource ) {
		super( LifecycleRequest.DESTROY, new EventMetaData( aAlias, aGroup, aChannel, aUid, aPublisherType ), aSource );
	}

	/**
	 * Instantiates a new destroy event.
	 * 
	 * @param aChannel The channel for the {@link EventMetaData}.
	 * @param aPublisherType The publisher type for the {@link EventMetaData}.
	 * @param aSource The according source (origin).
	 */
	public DestroyBusEvent( String aChannel, Class<?> aPublisherType, ApplicationBus aSource ) {
		super( LifecycleRequest.DESTROY, new EventMetaData( aChannel, aPublisherType ), aSource );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public LifecycleRequest getAction() {
		return (LifecycleRequest) super.getAction();
	}

	/**
	 * Creates builder to build {@link DestroyBusEvent}.
	 * 
	 * @return created builder
	 */
	public static Builder builder() {
		return new Builder();
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Builder to build {@link DestroyBusEvent}.
	 */
	public static final class Builder extends ApplicationBusEvent.Builder {

		private Builder() {}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withAction( Enum<?> aAction ) {
			action = aAction;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withSource( ApplicationBus aSource ) {
			source = aSource;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withAlias( String aAlias ) {
			eventMetaData.withAlias( aAlias );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withChannel( String aChannel ) {
			eventMetaData.withChannel( aChannel );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withGroup( String aGroup ) {
			eventMetaData.withGroup( aGroup );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withUniversalId( String aUid ) {
			eventMetaData.withUniversalId( aUid );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withPublisherType( Class<?> aPublisherType ) {
			eventMetaData.withPublisherType( aPublisherType );
			return this;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * Merges all not-null values of the provided {@link EventMetaData}
		 * instance into this {@link Builder} instance.
		 * 
		 * @param aEventMetaData The {@link EventMetaData} instance to be merged
		 *        into this {@link Builder} instance.
		 * 
		 * @return This {@link Builder} instance as of the builder pattern.
		 */
		@Override
		public Builder withMetaData( EventMetaData aEventMetaData ) {
			eventMetaData.withMetaData( aEventMetaData );
			return this;
		}

		/**
		 * Builder method of the builder.
		 * 
		 * @return The built instance.
		 */
		@Override
		public DestroyBusEvent build() {
			return new DestroyBusEvent( this );
		}
	}
}
