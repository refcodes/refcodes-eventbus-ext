// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.eventbus.ext.application;

import org.refcodes.eventbus.EventBrokerEvent;
import org.refcodes.mixin.AliasAccessor.AliasBuilder;
import org.refcodes.mixin.ChannelAccessor.ChannelBuilder;
import org.refcodes.mixin.GroupAccessor.GroupBuilder;
import org.refcodes.mixin.UniversalIdAccessor.UniversalIdBuilder;
import org.refcodes.observer.AbstractMetaDataActionEvent;
import org.refcodes.observer.EventMetaData;
import org.refcodes.observer.PublisherTypeAccessor.PublisherTypeBuilder;

/**
 * Intuitive Meta-Interface for the {@link ApplicationBusEvent} as used by the
 * {@link ApplicationBus}.
 */
public class ApplicationBusEvent extends AbstractMetaDataActionEvent<Enum<?>, EventMetaData, ApplicationBus> implements EventBrokerEvent<ApplicationBus> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private ApplicationBusEvent( Builder builder ) {
		super( builder.action, builder.eventMetaData.build(), builder.source );
	}

	/**
	 * Constructs an event with predefined values for the according properties
	 * retrieved from the caller's class.
	 *
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source of this event.
	 */
	public ApplicationBusEvent( Class<?> aPublisherType, ApplicationBus aSource ) {
		super( new EventMetaData( aPublisherType ), aSource );
	}

	/**
	 * Constructs an event with predefined values for the according properties
	 * retrieved from the caller's class.
	 *
	 * @param aAction The action associated to this event.
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source of this event.
	 */
	public ApplicationBusEvent( Enum<?> aAction, Class<?> aPublisherType, ApplicationBus aSource ) {
		super( aAction, new EventMetaData( aPublisherType ), aSource );
	}

	/**
	 * Constructs an event with the given Meta-Data.
	 *
	 * @param aAction The action associated to this event.
	 * @param aEventMetaData The event's meta data.
	 * @param aSource The source of this event.
	 */
	public ApplicationBusEvent( Enum<?> aAction, EventMetaData aEventMetaData, ApplicationBus aSource ) {
		super( aAction, aEventMetaData, aSource );
	}

	/**
	 * Constructs an event with the given Meta-Data.
	 *
	 * @param aAction The action associated to this event.
	 * @param aSource The source of this event.
	 */
	public ApplicationBusEvent( Enum<?> aAction, ApplicationBus aSource ) {
		super( aAction, aSource );
	}

	/**
	 * Constructs an event with predefined values for the according properties
	 * retrieved from the caller's class.
	 *
	 * @param aAction The action associated to this event.
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aSource The source of this event.
	 */
	public ApplicationBusEvent( Enum<?> aAction, String aChannel, ApplicationBus aSource ) {
		super( aAction, new EventMetaData( aChannel ), aSource );
	}

	/**
	 * Constructs an event with predefined values for the according properties
	 * retrieved from the caller's class.
	 *
	 * @param aAction The action associated to this event.
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aPublisherType The publisher type for the {@link EventMetaData}.
	 * @param aSource The source of this event.
	 */
	public ApplicationBusEvent( Enum<?> aAction, String aChannel, Class<?> aPublisherType, ApplicationBus aSource ) {
		super( aAction, new EventMetaData( aChannel, aPublisherType ), aSource );
	}

	/**
	 * Constructs an event with the given values for the according properties.
	 *
	 * @param aAction The action associated to this event.
	 * @param aAlias The alias for this event.
	 * @param aGroup The group this event belongs to.
	 * @param aChannel The channel this event is broadcasted on.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source of this event.
	 */
	public ApplicationBusEvent( Enum<?> aAction, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, ApplicationBus aSource ) {
		super( aAction, new EventMetaData( aAlias, aGroup, aChannel, aUid, aPublisherType ), aSource );
	}

	/**
	 * Constructs an event with the given Meta-Data.
	 *
	 * @param aEventMetaData The event's meta data.
	 * @param aSource The source of this event.
	 */
	public ApplicationBusEvent( EventMetaData aEventMetaData, ApplicationBus aSource ) {
		super( aEventMetaData, aSource );
	}

	/**
	 * Constructs an event with the given Meta-Data.
	 *
	 * @param aSource The source of this event.
	 */
	public ApplicationBusEvent( ApplicationBus aSource ) {
		super( aSource );
	}

	/**
	 * Constructs an event with predefined values for the according properties
	 * retrieved from the caller's class.
	 *
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aSource The source of this event.
	 */
	public ApplicationBusEvent( String aChannel, ApplicationBus aSource ) {
		super( new EventMetaData( aChannel ), aSource );
	}

	/**
	 * Constructs an event with the given values for the according properties.
	 *
	 * @param aAlias The alias for this event.
	 * @param aGroup The group this event belongs to.
	 * @param aChannel The channel this event is broadcasted on.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source of this event.
	 */
	public ApplicationBusEvent( String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, ApplicationBus aSource ) {
		super( new EventMetaData( aAlias, aGroup, aChannel, aUid, aPublisherType ), aSource );
	}

	//	/**
	//	 * Creates builder to build {@link ApplicationBusEvent}.
	//	 * 
	//	 * @return created builder
	//	 */
	//	public static Builder builder() {
	//		return new Builder();
	//	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Builder to build {@link ApplicationBusEvent}.
	 */
	public static class Builder implements ActionBuilder<Enum<?>, Builder>, MetaDataBuilder<EventMetaData, Builder>, SourceBuilder<ApplicationBus, Builder>, AliasBuilder<Builder>, ChannelBuilder<Builder>, GroupBuilder<Builder>, UniversalIdBuilder<Builder>, PublisherTypeBuilder<Builder> {

		protected Enum<?> action;
		protected final EventMetaData.Builder eventMetaData = EventMetaData.builder();
		protected ApplicationBus source;

		/**
		 * Instantiates a new builder.
		 */
		protected Builder() {}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withAction( Enum<?> aAction ) {
			action = aAction;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withSource( ApplicationBus aSource ) {
			source = aSource;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withAlias( String aAlias ) {
			eventMetaData.withAlias( aAlias );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withChannel( String aChannel ) {
			eventMetaData.withChannel( aChannel );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withGroup( String aGroup ) {
			eventMetaData.withGroup( aGroup );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withUniversalId( String aUid ) {
			eventMetaData.withUniversalId( aUid );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withPublisherType( Class<?> aPublisherType ) {
			eventMetaData.withPublisherType( aPublisherType );
			return this;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * Merges all not-null values of the provided {@link EventMetaData}
		 * instance into this {@link Builder} instance.
		 * 
		 * @param aEventMetaData The {@link EventMetaData} instance to be merged
		 *        into this {@link Builder} instance.
		 * 
		 * @return This {@link Builder} instance as of the builder pattern.
		 */
		@Override
		public Builder withMetaData( EventMetaData aEventMetaData ) {
			eventMetaData.withMetaData( aEventMetaData );
			return this;
		}

		/**
		 * Builder method of the builder.
		 * 
		 * @return The built instance.
		 */
		public ApplicationBusEvent build() {
			return new ApplicationBusEvent( this );
		}
	}
}
