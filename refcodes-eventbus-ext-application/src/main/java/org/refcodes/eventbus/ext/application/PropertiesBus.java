// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.eventbus.ext.application;

import static org.refcodes.eventbus.ext.application.ApplicationBusSugar.*;

import org.refcodes.eventbus.DispatchStrategy;
import org.refcodes.eventbus.EventDispatcher;
import org.refcodes.observer.EventMatcher;
import org.refcodes.observer.EventMetaData;
import org.refcodes.observer.Observable;
import org.refcodes.observer.Observer;
import org.refcodes.properties.Properties;

/**
 * The {@link PropertiesBus} extends the {@link ApplicationBus} with convenience
 * functionality common to everyday application and service development
 * regarding dispatching of {@link Properties} instances.
 */
public interface PropertiesBus extends EventDispatcher<ApplicationBusEvent, Observer<ApplicationBusEvent>, ApplicationBusEventMatcher, EventMetaData, String> {

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link PropertiesBusEvent} instances with the given attributes.
	 *
	 * @param aPublisherType The type of the event publisher.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onProperties( Class<?> aPublisherType, Observer<PropertiesBusEvent> aObserver ) {
		return subscribe( PropertiesBusEvent.class, isPublisherTypeOf( aPublisherType ), aObserver );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link PropertiesBusEvent} instances with the given attributes.
	 *
	 * @param aAction The action property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onProperties( Enum<?> aAction, Class<?> aPublisherType, Observer<PropertiesBusEvent> aObserver ) {
		return subscribe( PropertiesBusEvent.class, and( actionEqualWith( aAction ), isPublisherTypeOf( aPublisherType ) ), aObserver );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link PropertiesBusEvent} instances with the given attributes. Your
	 * {@link Observable} may be of the required type!
	 *
	 * @param aAction The action property.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onProperties( Enum<?> aAction, Observer<PropertiesBusEvent> aObserver ) {
		return subscribe( PropertiesBusEvent.class, actionEqualWith( aAction ), aObserver );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link PropertiesBusEvent} instances with the given attributes.
	 *
	 * @param aAction The action property.
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onProperties( Enum<?> aAction, String aChannel, Observer<PropertiesBusEvent> aObserver ) {
		return subscribe( PropertiesBusEvent.class, and( actionEqualWith( aAction ), channelEqualWith( aChannel ) ), aObserver );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link PropertiesBusEvent} instances with the given attributes.
	 *
	 * @param aAction The action property.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onProperties( Enum<?> aAction, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, Observer<PropertiesBusEvent> aObserver ) {
		return subscribe( PropertiesBusEvent.class, and( actionEqualWith( aAction ), aliasEqualWith( aAlias ), groupEqualWith( aGroup ), channelEqualWith( aChannel ), uidIdEqualWith( aUid ), isPublisherTypeOf( aPublisherType ) ), aObserver );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link PropertiesBusEvent} instances.
	 *
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onProperties( Observer<PropertiesBusEvent> aObserver ) {
		return subscribe( PropertiesBusEvent.class, aObserver );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link PropertiesBusEvent} instances with the given attributes.
	 *
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onProperties( String aChannel, Observer<PropertiesBusEvent> aObserver ) {
		return subscribe( PropertiesBusEvent.class, channelEqualWith( aChannel ), aObserver );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link PropertiesBusEvent} instances with the given attributes.
	 *
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onProperties( String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, Observer<PropertiesBusEvent> aObserver ) {
		return subscribe( PropertiesBusEvent.class, and( aliasEqualWith( aAlias ), groupEqualWith( aGroup ), channelEqualWith( aChannel ), uidIdEqualWith( aUid ), isPublisherTypeOf( aPublisherType ) ), aObserver );
	}

	/**
	 * Fires an event with the properties set for the
	 * {@link PropertiesBusEvent.Builder} instance upon finishing of by invoking
	 * {@link PropertiesBusEvent.Builder#build()}.
	 * 
	 * @return The {@link PropertiesBusEvent} class' builder.
	 */
	default PropertiesBusEvent.Builder publishProperties() {
		PropertiesBusEvent.Builder theBuilder = new PropertiesBusEvent.Builder() {
			@Override
			public PropertiesBusEvent build() {
				PropertiesBusEvent theEvent = super.build();
				publishEvent( theEvent );
				return theEvent;
			}
		};
		return theBuilder;
	}

	/**
	 * Fires an event with the properties set for the
	 * {@link PropertiesBusEvent.Builder} instance upon finishing of by invoking
	 * {@link PropertiesBusEvent.Builder#build()}.
	 * 
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 * 
	 * @return The {@link PropertiesBusEvent} class' builder.
	 */
	default PropertiesBusEvent.Builder publishProperties( DispatchStrategy aStrategy ) {
		PropertiesBusEvent.Builder theBuilder = new PropertiesBusEvent.Builder() {
			@Override
			public PropertiesBusEvent build() {
				PropertiesBusEvent theEvent = super.build();
				publishEvent( theEvent, aStrategy );
				return theEvent;
			}
		};
		return theBuilder;
	}

	/**
	 * Publishes an event with the provided properties and the given attributes.
	 * 
	 * @param aAction The action which the event represents.
	 * @param aProperties The properties to be carried by the event.
	 */
	default void publishProperties( Enum<?> aAction, Properties aProperties ) {
		publishEvent( new PropertiesBusEvent( aAction, aProperties, getClass(), (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided properties and the given attributes.
	 * 
	 * @param aAction The action which this represents.
	 * @param aProperties The properties to be carried by the event.
	 * @param aPublisherType The type of the event publisher.
	 */
	default void publishProperties( Enum<?> aAction, Properties aProperties, Class<?> aPublisherType ) {
		publishEvent( new PropertiesBusEvent( aAction, aProperties, aPublisherType, (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided properties and the given attributes.
	 * 
	 * @param aAction The action which this represents.
	 * @param aProperties The properties to be carried by the event.
	 * @param aPublisherType The type of the event publisher.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default void publishProperties( Enum<?> aAction, Properties aProperties, Class<?> aPublisherType, DispatchStrategy aStrategy ) {
		publishEvent( new PropertiesBusEvent( aAction, aProperties, aPublisherType, (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided properties and the given attributes.
	 * 
	 * @param aAction The action which the event represents.
	 * @param aProperties The properties to be carried by the event.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default void publishProperties( Enum<?> aAction, Properties aProperties, DispatchStrategy aStrategy ) {
		publishEvent( new PropertiesBusEvent( aAction, aProperties, getClass(), (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided properties and the given attributes.
	 * 
	 * @param aAction The action which the event represents.
	 * @param aProperties The properties to be carried by the event.
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 */
	default void publishProperties( Enum<?> aAction, Properties aProperties, EventMetaData aEventMetaData ) {
		publishEvent( new PropertiesBusEvent( aAction, aProperties, aEventMetaData, (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided properties and the given attributes.
	 * 
	 * @param aAction The action which the event represents.
	 * @param aProperties The properties to be carried by the event.
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default void publishProperties( Enum<?> aAction, Properties aProperties, EventMetaData aEventMetaData, DispatchStrategy aStrategy ) {
		publishEvent( new PropertiesBusEvent( aAction, aProperties, aEventMetaData, (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided properties and the given attributes.
	 * 
	 * @param aAction The action which this represents.
	 * @param aProperties The properties to be carried by the event.
	 * @param aChannel The channel name on which the event is receivable.
	 */
	default void publishProperties( Enum<?> aAction, Properties aProperties, String aChannel ) {
		publishEvent( new PropertiesBusEvent( aAction, aProperties, aChannel, getClass(), (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided properties and the given attributes.
	 * 
	 * @param aAction The action which this represents.
	 * @param aProperties The properties to be carried by the event.
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default void publishProperties( Enum<?> aAction, Properties aProperties, String aChannel, DispatchStrategy aStrategy ) {
		publishEvent( new PropertiesBusEvent( aAction, aProperties, aChannel, getClass(), (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided properties for the given attributes.
	 * 
	 * @param aAction The action which this represents.
	 * @param aProperties The properties to be carried by the event.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 */
	default void publishProperties( Enum<?> aAction, Properties aProperties, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType ) {
		publishEvent( new PropertiesBusEvent( aAction, aProperties, aAlias, aGroup, aChannel, aUid, aPublisherType, (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided properties and the given attributes.
	 * 
	 * @param aAction The action which this represents.
	 * @param aProperties The properties to be carried by the event.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default void publishProperties( Enum<?> aAction, Properties aProperties, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, DispatchStrategy aStrategy ) {
		publishEvent( new PropertiesBusEvent( aAction, aProperties, aAlias, aGroup, aChannel, aUid, aPublisherType, (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided properties and the given attributes.
	 *
	 * @param aProperties The properties to be carried by the event.
	 */
	default void publishProperties( Properties aProperties ) {
		publishEvent( new PropertiesBusEvent( aProperties, getClass(), (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided properties and the given attributes.
	 * 
	 * @param aProperties The properties to be carried by the event.
	 * @param aPublisherType The type of the event publisher.
	 */
	default void publishProperties( Properties aProperties, Class<?> aPublisherType ) {
		publishEvent( new PropertiesBusEvent( aProperties, aPublisherType, (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided properties and the given attributes.
	 * 
	 * @param aProperties The properties to be carried by the event.
	 * @param aPublisherType The type of the event publisher.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default void publishProperties( Properties aProperties, Class<?> aPublisherType, DispatchStrategy aStrategy ) {
		publishEvent( new PropertiesBusEvent( aProperties, aPublisherType, (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided properties and the given attributes.
	 *
	 * @param aProperties The properties to be carried by the event.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default void publishProperties( Properties aProperties, DispatchStrategy aStrategy ) {
		publishEvent( new PropertiesBusEvent( aProperties, getClass(), (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided properties and the given attributes.
	 * 
	 * @param aProperties The properties to be carried by the event.
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 */
	default void publishProperties( Properties aProperties, EventMetaData aEventMetaData ) {
		publishEvent( new PropertiesBusEvent( aProperties, aEventMetaData, (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided properties and the given attributes.
	 * 
	 * @param aProperties The properties to be carried by the event.
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default void publishProperties( Properties aProperties, EventMetaData aEventMetaData, DispatchStrategy aStrategy ) {
		publishEvent( new PropertiesBusEvent( aProperties, aEventMetaData, (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided properties and the given attributes.
	 * 
	 * @param aProperties The properties to be carried by the event.
	 * @param aChannel The channel name on which the event is receivable.
	 */
	default void publishProperties( Properties aProperties, String aChannel ) {
		publishEvent( new PropertiesBusEvent( aProperties, aChannel, getClass(), (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided properties and the given attributes.
	 * 
	 * @param aProperties The properties to be carried by the event.
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default void publishProperties( Properties aProperties, String aChannel, DispatchStrategy aStrategy ) {
		publishEvent( new PropertiesBusEvent( aProperties, aChannel, getClass(), (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided properties and the given attributes.
	 * 
	 * @param aProperties The properties to be carried by the event.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 */
	default void publishProperties( Properties aProperties, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType ) {
		publishEvent( new PropertiesBusEvent( aProperties, aAlias, aGroup, aChannel, aUid, aPublisherType, (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided properties and the given attributes.
	 * 
	 * @param aProperties The properties to be carried by the event.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default void publishProperties( Properties aProperties, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, DispatchStrategy aStrategy ) {
		publishEvent( new PropertiesBusEvent( aProperties, aAlias, aGroup, aChannel, aUid, aPublisherType, (ApplicationBus) this ), aStrategy );
	}
}
