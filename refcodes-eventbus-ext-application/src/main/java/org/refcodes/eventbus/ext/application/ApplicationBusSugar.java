// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.eventbus.ext.application;

import org.refcodes.eventbus.DispatchStrategy;
import org.refcodes.matcher.Matcher;
import org.refcodes.matcher.MatcherSchema;
import org.refcodes.matcher.MatcherSugar;
import org.refcodes.observer.AbstractEventMatcher;
import org.refcodes.observer.ActionEqualWithEventMatcher;
import org.refcodes.observer.ActionEvent;
import org.refcodes.observer.AliasEqualWithEventMatcher;
import org.refcodes.observer.CatchAllEventMatcher;
import org.refcodes.observer.CatchNoneEventMatcher;
import org.refcodes.observer.ChannelEqualWithEventMatcher;
import org.refcodes.observer.EventMetaData;
import org.refcodes.observer.GroupEqualWithEventMatcher;
import org.refcodes.observer.MetaDataEvent;
import org.refcodes.observer.PublisherTypeOfEventMatcher;
import org.refcodes.observer.UniversalIdEqualWithEventMatcher;
import org.refcodes.properties.Properties;

/**
 * Declarative syntactic sugar which may be statically imported in order to
 * allow declarative definitions for the {@link ApplicationBusEventMatcher}
 * elements.
 */
public class ApplicationBusSugar {

	private ApplicationBusSugar() {}

	/**
	 * Constructs the {@link ApplicationBus} with the {@link DispatchStrategy}
	 * as of {@link ApplicationBus#ApplicationBus()}.
	 * 
	 * @return The accordingly configured {@link ApplicationBus}.
	 */
	public static ApplicationBus applicationBus() {
		return new ApplicationBus();
	}

	/**
	 * Constructs the {@link ApplicationBus} with the {@link DispatchStrategy}
	 * as of {@link ApplicationBus#ApplicationBus(boolean)}.
	 * 
	 * @param isDaemon True when to create daemon dispatch {@link Thread}
	 *        instances (shutdown upon last application {@link Thread}
	 *        shutdown), else application {@link Thread} instances are created
	 *        for dispatch.
	 * 
	 * @return The accordingly configured {@link ApplicationBus}.
	 */
	public static ApplicationBus applicationBus( boolean isDaemon ) {
		return new ApplicationBus( isDaemon );
	}

	/**
	 * Constructs the {@link ApplicationBus} with the
	 * {@link DispatchStrategy#PARALLEL} when publishing events:
	 * 
	 * Each matching observer is invoked in its own thread. No observer can
	 * block your invoking thread.
	 * 
	 * @return The accordingly configured {@link ApplicationBus}.
	 */
	public static ApplicationBus parallelDispatchBus() {
		return new ApplicationBus( DispatchStrategy.PARALLEL );
	}

	/**
	 * Constructs the {@link ApplicationBus} with the
	 * {@link DispatchStrategy#PARALLEL} when publishing events:
	 * 
	 * Each matching observer is invoked in its own thread. No observer can
	 * block your invoking thread.
	 * 
	 * @param isDaemon True when to create daemon dispatch {@link Thread}
	 *        instances (shutdown upon last application {@link Thread}
	 *        shutdown), else application {@link Thread} instances are created
	 *        for dispatch.
	 * 
	 * @return The accordingly configured {@link ApplicationBus}.
	 */
	public static ApplicationBus parallelDispatchBus( boolean isDaemon ) {
		return new ApplicationBus( DispatchStrategy.PARALLEL, isDaemon );
	}

	/**
	 * Constructs the {@link ApplicationBus} with the
	 * {@link DispatchStrategy#SEQUENTIAL} when publishing events:
	 * 
	 * The parent (invoker) thread is used to publish the parent's event as well
	 * as the child events published by the matching observers of the parent
	 * event (and so on, in case them use {@link DispatchStrategy#SEQUENTIAL} as
	 * well). Any observer (directly or indirectly) invoked by your invoking
	 * thread can block your invoking thread.
	 * 
	 * @return The accordingly configured {@link ApplicationBus}.
	 */
	public static ApplicationBus sequentialDispatchBus() {
		return new ApplicationBus( DispatchStrategy.SEQUENTIAL );
	}

	/**
	 * Constructs the {@link ApplicationBus} with the
	 * {@link DispatchStrategy#SEQUENTIAL} when publishing events:
	 * 
	 * The parent (invoker) thread is used to publish the parent's event as well
	 * as the child events published by the matching observers of the parent
	 * event (and so on, in case them use {@link DispatchStrategy#SEQUENTIAL} as
	 * well). Any observer (directly or indirectly) invoked by your invoking
	 * thread can block your invoking thread.
	 * 
	 * @param isDaemon True when to create daemon dispatch {@link Thread}
	 *        instances (shutdown upon last application {@link Thread}
	 *        shutdown), else application {@link Thread} instances are created
	 *        for dispatch.
	 * 
	 * @return The accordingly configured {@link ApplicationBus}.
	 */
	public static ApplicationBus sequentialDispatchBus( boolean isDaemon ) {
		return new ApplicationBus( DispatchStrategy.SEQUENTIAL, isDaemon );
	}

	/**
	 * Constructs the {@link ApplicationBus} with the
	 * {@link DispatchStrategy#ASYNC} when publishing events:
	 * 
	 * Same as the {@link DispatchStrategy#SEQUENTIAL} approach with the
	 * difference that the sequential dispatch process is done asynchronously,
	 * freeing your parent's thread immediately after publishing your parent
	 * event. Exactly one extra thread is created to kick off the asynchronous
	 * way of doing a sequential dispatch. Any observer (directly or indirectly)
	 * invoked by the "asynchronous" thread can block any other observer in that
	 * chain, but not your invoking thread.
	 * 
	 * @return The accordingly configured {@link ApplicationBus}.
	 */
	public static ApplicationBus asyncDispatchBus() {
		return new ApplicationBus( DispatchStrategy.ASYNC );
	}

	/**
	 * Constructs the {@link ApplicationBus} with the
	 * {@link DispatchStrategy#ASYNC} when publishing events:
	 * 
	 * Same as the {@link DispatchStrategy#SEQUENTIAL} approach with the
	 * difference that the sequential dispatch process is done asynchronously,
	 * freeing your parent's thread immediately after publishing your parent
	 * event. Exactly one extra thread is created to kick off the asynchronous
	 * way of doing a sequential dispatch. Any observer (directly or indirectly)
	 * invoked by the "asynchronous" thread can block any other observer in that
	 * chain, but not your invoking thread.
	 * 
	 * @param isDaemon True when to create daemon dispatch {@link Thread}
	 *        instances (shutdown upon last application {@link Thread}
	 *        shutdown), else application {@link Thread} instances are created
	 *        for dispatch.
	 * 
	 * @return The accordingly configured {@link ApplicationBus}.
	 */
	public static ApplicationBus asyncDispatchBus( boolean isDaemon ) {
		return new ApplicationBus( DispatchStrategy.ASYNC, isDaemon );
	}

	/**
	 * Constructs the {@link ApplicationBus} with the
	 * {@link DispatchStrategy#CASCADE} when publishing events:
	 * 
	 * The parent (invoker) thread is used to publish the parent's event to all
	 * matching observers (and is blocked till done). Child events published by
	 * the matching observers invoked with the parent's event are queued until
	 * the parent's thread finished dispatching the parent's event. The queued
	 * child events then are published in their own separate threads, now
	 * considered being parent events with their according parent threads, to
	 * all matching observers (dispatching as described above). The
	 * {@link DispatchStrategy#CASCADE} strategy is useful when publishing
	 * lifecycle or bootstrapping events to make sure, that any observer was
	 * notified before publishing post-lifecycle actions. Observers directly
	 * invoked by your invoking thread can block your invoking thread and
	 * indirectly invoked observers called by your directly invoked observers
	 * using the {@link DispatchStrategy#SEQUENTIAL} strategy for publishing
	 * their events.
	 * 
	 * @return The accordingly configured {@link ApplicationBus}.
	 */
	public static ApplicationBus cascadeDispatchBus() {
		return new ApplicationBus( DispatchStrategy.CASCADE );
	}

	/**
	 * Constructs the {@link ApplicationBus} with the
	 * {@link DispatchStrategy#CASCADE} when publishing events:
	 * 
	 * The parent (invoker) thread is used to publish the parent's event to all
	 * matching observers (and is blocked till done). Child events published by
	 * the matching observers invoked with the parent's event are queued until
	 * the parent's thread finished dispatching the parent's event. The queued
	 * child events then are published in their own separate threads, now
	 * considered being parent events with their according parent threads, to
	 * all matching observers (dispatching as described above). The
	 * {@link DispatchStrategy#CASCADE} strategy is useful when publishing
	 * lifecycle or bootstrapping events to make sure, that any observer was
	 * notified before publishing post-lifecycle actions. Observers directly
	 * invoked by your invoking thread can block your invoking thread and
	 * indirectly invoked observers called by your directly invoked observers
	 * using the {@link DispatchStrategy#SEQUENTIAL} strategy for publishing
	 * their events.
	 * 
	 * @param isDaemon True when to create daemon dispatch {@link Thread}
	 *        instances (shutdown upon last application {@link Thread}
	 *        shutdown), else application {@link Thread} instances are created
	 *        for dispatch.
	 * 
	 * @return The accordingly configured {@link ApplicationBus}.
	 */
	public static ApplicationBus cascadeDispatchBus( boolean isDaemon ) {
		return new ApplicationBus( DispatchStrategy.CASCADE, isDaemon );
	}

	/**
	 * Constructs a builder for an {@link ApplicationBusEvent} instance.
	 *
	 * @return The accordingly created {@link ApplicationBusEvent.Builder}
	 *         instance.
	 */
	public static ApplicationBusEvent.Builder applicationBusEventBuilder() {
		return new ApplicationBusEvent.Builder();
	}

	/**
	 * Constructs an {@link ApplicationBusEvent} event with predefined values
	 * for the according properties retrieved from the caller's class.
	 * 
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link ApplicationBusEvent} instance.
	 */
	public static ApplicationBusEvent applicationBusEvent( Class<?> aPublisherType, ApplicationBus aSource ) {
		return new ApplicationBusEvent( new EventMetaData( aPublisherType ), aSource );
	}

	/**
	 * Constructs an {@link ApplicationBusEvent} event with predefined values
	 * for the according properties retrieved from the caller's class.
	 * 
	 * @param aAction The action which this represents.
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link ApplicationBusEvent} instance.
	 */
	public static ApplicationBusEvent applicationBusEvent( Enum<?> aAction, Class<?> aPublisherType, ApplicationBus aSource ) {
		return new ApplicationBusEvent( aAction, new EventMetaData( aPublisherType ), aSource );
	}

	/**
	 * Constructs an {@link ApplicationBusEvent} event with the given Meta-Data.
	 * 
	 * @param aAction The action which this represents.
	 * @param aEventMetaData The Meta-Data to be supplied by the event.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link ApplicationBusEvent} instance.
	 */
	public static ApplicationBusEvent applicationBusEvent( Enum<?> aAction, EventMetaData aEventMetaData, ApplicationBus aSource ) {
		return new ApplicationBusEvent( aAction, aEventMetaData, aSource );
	}

	/**
	 * Constructs an {@link ApplicationBusEvent} event with the given Meta-Data.
	 * 
	 * @param aAction The action which this represents.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link ApplicationBusEvent} instance.
	 */
	public static ApplicationBusEvent applicationBusEvent( Enum<?> aAction, ApplicationBus aSource ) {
		return new ApplicationBusEvent( aAction, aSource );
	}

	/**
	 * Constructs an {@link ApplicationBusEvent} event with predefined values
	 * for the according properties retrieved from the caller's class.
	 * 
	 * @param aAction The action which this represents.
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link ApplicationBusEvent} instance.
	 */
	public static ApplicationBusEvent applicationBusEvent( Enum<?> aAction, String aChannel, ApplicationBus aSource ) {
		return new ApplicationBusEvent( aAction, new EventMetaData( aChannel ), aSource );
	}

	/**
	 * Constructs an {@link ApplicationBusEvent} event with the given values for
	 * the according properties.
	 * 
	 * @param aAction The action which this represents.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link ApplicationBusEvent} instance.
	 */
	public static ApplicationBusEvent applicationBusEvent( Enum<?> aAction, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, ApplicationBus aSource ) {
		return new ApplicationBusEvent( aAction, new EventMetaData( aAlias, aGroup, aChannel, aUid, aPublisherType ), aSource );
	}

	/**
	 * Constructs an {@link ApplicationBusEvent} event with the given Meta-Data.
	 * 
	 * @param aEventMetaData The Meta-Data to be supplied by the event.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link ApplicationBusEvent} instance.
	 */
	public static ApplicationBusEvent applicationBusEvent( EventMetaData aEventMetaData, ApplicationBus aSource ) {
		return new ApplicationBusEvent( aEventMetaData, aSource );
	}

	/**
	 * Constructs an {@link ApplicationBusEvent} event with the given Meta-Data.
	 *
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link ApplicationBusEvent} instance.
	 */
	public static ApplicationBusEvent applicationBusEvent( ApplicationBus aSource ) {
		return new ApplicationBusEvent( aSource );
	}

	/**
	 * Constructs an {@link ApplicationBusEvent} event with predefined values
	 * for the according properties retrieved from the caller's class.
	 * 
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link ApplicationBusEvent} instance.
	 */
	public static ApplicationBusEvent applicationBusEvent( String aChannel, ApplicationBus aSource ) {
		return new ApplicationBusEvent( aChannel, aSource );
	}

	/**
	 * Constructs an {@link ApplicationBusEvent} event with the given values for
	 * the according properties.
	 * 
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link ApplicationBusEvent} instance.
	 */
	public static ApplicationBusEvent applicationBusEvent( String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, ApplicationBus aSource ) {
		return new ApplicationBusEvent( new EventMetaData( aAlias, aGroup, aChannel, aUid, aPublisherType ), aSource );
	}

	/**
	 * Constructs a builder for an {@link ExceptionBusEvent} instance.
	 *
	 * @return The accordingly created {@link ExceptionBusEvent.Builder}
	 *         instance.
	 */
	public static ExceptionBusEvent.Builder exceptionBusEventBuilder() {
		return ExceptionBusEvent.builder();
	}

	/**
	 * Constructs an {@link ExceptionBusEvent} event with predefined values for
	 * the according properties retrieved from the caller's class.
	 * 
	 * @param aException The exception to carry.
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link ExceptionBusEvent} instance.
	 */
	public static ExceptionBusEvent exceptionBusEvent( Throwable aException, Class<?> aPublisherType, ApplicationBus aSource ) {
		return new ExceptionBusEvent( aException, aPublisherType, aSource );
	}

	/**
	 * Constructs an {@link ExceptionBusEvent} event with predefined values for
	 * the according properties retrieved from the caller's class.
	 * 
	 * @param aAction The action which this represents.
	 * @param aException The exception to carry.
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link ExceptionBusEvent} instance.
	 */
	public static ExceptionBusEvent exceptionBusEvent( Enum<?> aAction, Throwable aException, Class<?> aPublisherType, ApplicationBus aSource ) {
		return new ExceptionBusEvent( aAction, aException, aPublisherType, aSource );
	}

	/**
	 * Constructs an {@link ExceptionBusEvent} event with the given Meta-Data.
	 * 
	 * @param aAction The action which this represents.
	 * @param aException The exception to carry.
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link ExceptionBusEvent} instance.
	 */
	public static ExceptionBusEvent exceptionBusEvent( Enum<?> aAction, Throwable aException, EventMetaData aEventMetaData, ApplicationBus aSource ) {
		return new ExceptionBusEvent( aAction, aException, aEventMetaData, aSource );
	}

	/**
	 * Constructs an {@link ExceptionBusEvent} event with the given Meta-Data.
	 * 
	 * @param aAction The action which this represents.
	 * @param aException The exception to carry.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link ExceptionBusEvent} instance.
	 */
	public static ExceptionBusEvent exceptionBusEvent( Enum<?> aAction, Throwable aException, ApplicationBus aSource ) {
		return new ExceptionBusEvent( aAction, aException, aSource );
	}

	/**
	 * Constructs an {@link ExceptionBusEvent} event with predefined values for
	 * the according properties retrieved from the caller's class.
	 * 
	 * @param aAction The action which this represents.
	 * @param aException The exception to carry.
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link ExceptionBusEvent} instance.
	 */
	public static ExceptionBusEvent exceptionBusEvent( Enum<?> aAction, Throwable aException, String aChannel, ApplicationBus aSource ) {
		return new ExceptionBusEvent( aAction, aException, aChannel, aSource );
	}

	/**
	 * Constructs an {@link ExceptionBusEvent} event with the given values for
	 * the according properties.
	 * 
	 * @param aAction The action which this represents.
	 * @param aException The exception to carry.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link ExceptionBusEvent} instance.
	 */
	public static ExceptionBusEvent exceptionBusEvent( Enum<?> aAction, Throwable aException, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, ApplicationBus aSource ) {
		return new ExceptionBusEvent( aAction, aException, aAlias, aGroup, aChannel, aUid, aPublisherType, aSource );
	}

	/**
	 * Constructs an {@link ExceptionBusEvent} event with the given Meta-Data.
	 * 
	 * @param aException The exception to carry.
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link ExceptionBusEvent} instance.
	 */
	public static ExceptionBusEvent exceptionBusEvent( Throwable aException, EventMetaData aEventMetaData, ApplicationBus aSource ) {
		return new ExceptionBusEvent( aException, aEventMetaData, aSource );
	}

	/**
	 * Constructs an {@link ExceptionBusEvent} event with the given Meta-Data.
	 *
	 * @param aException The exception to carry.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link ExceptionBusEvent} instance.
	 */
	public static ExceptionBusEvent exceptionBusEvent( Throwable aException, ApplicationBus aSource ) {
		return new ExceptionBusEvent( aException, aSource );
	}

	/**
	 * Constructs an {@link ExceptionBusEvent} event with predefined values for
	 * the according properties retrieved from the caller's class.
	 * 
	 * @param aException The exception to carry.
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link ExceptionBusEvent} instance.
	 */
	public static ExceptionBusEvent exceptionBusEvent( Throwable aException, String aChannel, ApplicationBus aSource ) {
		return new ExceptionBusEvent( aException, aChannel, aSource );
	}

	/**
	 * Constructs an {@link ExceptionBusEvent} event with the given values for
	 * the according properties.
	 * 
	 * @param aException The exception to carry.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link ExceptionBusEvent} instance.
	 */
	public static ExceptionBusEvent exceptionBusEvent( Throwable aException, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, ApplicationBus aSource ) {
		return new ExceptionBusEvent( aException, aAlias, aGroup, aChannel, aUid, aPublisherType, aSource );
	}

	/**
	 * Constructs an {@link ExceptionBusEvent} event with the given values for
	 * the according properties.
	 * 
	 * @param aException The exception to carry.
	 * @param aChannel The channel for the {@link EventMetaData}.
	 * @param aPublisherType The publisher type for the {@link EventMetaData}.
	 * @param aSource The according source (origin).
	 *
	 * @return The accordingly created {@link ExceptionBusEvent} instance.
	 */
	public static ExceptionBusEvent exceptionBusEvent( Throwable aException, String aChannel, Class<?> aPublisherType, ApplicationBus aSource ) {
		return new ExceptionBusEvent( aException, new EventMetaData( aChannel, aPublisherType ), aSource );
	}

	/**
	 * Constructs an {@link ExceptionBusEvent} event with the given values for
	 * the according properties.
	 * 
	 * @param aAction The action which this represents.
	 * @param aException The exception to carry.
	 * @param aChannel The channel property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link ExceptionBusEvent} instance.
	 */
	public static ExceptionBusEvent exceptionBusEvent( Enum<?> aAction, Throwable aException, String aChannel, Class<?> aPublisherType, ApplicationBus aSource ) {
		return new ExceptionBusEvent( aAction, aException, new EventMetaData( aChannel, aPublisherType ), aSource );
	}

	/**
	 * Constructs a builder for an {@link MessageBusEvent} instance.
	 *
	 * @return The accordingly created {@link MessageBusEvent.Builder} instance.
	 */
	public static MessageBusEvent.Builder messageBusEventBuilder() {
		return MessageBusEvent.builder();
	}

	/**
	 * Constructs an {@link MessageBusEvent} event with predefined values for
	 * the according properties retrieved from the caller's class.
	 * 
	 * @param aMessage The message to carry.
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link MessageBusEvent} instance.
	 */
	public static MessageBusEvent messageBusEvent( String aMessage, Class<?> aPublisherType, ApplicationBus aSource ) {
		return new MessageBusEvent( aMessage, aPublisherType, aSource );
	}

	/**
	 * Constructs an {@link MessageBusEvent} event with predefined values for
	 * the according properties retrieved from the caller's class.
	 * 
	 * @param aAction The action which this represents.
	 * @param aMessage The message to carry.
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link MessageBusEvent} instance.
	 */
	public static MessageBusEvent messageBusEvent( Enum<?> aAction, String aMessage, Class<?> aPublisherType, ApplicationBus aSource ) {
		return new MessageBusEvent( aAction, aMessage, aPublisherType, aSource );
	}

	/**
	 * Constructs an {@link MessageBusEvent} event with the given Meta-Data.
	 * 
	 * @param aAction The action which this represents.
	 * @param aMessage The message to carry.
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link MessageBusEvent} instance.
	 */
	public static MessageBusEvent messageBusEvent( Enum<?> aAction, String aMessage, EventMetaData aEventMetaData, ApplicationBus aSource ) {
		return new MessageBusEvent( aAction, aMessage, aEventMetaData, aSource );
	}

	/**
	 * Constructs an {@link MessageBusEvent} event with the given Meta-Data.
	 * 
	 * @param aAction The action which this represents.
	 * @param aMessage The message to carry.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link MessageBusEvent} instance.
	 */
	public static MessageBusEvent messageBusEvent( Enum<?> aAction, String aMessage, ApplicationBus aSource ) {
		return new MessageBusEvent( aAction, aMessage, aSource );
	}

	/**
	 * Constructs an {@link MessageBusEvent} event with predefined values for
	 * the according properties retrieved from the caller's class.
	 * 
	 * @param aAction The action which this represents.
	 * @param aMessage The message to carry.
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link MessageBusEvent} instance.
	 */
	public static MessageBusEvent messageBusEvent( Enum<?> aAction, String aMessage, String aChannel, ApplicationBus aSource ) {
		return new MessageBusEvent( aAction, aMessage, aChannel, aSource );
	}

	/**
	 * Constructs an {@link MessageBusEvent} event with the given values for the
	 * according properties.
	 * 
	 * @param aAction The action which this represents.
	 * @param aMessage The message to carry.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link MessageBusEvent} instance.
	 */
	public static MessageBusEvent messageBusEvent( Enum<?> aAction, String aMessage, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, ApplicationBus aSource ) {
		return new MessageBusEvent( aAction, aMessage, aAlias, aGroup, aChannel, aUid, aPublisherType, aSource );
	}

	/**
	 * Constructs an {@link MessageBusEvent} event with the given Meta-Data.
	 * 
	 * @param aMessage The message to carry.
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link MessageBusEvent} instance.
	 */
	public static MessageBusEvent messageBusEvent( String aMessage, EventMetaData aEventMetaData, ApplicationBus aSource ) {
		return new MessageBusEvent( aMessage, aEventMetaData, aSource );
	}

	/**
	 * Constructs an {@link MessageBusEvent} event with the given Meta-Data.
	 *
	 * @param aMessage The message to carry.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link MessageBusEvent} instance.
	 */
	public static MessageBusEvent messageBusEvent( String aMessage, ApplicationBus aSource ) {
		return new MessageBusEvent( aMessage, aSource );
	}

	/**
	 * Constructs an {@link MessageBusEvent} event with predefined values for
	 * the according properties retrieved from the caller's class.
	 * 
	 * @param aMessage The message to carry.
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link MessageBusEvent} instance.
	 */
	public static MessageBusEvent messageBusEvent( String aMessage, String aChannel, ApplicationBus aSource ) {
		return new MessageBusEvent( aMessage, aChannel, aSource );
	}

	/**
	 * Constructs an {@link MessageBusEvent} event with the given values for the
	 * according properties.
	 * 
	 * @param aMessage The message to carry.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link MessageBusEvent} instance.
	 */
	public static MessageBusEvent messageBusEvent( String aMessage, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, ApplicationBus aSource ) {
		return new MessageBusEvent( aMessage, aAlias, aGroup, aChannel, aUid, aPublisherType, aSource );
	}

	/**
	 * Constructs an {@link MessageBusEvent} event with the given values for the
	 * according properties.
	 * 
	 * @param aMessage The message to carry.
	 * @param aChannel The channel for the {@link EventMetaData}.
	 * @param aPublisherType The publisher type for the {@link EventMetaData}.
	 * @param aSource The according source (origin).
	 *
	 * @return The accordingly created {@link MessageBusEvent} instance.
	 */
	public static MessageBusEvent messageBusEvent( String aMessage, String aChannel, Class<?> aPublisherType, ApplicationBus aSource ) {
		return new MessageBusEvent( aMessage, new EventMetaData( aChannel, aPublisherType ), aSource );
	}

	/**
	 * Constructs an {@link MessageBusEvent} event with the given values for the
	 * according properties.
	 * 
	 * @param aAction The action which this represents.
	 * @param aMessage The message to carry.
	 * @param aChannel The channel property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link MessageBusEvent} instance.
	 */
	public static MessageBusEvent messageBusEvent( Enum<?> aAction, String aMessage, String aChannel, Class<?> aPublisherType, ApplicationBus aSource ) {
		return new MessageBusEvent( aAction, aMessage, new EventMetaData( aChannel, aPublisherType ), aSource );
	}

	/**
	 * Constructs a builder for an {@link PayloadBusEvent} instance.
	 * 
	 * @param <P> The type of the payload being carried by the resulting event.
	 *
	 * @return The accordingly created {@link PayloadBusEvent.Builder} instance.
	 */
	public static <P> PayloadBusEvent.Builder<P> payloadBusEventBuilder() {
		return PayloadBusEvent.builder();
	}

	/**
	 * Constructs an {@link PayloadBusEvent} event with the given Meta-Data.
	 * 
	 * @param <P> The type of the payload being carried by this event.
	 *
	 * @param aAction The action which this represents.
	 * @param aPayload The payload to carry.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link PayloadBusEvent} instance.
	 */
	public static <P> PayloadBusEvent<P> payloadBusEvent( Enum<?> aAction, P aPayload, ApplicationBus aSource ) {
		return new PayloadBusEvent<>( aAction, aPayload, null, null, null, null, null, null, aSource );
	}

	/**
	 * Constructs an {@link PayloadBusEvent} event with the given Meta-Data.
	 *
	 * @param <P> The type of the payload being carried by this event.
	 * @param aAction The action which this represents.
	 * @param aPayload The payload to carry.
	 * @param aType The type of the payload.
	 * @param aSource The source from which this event originated.
	 * 
	 * @return The accordingly created {@link PayloadBusEvent} instance.
	 */
	public static <P> PayloadBusEvent<P> payloadBusEvent( Enum<?> aAction, P aPayload, Class<P> aType, ApplicationBus aSource ) {
		return new PayloadBusEvent<>( aAction, aPayload, aType, null, null, null, null, null, aSource );
	}

	/**
	 * Constructs an {@link PayloadBusEvent} event with predefined values for
	 * the according properties retrieved from the caller's class.
	 *
	 * @param <P> The type of the payload being carried by this event.
	 * @param aAction The action which this represents.
	 * @param aPayload The payload to carry.
	 * @param aType The type of the payload.
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source from which this event originated.
	 * 
	 * @return The accordingly created {@link PayloadBusEvent} instance.
	 */
	public static <P> PayloadBusEvent<P> payloadBusEvent( Enum<?> aAction, P aPayload, Class<P> aType, Class<?> aPublisherType, ApplicationBus aSource ) {
		return new PayloadBusEvent<>( aAction, aPayload, aType, null, null, null, null, aPublisherType, aSource );
	}

	/**
	 * Constructs an {@link PayloadBusEvent} event with predefined values for
	 * the according properties retrieved from the caller's class.
	 *
	 * @param <P> The type of the payload being carried by this event.
	 * @param aAction The action which this represents.
	 * @param aPayload The payload to carry.
	 * @param aType The type of the payload.
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aSource The source from which this event originated.
	 * 
	 * @return The accordingly created {@link PayloadBusEvent} instance.
	 */
	public static <P> PayloadBusEvent<P> payloadBusEvent( Enum<?> aAction, P aPayload, Class<P> aType, String aChannel, ApplicationBus aSource ) {
		return new PayloadBusEvent<>( aAction, aPayload, aType, null, null, aChannel, null, null, aSource );
	}

	/**
	 * Constructs an {@link PayloadBusEvent} event with the given values for the
	 * according properties.
	 *
	 * @param <P> The type of the payload being carried by this event.
	 * @param aAction The action which this represents.
	 * @param aPayload The payload to carry.
	 * @param aType The type of the payload.
	 * @param aChannel The channel property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source from which this event originated.
	 * 
	 * @return The accordingly created {@link PayloadBusEvent} instance.
	 */
	public static <P> PayloadBusEvent<P> payloadBusEvent( Enum<?> aAction, P aPayload, Class<P> aType, String aChannel, Class<?> aPublisherType, ApplicationBus aSource ) {
		return new PayloadBusEvent<>( aAction, aPayload, aType, null, null, aChannel, null, aPublisherType, aSource );
	}

	/**
	 * Constructs an {@link PayloadBusEvent} event with the given Meta-Data.
	 * 
	 * @param <P> The type of the payload being carried by this event.
	 *
	 * @param aAction The action which this represents.
	 * @param aPayload The payload to carry.
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link PayloadBusEvent} instance.
	 */
	public static <P> PayloadBusEvent<P> payloadBusEvent( Enum<?> aAction, P aPayload, EventMetaData aEventMetaData, ApplicationBus aSource ) {
		return new PayloadBusEvent<>( aAction, aPayload, null, aEventMetaData, aSource );
	}

	/**
	 * Constructs an {@link PayloadBusEvent} event with predefined values for
	 * the according properties retrieved from the caller's class.
	 * 
	 * @param <P> The type of the payload being carried by this event.
	 *
	 * @param aAction The action which this represents.
	 * @param aPayload The payload to carry.
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link PayloadBusEvent} instance.
	 */
	public static <P> PayloadBusEvent<P> payloadBusEvent( Enum<?> aAction, P aPayload, String aChannel, ApplicationBus aSource ) {
		return new PayloadBusEvent<>( aAction, aPayload, null, null, null, aChannel, null, null, aSource );
	}

	/**
	 * Constructs an {@link PayloadBusEvent} event with the given values for the
	 * according properties.
	 * 
	 * @param <P> The type of the payload being carried by this event.
	 *
	 * @param aAction The action which this represents.
	 * @param aPayload The payload to carry.
	 * @param aChannel The channel property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link PayloadBusEvent} instance.
	 */
	public static <P> PayloadBusEvent<P> payloadBusEvent( Enum<?> aAction, P aPayload, String aChannel, Class<?> aPublisherType, ApplicationBus aSource ) {
		return new PayloadBusEvent<>( aAction, aPayload, null, null, null, aChannel, null, aPublisherType, aSource );
	}

	/**
	 * Constructs an {@link PayloadBusEvent} event with the given values for the
	 * according properties.
	 * 
	 * @param <P> The type of the payload being carried by this event.
	 *
	 * @param aAction The action which this represents.
	 * @param aPayload The payload to carry.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link PayloadBusEvent} instance.
	 */
	public static <P> PayloadBusEvent<P> payloadBusEvent( Enum<?> aAction, P aPayload, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, ApplicationBus aSource ) {
		return new PayloadBusEvent<>( aAction, aPayload, null, aAlias, aGroup, aChannel, aUid, aPublisherType, aSource );
	}

	/**
	 * Constructs an {@link PayloadBusEvent} event with the given Meta-Data.
	 *
	 * @param <P> The type of the payload being carried by this event.
	 * @param aPayload The payload to carry.
	 * @param aSource The source from which this event originated.
	 * 
	 * @return The accordingly created {@link PayloadBusEvent} instance.
	 */
	public static <P> PayloadBusEvent<P> payloadBusEvent( P aPayload, ApplicationBus aSource ) {
		return new PayloadBusEvent<>( null, aPayload, null, null, null, null, null, null, aSource );
	}

	/**
	 * Constructs an {@link PayloadBusEvent} event with the given Meta-Data.
	 *
	 * @param <P> The type of the payload being carried by this event.
	 * @param aPayload The payload to carry.
	 * @param aType The type of the payload.
	 * @param aSource The source from which this event originated.
	 * 
	 * @return The accordingly created {@link PayloadBusEvent} instance.
	 */
	public static <P> PayloadBusEvent<P> payloadBusEvent( P aPayload, Class<P> aType, ApplicationBus aSource ) {
		return new PayloadBusEvent<>( null, aPayload, aType, null, null, null, null, null, aSource );
	}

	/**
	 * Constructs an {@link PayloadBusEvent} event with predefined values for
	 * the according properties retrieved from the caller's class.
	 *
	 * @param <P> The type of the payload being carried by this event.
	 * @param aPayload The payload to carry.
	 * @param aType The type of the payload.
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source from which this event originated.
	 * 
	 * @return The accordingly created {@link PayloadBusEvent} instance.
	 */
	public static <P> PayloadBusEvent<P> payloadBusEvent( P aPayload, Class<P> aType, Class<?> aPublisherType, ApplicationBus aSource ) {
		return new PayloadBusEvent<>( null, aPayload, aType, null, null, null, null, aPublisherType, aSource );

	}

	/**
	 * Constructs an {@link PayloadBusEvent} event with the given Meta-Data.
	 *
	 * @param <P> The type of the payload being carried by this event.
	 * @param aPayload The payload to carry.
	 * @param aType The type of the payload.
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 * @param aSource The source from which this event originated.
	 * 
	 * @return The accordingly created {@link PayloadBusEvent} instance.
	 */
	public static <P> PayloadBusEvent<P> payloadBusEvent( P aPayload, Class<P> aType, EventMetaData aEventMetaData, ApplicationBus aSource ) {
		return new PayloadBusEvent<>( null, aPayload, aType, aEventMetaData, aSource );
	}

	/**
	 * Constructs an {@link PayloadBusEvent} event with predefined values for
	 * the according properties retrieved from the caller's class.
	 *
	 * @param <P> The type of the payload being carried by this event.
	 * @param aPayload The payload to carry.
	 * @param aType The type of the payload.
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aSource The source from which this event originated.
	 * 
	 * @return The accordingly created {@link PayloadBusEvent} instance.
	 */
	public static <P> PayloadBusEvent<P> payloadBusEvent( P aPayload, Class<P> aType, String aChannel, ApplicationBus aSource ) {
		return new PayloadBusEvent<>( null, aPayload, aType, null, null, aChannel, null, null, aSource );
	}

	/**
	 * Constructs an {@link PayloadBusEvent} event with the given values for the
	 * according properties.
	 *
	 * @param <P> The type of the payload being carried by this event.
	 * @param aPayload The payload to carry.
	 * @param aType The type of the payload.
	 * @param aChannel The channel for the {@link EventMetaData}.
	 * @param aPublisherType The publisher type for the {@link EventMetaData}.
	 * @param aSource The according source (origin).
	 * 
	 * @return The accordingly created {@link PayloadBusEvent} instance.
	 */
	public static <P> PayloadBusEvent<P> payloadBusEvent( P aPayload, Class<P> aType, String aChannel, Class<?> aPublisherType, ApplicationBus aSource ) {
		return new PayloadBusEvent<>( null, aPayload, aType, null, null, aChannel, null, aPublisherType, aSource );
	}

	/**
	 * Constructs an {@link PayloadBusEvent} event with the given values for the
	 * according properties.
	 *
	 * @param <P> The type of the payload being carried by this event.
	 * @param aPayload The payload to carry.
	 * @param aType The type of the payload.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source from which this event originated.
	 * 
	 * @return The accordingly created {@link PayloadBusEvent} instance.
	 */
	public static <P> PayloadBusEvent<P> payloadBusEvent( P aPayload, Class<P> aType, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, ApplicationBus aSource ) {
		return new PayloadBusEvent<>( null, aPayload, aType, aAlias, aGroup, aChannel, aUid, aPublisherType, aSource );
	}

	/**
	 * Constructs an {@link PayloadBusEvent} event with the given Meta-Data.
	 * 
	 * @param <P> The type of the payload being carried by this event.
	 *
	 * @param aPayload The payload to carry.
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link PayloadBusEvent} instance.
	 */
	public static <P> PayloadBusEvent<P> payloadBusEvent( P aPayload, EventMetaData aEventMetaData, ApplicationBus aSource ) {
		return new PayloadBusEvent<>( null, aPayload, null, aEventMetaData, aSource );
	}

	/**
	 * Constructs an {@link PayloadBusEvent} event with predefined values for
	 * the according properties retrieved from the caller's class.
	 * 
	 * @param <P> The type of the payload being carried by this event.
	 *
	 * @param aPayload The payload to carry.
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link PayloadBusEvent} instance.
	 */
	public static <P> PayloadBusEvent<P> payloadBusEvent( P aPayload, String aChannel, ApplicationBus aSource ) {
		return new PayloadBusEvent<>( null, aPayload, null, null, null, aChannel, null, null, aSource );
	}

	/**
	 * Constructs an {@link PayloadBusEvent} event with the given values for the
	 * according properties.
	 * 
	 * @param <P> The type of the payload being carried by this event.
	 *
	 * @param aPayload The payload to carry.
	 * @param aChannel The channel for the {@link EventMetaData}.
	 * @param aPublisherType The publisher type for the {@link EventMetaData}.
	 * @param aSource The according source (origin).
	 *
	 * @return The accordingly created {@link PayloadBusEvent} instance.
	 */
	public static <P> PayloadBusEvent<P> payloadBusEvent( P aPayload, String aChannel, Class<?> aPublisherType, ApplicationBus aSource ) {
		return new PayloadBusEvent<>( null, aPayload, null, null, null, aChannel, null, aPublisherType, aSource );
	}

	/**
	 * Constructs an {@link PayloadBusEvent} event with the given values for the
	 * according properties.
	 * 
	 * @param <P> The type of the payload being carried by this event.
	 *
	 * @param aPayload The payload to carry.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link PayloadBusEvent} instance.
	 */
	public static <P> PayloadBusEvent<P> payloadBusEvent( P aPayload, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, ApplicationBus aSource ) {
		return new PayloadBusEvent<>( null, aPayload, null, aAlias, aGroup, aChannel, aUid, aPublisherType, aSource );
	}

	/**
	 * Constructs an {@link PayloadBusEvent} event with the given values for the
	 * according properties.
	 *
	 * @param <P> The type of the payload being carried by this event.
	 * @param aAction The action which this represents.
	 * @param aPayload The payload to carry.
	 * @param aType The type of the payload.
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 * @param aSource The source from which this event originated.
	 * 
	 * @return The accordingly created {@link PayloadBusEvent} instance.
	 */
	public static <P> PayloadBusEvent<P> payloadBusEvent( Enum<?> aAction, P aPayload, Class<P> aType, EventMetaData aEventMetaData, ApplicationBus aSource ) {
		return new PayloadBusEvent<>( aAction, aPayload, aType, aEventMetaData, aSource );
	}

	/**
	 * Constructs an {@link PayloadBusEvent} event with the given values for the
	 * according properties.
	 *
	 * @param <P> The type of the payload being carried by this event.
	 * @param aAction The action which this represents.
	 * @param aPayload The payload to carry.
	 * @param aType The type of the payload.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source from which this event originated.
	 * 
	 * @return The accordingly created {@link PayloadBusEvent} instance.
	 */
	public static <P> PayloadBusEvent<P> payloadBusEvent( Enum<?> aAction, P aPayload, Class<P> aType, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, ApplicationBus aSource ) {
		return new PayloadBusEvent<>( aAction, aPayload, aType, aAlias, aGroup, aChannel, aUid, aPublisherType, aSource );
	}

	/**
	 * Constructs a builder for an {@link PropertiesBusEvent} instance.
	 *
	 * @return The accordingly created {@link PropertiesBusEvent.Builder}
	 *         instance.
	 */
	public static PropertiesBusEvent.Builder propertiesBusEventBuilder() {
		return PropertiesBusEvent.builder();
	}

	/**
	 * Constructs an {@link PropertiesBusEvent} event with predefined values for
	 * the according properties retrieved from the caller's class.
	 * 
	 * @param aProperties The properties to carry.
	 * 
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link PropertiesBusEvent} instance.
	 */
	public static PropertiesBusEvent propertiesBusEvent( Properties aProperties, Class<?> aPublisherType, ApplicationBus aSource ) {
		return new PropertiesBusEvent( aProperties, aPublisherType, aSource );
	}

	/**
	 * Constructs an {@link PropertiesBusEvent} event with predefined values for
	 * the according properties retrieved from the caller's class.
	 * 
	 * @param aAction The action which this represents.
	 * 
	 * @param aProperties The properties to carry.
	 * 
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link PropertiesBusEvent} instance.
	 */
	public static PropertiesBusEvent propertiesBusEvent( Enum<?> aAction, Properties aProperties, Class<?> aPublisherType, ApplicationBus aSource ) {
		return new PropertiesBusEvent( aAction, aProperties, aPublisherType, aSource );
	}

	/**
	 * Constructs an {@link PropertiesBusEvent} event with the given Meta-Data.
	 * 
	 * @param aAction The action which this represents.
	 * 
	 * @param aProperties The properties to carry.
	 * 
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link PropertiesBusEvent} instance.
	 */
	public static PropertiesBusEvent propertiesBusEvent( Enum<?> aAction, Properties aProperties, EventMetaData aEventMetaData, ApplicationBus aSource ) {
		return new PropertiesBusEvent( aAction, aProperties, aEventMetaData, aSource );
	}

	/**
	 * Constructs an {@link PropertiesBusEvent} event with the given Meta-Data.
	 * 
	 * @param aAction The action which this represents.
	 * 
	 * @param aProperties The properties to carry.
	 * 
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link PropertiesBusEvent} instance.
	 */
	public static PropertiesBusEvent propertiesBusEvent( Enum<?> aAction, Properties aProperties, ApplicationBus aSource ) {
		return new PropertiesBusEvent( aAction, aProperties, aSource );
	}

	/**
	 * Constructs an {@link PropertiesBusEvent} event with predefined values for
	 * the according properties retrieved from the caller's class.
	 * 
	 * @param aAction The action which this represents.
	 * 
	 * @param aProperties The properties to carry.
	 * 
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link PropertiesBusEvent} instance.
	 */
	public static PropertiesBusEvent propertiesBusEvent( Enum<?> aAction, Properties aProperties, String aChannel, ApplicationBus aSource ) {
		return new PropertiesBusEvent( aAction, aProperties, aChannel, aSource );
	}

	/**
	 * Constructs an {@link PropertiesBusEvent} event with the given values for
	 * the according properties.
	 * 
	 * @param aAction The action which this represents.
	 * 
	 * @param aProperties The properties to carry.
	 * 
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link PropertiesBusEvent} instance.
	 */
	public static PropertiesBusEvent propertiesBusEvent( Enum<?> aAction, Properties aProperties, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, ApplicationBus aSource ) {
		return new PropertiesBusEvent( aAction, aProperties, aAlias, aGroup, aChannel, aUid, aPublisherType, aSource );
	}

	/**
	 * Constructs an {@link PropertiesBusEvent} event with the given Meta-Data.
	 * 
	 * @param aProperties The properties to carry.
	 * 
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link PropertiesBusEvent} instance.
	 */
	public static PropertiesBusEvent propertiesBusEvent( Properties aProperties, EventMetaData aEventMetaData, ApplicationBus aSource ) {
		return new PropertiesBusEvent( aProperties, aEventMetaData, aSource );
	}

	/**
	 * Constructs an {@link PropertiesBusEvent} event with the given Meta-Data.
	 *
	 * @param aProperties The properties to carry.
	 * 
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link PropertiesBusEvent} instance.
	 */
	public static PropertiesBusEvent propertiesBusEvent( Properties aProperties, ApplicationBus aSource ) {
		return new PropertiesBusEvent( aProperties, aSource );
	}

	/**
	 * Constructs an {@link PropertiesBusEvent} event with predefined values for
	 * the according properties retrieved from the caller's class.
	 * 
	 * @param aProperties The properties to carry.
	 * 
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link PropertiesBusEvent} instance.
	 */
	public static PropertiesBusEvent propertiesBusEvent( Properties aProperties, String aChannel, ApplicationBus aSource ) {
		return new PropertiesBusEvent( aProperties, aChannel, aSource );
	}

	/**
	 * Constructs an {@link PropertiesBusEvent} event with the given values for
	 * the according properties.
	 * 
	 * @param aProperties The properties to carry.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link PropertiesBusEvent} instance.
	 */
	public static PropertiesBusEvent propertiesBusEvent( Properties aProperties, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, ApplicationBus aSource ) {
		return new PropertiesBusEvent( aProperties, aAlias, aGroup, aChannel, aUid, aPublisherType, aSource );
	}

	/**
	 * Constructs an {@link PropertiesBusEvent} event with the given values for
	 * the according properties.
	 * 
	 * @param aProperties The properties to carry.
	 * 
	 * @param aChannel The channel for the {@link EventMetaData}.
	 * @param aPublisherType The publisher type for the {@link EventMetaData}.
	 * @param aSource The according source (origin).
	 *
	 * @return The accordingly created {@link PropertiesBusEvent} instance.
	 */
	public static PropertiesBusEvent propertiesBusEvent( Properties aProperties, String aChannel, Class<?> aPublisherType, ApplicationBus aSource ) {
		return new PropertiesBusEvent( aProperties, new EventMetaData( aChannel, aPublisherType ), aSource );
	}

	/**
	 * Constructs an {@link PropertiesBusEvent} event with the given values for
	 * the according properties.
	 * 
	 * @param aAction The action which this represents.
	 * 
	 * @param aProperties The properties to carry.
	 * 
	 * @param aChannel The channel property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source from which this event originated.
	 *
	 * @return The accordingly created {@link PropertiesBusEvent} instance.
	 */
	public static PropertiesBusEvent propertiesBusEvent( Enum<?> aAction, Properties aProperties, String aChannel, Class<?> aPublisherType, ApplicationBus aSource ) {
		return new PropertiesBusEvent( aAction, aProperties, new EventMetaData( aChannel, aPublisherType ), aSource );
	}

	/**
	 * Catches all events, no matching is done.
	 * 
	 * @return The "catch-all" {@link ApplicationBusEventMatcher}.
	 */
	public static ApplicationBusEventMatcher catchAll() {
		return new CatchAllMatcher();
	}

	/**
	 * Catches no event, no matching is done.
	 * 
	 * @return The "catch-none" {@link ApplicationBusEventMatcher}.
	 */
	public static ApplicationBusEventMatcher catchNone() {
		return new CatchNoneMatcher();
	}

	/**
	 * Factory method to create an event matcher by event type.
	 * 
	 * @param aEventType The event type to be matched by this matcher.
	 * 
	 * @return An event matcher by event type.
	 */
	public static ApplicationBusEventMatcher isEventTypeOf( Class<?> aEventType ) {
		return new MatcherWrapper( MatcherSugar.isAssignableFrom( aEventType ) );
	}

	/**
	 * Checks if is payload type of.
	 *
	 * @param <P> The type of the payload to be matched.
	 * @param aType The type of the payload.
	 * 
	 * @return An event matcher by payload type.
	 */
	public static <P> ApplicationBusEventMatcher isPayloadTypeOf( Class<P> aType ) {
		return new PayloadTypeOf<P>( aType );
	}

	/**
	 * Factory method to create an event matcher by event publisher type.
	 *
	 * @param <PT> The publisher descriptor type
	 * @param aPublisherType The event publisher type to be matched by this
	 *        matcher.
	 * 
	 * @return An event matcher by event type.
	 */
	public static <PT extends Object> ApplicationBusEventMatcher isPublisherTypeOf( Class<? extends PT> aPublisherType ) {
		return new PublisherTypeOf<PT>( aPublisherType );
	}

	/**
	 * Factory method to create an "OR" matcher for the given matchers.
	 * 
	 * @param aEventMatchers The matchers to be combined by an "OR".
	 * 
	 * @return An "OR" matcher.
	 */
	@SafeVarargs
	public static ApplicationBusEventMatcher or( ApplicationBusEventMatcher... aEventMatchers ) {
		return new MatcherWrapper( MatcherSugar.or( aEventMatchers ) );
	}

	/**
	 * Factory method to create an "AND" matcher for the given matchers.
	 *
	 * @param aEventMatchers The matchers to be combined by an "AND".
	 * 
	 * @return An "AND" matcher.
	 */
	@SafeVarargs
	public static ApplicationBusEventMatcher and( ApplicationBusEventMatcher... aEventMatchers ) {
		return new MatcherWrapper( MatcherSugar.and( aEventMatchers ) );
	}

	/**
	 * Factory method to create an "EQUAL WITH" matcher for the given name
	 * compared with the name stored in the {@link EventMetaData}.
	 *
	 * @param aAlias The name to be compared with a {@link MetaDataEvent}'s
	 *        {@link EventMetaData}'s name property.
	 * 
	 * @return An "EQUAL WITH" matcher regarding the {@link MetaDataEvent}'s
	 *         name property.
	 */
	public static ApplicationBusEventMatcher aliasEqualWith( String aAlias ) {
		return new AliasEqualWithMatcher( aAlias );
	}

	/**
	 * Factory method to create an "EQUAL WITH" matcher for the given group
	 * compared with the group stored in the {@link EventMetaData}.
	 *
	 * @param aGroup The group to be compared with a {@link MetaDataEvent}'s
	 *        {@link EventMetaData}'s group property.
	 * 
	 * @return An "EQUAL WITH" matcher regarding the {@link MetaDataEvent}'s
	 *         group property.
	 */
	public static ApplicationBusEventMatcher groupEqualWith( String aGroup ) {
		return new GroupEqualWithMatcher( aGroup );
	}

	/**
	 * Factory method to create an "EQUAL WITH" matcher for the given channel
	 * compared with the channel stored in the {@link EventMetaData}.
	 *
	 * @param aChannel The channel to be compared with a {@link MetaDataEvent}'s
	 *        {@link EventMetaData}'s channel property.
	 * 
	 * @return An "EQUAL WITH" matcher regarding the {@link MetaDataEvent}'s
	 *         channel property.
	 */
	public static ApplicationBusEventMatcher channelEqualWith( String aChannel ) {
		return new ChannelEqualWithMatcher( aChannel );
	}

	/**
	 * Factory method to create an "EQUAL WITH" matcher for the given UID
	 * compared with the UID stored in the {@link EventMetaData}.
	 *
	 * @param aUid The UID to be compared with a {@link MetaDataEvent}'s
	 *        {@link EventMetaData}'s UID property.
	 * 
	 * @return An "EQUAL WITH" matcher regarding the {@link MetaDataEvent}'s UID
	 *         property.
	 */
	public static ApplicationBusEventMatcher uidIdEqualWith( String aUid ) {
		return new UniversalIdEqualWithMatcher( aUid );
	}

	/**
	 * Factory method to create an "EQUAL WITH" matcher for the given action
	 * compared with the action stored in the {@link EventMetaData}.
	 *
	 * @param <A> The type of the action stored in the event. CAUTION: The
	 *        drawback of not using generic generic type declaration on a class
	 *        level is no granted type safety, the advantage is the ease of use:
	 *        Sub-classes can be used out of the box.
	 * @param aAction The action to be compared with a {@link MetaDataEvent}'s
	 *        {@link EventMetaData}'s action property.
	 * 
	 * @return An "EQUAL WITH" matcher regarding the {@link ActionEvent}'s
	 *         action property.
	 */
	public static <A> ApplicationBusEventMatcher actionEqualWith( A aAction ) {
		return new ActionEqualWithMatcher<A>( aAction );
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	protected static class CatchAllMatcher extends CatchAllEventMatcher<ApplicationBusEvent> implements ApplicationBusEventMatcher {}

	protected static class CatchNoneMatcher extends CatchNoneEventMatcher<ApplicationBusEvent> implements ApplicationBusEventMatcher {}

	private static class PublisherTypeOf<PT> extends PublisherTypeOfEventMatcher<ApplicationBusEvent, PT> implements ApplicationBusEventMatcher {
		public PublisherTypeOf( Class<? extends PT> aEventPublisherType ) {
			super( aEventPublisherType );
		}
	}

	private static class PayloadTypeOf<P> extends AbstractEventMatcher<ApplicationBusEvent> implements ApplicationBusEventMatcher {

		public static final String ALIAS = "PAYLOAD_TYPE_OF";

		private Class<P> _type;

		public PayloadTypeOf( Class<P> aType ) {
			super( ALIAS, "Matches by event payload type (PAYLOAD TYPE OF)." );
			_type = aType;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean isMatching( ApplicationBusEvent aEvent ) {
			if ( _type != null ) {
				if ( aEvent instanceof PayloadBusEvent<?> theEvent ) {
					return theEvent.getType().isAssignableFrom( _type );
				}
				return false;
			}
			return true;
		}
	}

	private static class AliasEqualWithMatcher extends AliasEqualWithEventMatcher<ApplicationBusEvent> implements ApplicationBusEventMatcher {
		public AliasEqualWithMatcher( String aAlias ) {
			super( aAlias );
		}
	}

	private static class GroupEqualWithMatcher extends GroupEqualWithEventMatcher<ApplicationBusEvent> implements ApplicationBusEventMatcher {
		public GroupEqualWithMatcher( String aGroup ) {
			super( aGroup );
		}
	}

	private static class ChannelEqualWithMatcher extends ChannelEqualWithEventMatcher<ApplicationBusEvent> implements ApplicationBusEventMatcher {
		public ChannelEqualWithMatcher( String aChannel ) {
			super( aChannel );
		}
	}

	private static class UniversalIdEqualWithMatcher extends UniversalIdEqualWithEventMatcher<ApplicationBusEvent> implements ApplicationBusEventMatcher {
		public UniversalIdEqualWithMatcher( String aUid ) {
			super( aUid );
		}
	}

	private static class ActionEqualWithMatcher<A> extends ActionEqualWithEventMatcher<ApplicationBusEvent> implements ApplicationBusEventMatcher {
		public ActionEqualWithMatcher( A aAction ) {
			super( aAction );
		}
	}

	private static class MatcherWrapper implements ApplicationBusEventMatcher {

		private final Matcher<ApplicationBusEvent> _eventMatcher;

		public MatcherWrapper( Matcher<ApplicationBusEvent> aMatcher ) {
			assert ( aMatcher != null );
			_eventMatcher = aMatcher;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean isMatching( ApplicationBusEvent aEvent ) {
			return _eventMatcher.isMatching( aEvent );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public MatcherSchema toSchema() {
			return new MatcherSchema( getClass(), _eventMatcher.toSchema() );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String getAlias() {
			return _eventMatcher.getAlias();
		}
	}
}
