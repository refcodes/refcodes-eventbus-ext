// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.eventbus.ext.application;

import static org.refcodes.eventbus.ext.application.ApplicationBusSugar.*;

import org.refcodes.eventbus.DispatchStrategy;
import org.refcodes.eventbus.EventDispatcher;
import org.refcodes.observer.EventMatcher;
import org.refcodes.observer.EventMetaData;
import org.refcodes.observer.Observable;
import org.refcodes.observer.Observer;

/**
 * The {@link InitializeBus} extends the {@link ApplicationBus} with convenience
 * functionality common to everyday application and service development
 * regarding dispatching of {@link InitializeBusEvent} instances.
 */
public interface InitializeBus extends EventDispatcher<ApplicationBusEvent, Observer<ApplicationBusEvent>, ApplicationBusEventMatcher, EventMetaData, String> {

	/**
	 * Publishes a initialize event with the given attributes. This method uses
	 * the {@link DispatchStrategy#CASCADE} to make sure your publishing thread
	 * synchronizes till all observers have been notified, though your
	 * publishing thread is not blocked upon event publishing of the observers
	 * (also using the default {@link DispatchStrategy#CASCADE}) themselves.
	 * 
	 * @param aPublisherType The type of the event publisher.
	 */
	default void publishInitialize( Class<?> aPublisherType ) {
		publishEvent( new InitializeBusEvent( aPublisherType, (ApplicationBus) this ), DispatchStrategy.CASCADE );
	}

	/**
	 * Publishes a initialize event with the given attributes. This method uses
	 * the {@link DispatchStrategy#CASCADE} to make sure your publishing thread
	 * synchronizes till all observers have been notified, though your
	 * publishing thread is not blocked upon event publishing of the observers
	 * (also using the default {@link DispatchStrategy#CASCADE}) themselves.
	 * 
	 * @param aChannel The channel name on which the event is receivable.
	 */
	default void publishInitialize( String aChannel ) {
		publishEvent( new InitializeBusEvent( aChannel, getClass(), (ApplicationBus) this ), DispatchStrategy.CASCADE );
	}

	/**
	 * Publishes an event with the provided initialize for the given attributes.
	 * This method uses the {@link DispatchStrategy#CASCADE} to make sure your
	 * publishing thread synchronizes till all observers have been notified,
	 * though your publishing thread is not blocked upon event publishing of the
	 * observers (also using the default {@link DispatchStrategy#CASCADE})
	 * themselves.
	 * 
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 */
	default void publishInitialize( String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType ) {
		publishEvent( new InitializeBusEvent( aAlias, aGroup, aChannel, aUid, aPublisherType, (ApplicationBus) this ), DispatchStrategy.CASCADE );
	}

	/**
	 * Publishes a initialize event with the given attributes. This method uses
	 * the {@link DispatchStrategy#CASCADE} to make sure your publishing thread
	 * synchronizes till all observers have been notified, though your
	 * publishing thread is not blocked upon event publishing of the observers
	 * (also using the default {@link DispatchStrategy#CASCADE}) themselves.
	 * 
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 */
	default void publishInitialize( EventMetaData aEventMetaData ) {
		publishEvent( new InitializeBusEvent( aEventMetaData, (ApplicationBus) this ), DispatchStrategy.CASCADE );
	}

	/**
	 * Publishes a initialize event with the given attributes. This method uses
	 * the {@link DispatchStrategy#CASCADE} to make sure your publishing thread
	 * synchronizes till all observers have been notified, though your
	 * publishing thread is not blocked upon event publishing of the observers
	 * (also using the default {@link DispatchStrategy#CASCADE}) themselves.
	 */
	default void publishInitialize() {
		publishEvent( new InitializeBusEvent( getClass(), (ApplicationBus) this ), DispatchStrategy.CASCADE );
	}

	/**
	 * Publishes a initialize event with the given attributes.
	 * 
	 * @param aPublisherType The type of the event publisher.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default void publishInitialize( Class<?> aPublisherType, DispatchStrategy aStrategy ) {
		publishEvent( new InitializeBusEvent( aPublisherType, (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes a initialize event with the given attributes.
	 * 
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default void publishInitialize( String aChannel, DispatchStrategy aStrategy ) {
		publishEvent( new InitializeBusEvent( aChannel, getClass(), (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes a initialize event with the given attributes.
	 * 
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default void publishInitialize( String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, DispatchStrategy aStrategy ) {
		publishEvent( new InitializeBusEvent( aAlias, aGroup, aChannel, aUid, aPublisherType, (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes a initialize event with the given attributes.
	 * 
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default void publishInitialize( EventMetaData aEventMetaData, DispatchStrategy aStrategy ) {
		publishEvent( new InitializeBusEvent( aEventMetaData, (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes a initialize event with the given attributes.
	 * 
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default void publishInitialize( DispatchStrategy aStrategy ) {
		publishEvent( new InitializeBusEvent( getClass(), (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link InitializeBusEvent} instances.
	 *
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onInitialize( Observer<InitializeBusEvent> aObserver ) {
		return subscribe( InitializeBusEvent.class, aObserver );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link InitializeBusEvent} instances with the given attributes.
	 *
	 * @param aPublisherType The type of the event publisher.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onInitialize( Class<?> aPublisherType, Observer<InitializeBusEvent> aObserver ) {
		return subscribe( InitializeBusEvent.class, isPublisherTypeOf( aPublisherType ), aObserver );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link InitializeBusEvent} instances with the given attributes.
	 *
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onInitialize( String aChannel, Observer<InitializeBusEvent> aObserver ) {
		return subscribe( InitializeBusEvent.class, channelEqualWith( aChannel ), aObserver );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link InitializeBusEvent} instances with the given attributes.
	 *
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onInitialize( String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, Observer<InitializeBusEvent> aObserver ) {
		return subscribe( InitializeBusEvent.class, and( aliasEqualWith( aAlias ), groupEqualWith( aGroup ), channelEqualWith( aChannel ), uidIdEqualWith( aUid ), isPublisherTypeOf( aPublisherType ) ), aObserver );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link InitializeBusEvent} instances with the given attributes.
	 *
	 * @param aAction The action property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onInitialize( Enum<?> aAction, Class<?> aPublisherType, Observer<InitializeBusEvent> aObserver ) {
		return subscribe( InitializeBusEvent.class, and( actionEqualWith( aAction ), isPublisherTypeOf( aPublisherType ) ), aObserver );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link InitializeBusEvent} instances with the given attributes.
	 *
	 * @param aAction The action property.
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onInitialize( Enum<?> aAction, String aChannel, Observer<InitializeBusEvent> aObserver ) {
		return subscribe( InitializeBusEvent.class, and( actionEqualWith( aAction ), channelEqualWith( aChannel ) ), aObserver );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link InitializeBusEvent} instances with the given attributes.
	 *
	 * @param aAction The action property.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onInitialize( Enum<?> aAction, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, Observer<InitializeBusEvent> aObserver ) {
		return subscribe( InitializeBusEvent.class, and( actionEqualWith( aAction ), aliasEqualWith( aAlias ), groupEqualWith( aGroup ), channelEqualWith( aChannel ), uidIdEqualWith( aUid ), isPublisherTypeOf( aPublisherType ) ), aObserver );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link InitializeBusEvent} instances with the given attributes. Your
	 * {@link Observable} may be of the required type!
	 *
	 * @param aAction The action property.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onInitialize( Enum<?> aAction, Observer<InitializeBusEvent> aObserver ) {
		return subscribe( InitializeBusEvent.class, actionEqualWith( aAction ), aObserver );
	}
}
