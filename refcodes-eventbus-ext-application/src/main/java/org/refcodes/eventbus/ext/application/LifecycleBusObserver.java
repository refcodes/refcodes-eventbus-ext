// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.eventbus.ext.application;

import org.refcodes.component.ext.observer.LifecycleRequestObserver;
import org.refcodes.observer.EventMetaData;

/**
 * An observer for listening to {@link LifecycleBusEvent} instances. The various
 * lifecycle methods have a default implementation, so just implement those
 * methods on which's events you are interested.
 */
public interface LifecycleBusObserver extends LifecycleRequestObserver<InitializeBusEvent, StartBusEvent, ResumeBusEvent, PauseBusEvent, StopBusEvent, DestroyBusEvent, EventMetaData, ApplicationBus> {

	/**
	 * {@inheritDoc}
	 */
	@Override
	default void onInitialize( InitializeBusEvent aEvent ) {
		/* IMPLEMENT WHEN INTERESTED IN THIS EVENT TYPE */
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default void onStart( StartBusEvent aEvent ) {
		/* IMPLEMENT WHEN INTERESTED IN THIS EVENT TYPE */
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default void onResume( ResumeBusEvent aEvent ) {
		/* IMPLEMENT WHEN INTERESTED IN THIS EVENT TYPE */
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default void onPause( PauseBusEvent aEvent ) {
		/* IMPLEMENT WHEN INTERESTED IN THIS EVENT TYPE */
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default void onStop( StopBusEvent aEvent ) {
		/* IMPLEMENT WHEN INTERESTED IN THIS EVENT TYPE */
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default void onDestroy( DestroyBusEvent aEvent ) {
		/* IMPLEMENT WHEN INTERESTED IN THIS EVENT TYPE */
	}
}