// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.eventbus.ext.application;

import static org.refcodes.eventbus.ext.application.ApplicationBusSugar.*;

import org.refcodes.eventbus.DispatchStrategy;
import org.refcodes.eventbus.EventDispatcher;
import org.refcodes.observer.EventMatcher;
import org.refcodes.observer.EventMetaData;
import org.refcodes.observer.Observable;
import org.refcodes.observer.Observer;

/**
 * The {@link ExceptionBus} extends the {@link ApplicationBus} with convenience
 * functionality common to everyday application and service development
 * regarding dispatching of {@link Throwable} instances.
 */
public interface ExceptionBus extends EventDispatcher<ApplicationBusEvent, Observer<ApplicationBusEvent>, ApplicationBusEventMatcher, EventMetaData, String> {

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link ExceptionBusEvent} instances with the given attributes.
	 *
	 * @param aPublisherType The type of the event publisher.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onException( Class<?> aPublisherType, Observer<ExceptionBusEvent> aObserver ) {
		return subscribe( ExceptionBusEvent.class, isPublisherTypeOf( aPublisherType ), aObserver );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link ExceptionBusEvent} instances with the given attributes.
	 *
	 * @param aAction The action property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onException( Enum<?> aAction, Class<?> aPublisherType, Observer<ExceptionBusEvent> aObserver ) {
		return subscribe( ExceptionBusEvent.class, and( actionEqualWith( aAction ), isPublisherTypeOf( aPublisherType ) ), aObserver );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link ExceptionBusEvent} instances with the given attributes. Your
	 * {@link Observable} may be of the required type!
	 *
	 * @param aAction The action property.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onException( Enum<?> aAction, Observer<ExceptionBusEvent> aObserver ) {
		return subscribe( ExceptionBusEvent.class, actionEqualWith( aAction ), aObserver );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link ExceptionBusEvent} instances with the given attributes.
	 *
	 * @param aAction The action property.
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onException( Enum<?> aAction, String aChannel, Observer<ExceptionBusEvent> aObserver ) {
		return subscribe( ExceptionBusEvent.class, and( actionEqualWith( aAction ), channelEqualWith( aChannel ) ), aObserver );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link ExceptionBusEvent} instances with the given attributes.
	 *
	 * @param aAction The action property.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onException( Enum<?> aAction, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, Observer<ExceptionBusEvent> aObserver ) {
		return subscribe( ExceptionBusEvent.class, and( actionEqualWith( aAction ), aliasEqualWith( aAlias ), groupEqualWith( aGroup ), channelEqualWith( aChannel ), uidIdEqualWith( aUid ), isPublisherTypeOf( aPublisherType ) ), aObserver );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link ExceptionBusEvent} instances.
	 *
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onException( Observer<ExceptionBusEvent> aObserver ) {
		return subscribe( ExceptionBusEvent.class, aObserver );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link ExceptionBusEvent} instances with the given attributes.
	 *
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onException( String aChannel, Observer<ExceptionBusEvent> aObserver ) {
		return subscribe( ExceptionBusEvent.class, channelEqualWith( aChannel ), aObserver );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link ExceptionBusEvent} instances with the given attributes.
	 *
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onException( String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, Observer<ExceptionBusEvent> aObserver ) {
		return subscribe( ExceptionBusEvent.class, and( aliasEqualWith( aAlias ), groupEqualWith( aGroup ), channelEqualWith( aChannel ), uidIdEqualWith( aUid ), isPublisherTypeOf( aPublisherType ) ), aObserver );
	}

	/**
	 * Fires an event with the properties set for the
	 * {@link ExceptionBusEvent.Builder} instance upon finishing of by invoking
	 * {@link ExceptionBusEvent.Builder#build()}.
	 * 
	 * @return The {@link ExceptionBusEvent} class' builder.
	 */
	default ExceptionBusEvent.Builder publishException() {
		ExceptionBusEvent.Builder theBuilder = new ExceptionBusEvent.Builder() {
			@Override
			public ExceptionBusEvent build() {
				ExceptionBusEvent theEvent = super.build();
				publishEvent( theEvent );
				return theEvent;
			}
		};
		return theBuilder;
	}

	/**
	 * Fires an event with the properties set for the
	 * {@link ExceptionBusEvent.Builder} instance upon finishing of by invoking
	 * {@link ExceptionBusEvent.Builder#build()}.
	 * 
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 * 
	 * @return The {@link ExceptionBusEvent} class' builder.
	 */
	default ExceptionBusEvent.Builder publishException( DispatchStrategy aStrategy ) {
		ExceptionBusEvent.Builder theBuilder = new ExceptionBusEvent.Builder() {
			@Override
			public ExceptionBusEvent build() {
				ExceptionBusEvent theEvent = super.build();
				publishEvent( theEvent, aStrategy );
				return theEvent;
			}
		};
		return theBuilder;
	}

	/**
	 * Publishes an event with the provided exception and the given attributes.
	 * 
	 * @param aAction The action which the event represents.
	 * @param aException The exception to be carried by the event.
	 */
	default void publishException( Enum<?> aAction, Exception aException ) {
		publishEvent( new ExceptionBusEvent( aAction, aException, getClass(), (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided exception and the given attributes.
	 * 
	 * @param aAction The action which this represents.
	 * @param aException The exception to be carried by the event.
	 * @param aPublisherType The type of the event publisher.
	 */
	default void publishException( Enum<?> aAction, Exception aException, Class<?> aPublisherType ) {
		publishEvent( new ExceptionBusEvent( aAction, aException, aPublisherType, (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided exception and the given attributes.
	 * 
	 * @param aAction The action which this represents.
	 * @param aException The exception to be carried by the event.
	 * @param aPublisherType The type of the event publisher.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default void publishException( Enum<?> aAction, Exception aException, Class<?> aPublisherType, DispatchStrategy aStrategy ) {
		publishEvent( new ExceptionBusEvent( aAction, aException, aPublisherType, (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided exception and the given attributes.
	 * 
	 * @param aAction The action which the event represents.
	 * @param aException The exception to be carried by the event.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default void publishException( Enum<?> aAction, Exception aException, DispatchStrategy aStrategy ) {
		publishEvent( new ExceptionBusEvent( aAction, aException, getClass(), (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided exception and the given attributes.
	 * 
	 * @param aAction The action which the event represents.
	 * @param aException The exception to be carried by the event.
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 */
	default void publishException( Enum<?> aAction, Exception aException, EventMetaData aEventMetaData ) {
		publishEvent( new ExceptionBusEvent( aAction, aException, aEventMetaData, (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided exception and the given attributes.
	 * 
	 * @param aAction The action which the event represents.
	 * @param aException The exception to be carried by the event.
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default void publishException( Enum<?> aAction, Exception aException, EventMetaData aEventMetaData, DispatchStrategy aStrategy ) {
		publishEvent( new ExceptionBusEvent( aAction, aException, aEventMetaData, (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided exception and the given attributes.
	 * 
	 * @param aAction The action which this represents.
	 * @param aException The exception to be carried by the event.
	 * @param aChannel The channel name on which the event is receivable.
	 */
	default void publishException( Enum<?> aAction, Exception aException, String aChannel ) {
		publishEvent( new ExceptionBusEvent( aAction, aException, aChannel, getClass(), (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided exception and the given attributes.
	 * 
	 * @param aAction The action which this represents.
	 * @param aException The exception to be carried by the event.
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default void publishException( Enum<?> aAction, Exception aException, String aChannel, DispatchStrategy aStrategy ) {
		publishEvent( new ExceptionBusEvent( aAction, aException, aChannel, getClass(), (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided exception for the given attributes.
	 * 
	 * @param aAction The action which this represents.
	 * @param aException The exception to be carried by the event.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 */
	default void publishException( Enum<?> aAction, Exception aException, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType ) {
		publishEvent( new ExceptionBusEvent( aAction, aException, aAlias, aGroup, aChannel, aUid, aPublisherType, (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided exception and the given attributes.
	 * 
	 * @param aAction The action which this represents.
	 * @param aException The exception to be carried by the event.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default void publishException( Enum<?> aAction, Exception aException, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, DispatchStrategy aStrategy ) {
		publishEvent( new ExceptionBusEvent( aAction, aException, aAlias, aGroup, aChannel, aUid, aPublisherType, (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided exception and the given attributes.
	 *
	 * @param aException The exception to be carried by the event.
	 */
	default void publishException( Exception aException ) {
		publishEvent( new ExceptionBusEvent( aException, getClass(), (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided exception and the given attributes.
	 * 
	 * @param aException The exception to be carried by the event.
	 * @param aPublisherType The type of the event publisher.
	 */
	default void publishException( Exception aException, Class<?> aPublisherType ) {
		publishEvent( new ExceptionBusEvent( aException, aPublisherType, (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided exception and the given attributes.
	 * 
	 * @param aException The exception to be carried by the event.
	 * @param aPublisherType The type of the event publisher.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default void publishException( Exception aException, Class<?> aPublisherType, DispatchStrategy aStrategy ) {
		publishEvent( new ExceptionBusEvent( aException, aPublisherType, (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided exception and the given attributes.
	 *
	 * @param aException The exception to be carried by the event.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default void publishException( Exception aException, DispatchStrategy aStrategy ) {
		publishEvent( new ExceptionBusEvent( aException, getClass(), (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided exception and the given attributes.
	 * 
	 * @param aException The exception to be carried by the event.
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 */
	default void publishException( Exception aException, EventMetaData aEventMetaData ) {
		publishEvent( new ExceptionBusEvent( aException, aEventMetaData, (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided exception and the given attributes.
	 * 
	 * @param aException The exception to be carried by the event.
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default void publishException( Exception aException, EventMetaData aEventMetaData, DispatchStrategy aStrategy ) {
		publishEvent( new ExceptionBusEvent( aException, aEventMetaData, (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided exception and the given attributes.
	 * 
	 * @param aException The exception to be carried by the event.
	 * @param aChannel The channel name on which the event is receivable.
	 */
	default void publishException( Exception aException, String aChannel ) {
		publishEvent( new ExceptionBusEvent( aException, aChannel, getClass(), (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided exception and the given attributes.
	 * 
	 * @param aException The exception to be carried by the event.
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default void publishException( Exception aException, String aChannel, DispatchStrategy aStrategy ) {
		publishEvent( new ExceptionBusEvent( aException, aChannel, getClass(), (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided exception and the given attributes.
	 * 
	 * @param aException The exception to be carried by the event.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 */
	default void publishException( Exception aException, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType ) {
		publishEvent( new ExceptionBusEvent( aException, aAlias, aGroup, aChannel, aUid, aPublisherType, (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided exception and the given attributes.
	 * 
	 * @param aException The exception to be carried by the event.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default void publishException( Exception aException, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, DispatchStrategy aStrategy ) {
		publishEvent( new ExceptionBusEvent( aException, aAlias, aGroup, aChannel, aUid, aPublisherType, (ApplicationBus) this ), aStrategy );
	}
}
