// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.eventbus.ext.application;

import org.refcodes.mixin.MessageAccessor;
import org.refcodes.observer.EventMetaData;

/**
 * The {@link MessageBusEvent} represents an event indication a message.
 */
public class MessageBusEvent extends ApplicationBusEvent implements MessageAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected String _message;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private MessageBusEvent( Builder builder ) {
		this( builder.action, builder.message, builder.eventMetaData.build(), builder.source );
	}

	/**
	 * Constructs an event with predefined values for the according properties
	 * retrieved from the caller's class.
	 * 
	 * @param aMessage The message to carry.
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source from which this event originated.
	 */
	public MessageBusEvent( String aMessage, Class<?> aPublisherType, ApplicationBus aSource ) {
		super( aPublisherType, aSource );
		_message = aMessage;
	}

	/**
	 * Constructs an event with predefined values for the according properties
	 * retrieved from the caller's class.
	 * 
	 * @param aAction The action which this represents.
	 * @param aMessage The message to carry.
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source from which this event originated.
	 */
	public MessageBusEvent( Enum<?> aAction, String aMessage, Class<?> aPublisherType, ApplicationBus aSource ) {
		super( aAction, aPublisherType, aSource );
		_message = aMessage;
	}

	/**
	 * Constructs an event with the given Meta-Data.
	 * 
	 * @param aAction The action which this represents.
	 * @param aMessage The message to carry.
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 * @param aSource The source from which this event originated.
	 */
	public MessageBusEvent( Enum<?> aAction, String aMessage, EventMetaData aEventMetaData, ApplicationBus aSource ) {
		super( aAction, aEventMetaData, aSource );
		_message = aMessage;
	}

	/**
	 * Constructs an event with the given Meta-Data.
	 * 
	 * @param aAction The action which this represents.
	 * @param aMessage The message to carry.
	 * @param aSource The source from which this event originated.
	 */
	public MessageBusEvent( Enum<?> aAction, String aMessage, ApplicationBus aSource ) {
		super( aAction, aSource );
		_message = aMessage;
	}

	/**
	 * Constructs an event with predefined values for the according properties
	 * retrieved from the caller's class.
	 * 
	 * @param aAction The action which this represents.
	 * @param aMessage The message to carry.
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aSource The source from which this event originated.
	 */
	public MessageBusEvent( Enum<?> aAction, String aMessage, String aChannel, ApplicationBus aSource ) {
		super( aAction, aChannel, aSource );
		_message = aMessage;
	}

	/**
	 * Constructs an event with the given values for the according properties.
	 * 
	 * @param aAction The action which this represents.
	 * @param aMessage The message to carry.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source from which this event originated.
	 */
	public MessageBusEvent( Enum<?> aAction, String aMessage, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, ApplicationBus aSource ) {
		super( aAction, aAlias, aGroup, aChannel, aUid, aPublisherType, aSource );
		_message = aMessage;
	}

	/**
	 * Constructs an event with the given Meta-Data.
	 * 
	 * @param aMessage The message to carry.
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 * @param aSource The source from which this event originated.
	 */
	public MessageBusEvent( String aMessage, EventMetaData aEventMetaData, ApplicationBus aSource ) {
		super( aEventMetaData, aSource );
		_message = aMessage;
	}

	/**
	 * Constructs an event with the given Meta-Data.
	 *
	 * @param aMessage The message to carry.
	 * @param aSource The source from which this event originated.
	 */
	public MessageBusEvent( String aMessage, ApplicationBus aSource ) {
		super( aSource );
		_message = aMessage;
	}

	/**
	 * Constructs an event with predefined values for the according properties
	 * retrieved from the caller's class.
	 * 
	 * @param aMessage The message to carry.
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aSource The source from which this event originated.
	 */
	public MessageBusEvent( String aMessage, String aChannel, ApplicationBus aSource ) {
		super( aChannel, aSource );
		_message = aMessage;
	}

	/**
	 * Constructs an event with the given values for the according properties.
	 * 
	 * @param aMessage The message to carry.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source from which this event originated.
	 */
	public MessageBusEvent( String aMessage, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, ApplicationBus aSource ) {
		super( aAlias, aGroup, aChannel, aUid, aPublisherType, aSource );
		_message = aMessage;
	}

	/**
	 * Constructs an event with the given values for the according properties.
	 * 
	 * @param aMessage The message to carry.
	 * @param aChannel The channel for the {@link EventMetaData}.
	 * @param aPublisherType The publisher type for the {@link EventMetaData}.
	 * @param aSource The according source (origin).
	 */
	public MessageBusEvent( String aMessage, String aChannel, Class<?> aPublisherType, ApplicationBus aSource ) {
		super( new EventMetaData( aChannel, aPublisherType ), aSource );
		_message = aMessage;
	}

	/**
	 * Constructs an event with the given values for the according properties.
	 * 
	 * @param aAction The action which this represents.
	 * @param aMessage The message to carry.
	 * @param aChannel The channel property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source from which this event originated.
	 */
	public MessageBusEvent( Enum<?> aAction, String aMessage, String aChannel, Class<?> aPublisherType, ApplicationBus aSource ) {
		super( aAction, new EventMetaData( aChannel, aPublisherType ), aSource );
		_message = aMessage;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getMessage() {
		return _message;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return _message;
	}

	/**
	 * Creates builder to build {@link MessageBusEvent}.
	 * 
	 * @return created builder
	 */
	public static Builder builder() {
		return new Builder();
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Builder to build {@link MessageBusEvent}.
	 */
	public static class Builder extends ApplicationBusEvent.Builder implements MessageBuilder<Builder> {

		private String message;

		/**
		 * Instantiates a new builder.
		 */
		protected Builder() {}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withAction( Enum<?> aAction ) {
			action = aAction;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withMessage( String aMessage ) {
			message = aMessage;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withSource( ApplicationBus aSource ) {
			source = aSource;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withAlias( String aAlias ) {
			eventMetaData.withAlias( aAlias );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withChannel( String aChannel ) {
			eventMetaData.withChannel( aChannel );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withGroup( String aGroup ) {
			eventMetaData.withGroup( aGroup );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withUniversalId( String aUid ) {
			eventMetaData.withUniversalId( aUid );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder withPublisherType( Class<?> aPublisherType ) {
			eventMetaData.withPublisherType( aPublisherType );
			return this;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * Merges all not-null values of the provided {@link EventMetaData}
		 * instance into this {@link Builder} instance.
		 * 
		 * @param aEventMetaData The {@link EventMetaData} instance to be merged
		 *        into this {@link Builder} instance.
		 * 
		 * @return This {@link Builder} instance as of the builder pattern.
		 */
		@Override
		public Builder withMetaData( EventMetaData aEventMetaData ) {
			eventMetaData.withMetaData( aEventMetaData );
			return this;
		}

		/**
		 * Builder method of the builder.
		 * 
		 * @return The built instance.
		 */
		@Override
		public MessageBusEvent build() {
			return new MessageBusEvent( this );
		}
	}
}
