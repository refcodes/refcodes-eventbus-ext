// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.eventbus.ext.application;

import static org.refcodes.eventbus.ext.application.ApplicationBusSugar.*;

import org.refcodes.eventbus.DispatchStrategy;
import org.refcodes.eventbus.EventDispatcher;
import org.refcodes.observer.EventMatcher;
import org.refcodes.observer.EventMetaData;
import org.refcodes.observer.Observable;
import org.refcodes.observer.Observer;

/**
 * The {@link PayloadBus} extends the {@link ApplicationBus} with convenience
 * functionality common to everyday application and service development
 * regarding dispatching of payloads.
 */
public interface PayloadBus extends EventDispatcher<ApplicationBusEvent, Observer<ApplicationBusEvent>, ApplicationBusEventMatcher, EventMetaData, String> {

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link PayloadBusEvent} instances with the given attributes.
	 *
	 * @param <P> The type of the payload.
	 * @param aType The type of the payload to be carried by the event.
	 * @param aPublisherType The type of the event publisher.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	@SuppressWarnings("unchecked")
	default <P> String onPayload( Class<P> aType, Class<?> aPublisherType, Observer<PayloadBusEvent<P>> aObserver ) {
		// Do it manually as generic types give up here:
		return subscribe( and( isPayloadTypeOf( aType ), isEventTypeOf( PayloadBusEvent.class ), isPublisherTypeOf( aPublisherType ) ), ( ApplicationBusEvent aEvent ) -> aObserver.onEvent( (PayloadBusEvent<P>) aEvent ) );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link PayloadBusEvent} instances with the given attributes.
	 *
	 * @param <P> The type of the payload.
	 * @param aType The type of the payload to be carried by the event.
	 * @param aAction The action property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	@SuppressWarnings("unchecked")
	default <P> String onPayload( Class<P> aType, Enum<?> aAction, Class<?> aPublisherType, Observer<PayloadBusEvent<P>> aObserver ) {
		// Do it manually as generic types give up here:
		return subscribe( and( isPayloadTypeOf( aType ), isEventTypeOf( PayloadBusEvent.class ), actionEqualWith( aAction ), isPublisherTypeOf( aPublisherType ) ), ( ApplicationBusEvent aEvent ) -> aObserver.onEvent( (PayloadBusEvent<P>) aEvent ) );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link PayloadBusEvent} instances with the given attributes. Your
	 * {@link Observable} may be of the required type!
	 *
	 * @param <P> The type of the payload.
	 * @param aType The type of the payload to be carried by the event.
	 * @param aAction The action property.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	@SuppressWarnings("unchecked")
	default <P> String onPayload( Class<P> aType, Enum<?> aAction, Observer<PayloadBusEvent<P>> aObserver ) {
		// Do it manually as generic types give up here:
		return subscribe( and( isPayloadTypeOf( aType ), isEventTypeOf( PayloadBusEvent.class ), actionEqualWith( aAction ) ), ( ApplicationBusEvent aEvent ) -> aObserver.onEvent( (PayloadBusEvent<P>) aEvent ) );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link PayloadBusEvent} instances with the given attributes.
	 *
	 * @param <P> The type of the payload.
	 * @param aType The type of the payload to be carried by the event.
	 * @param aAction The action property.
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	@SuppressWarnings("unchecked")
	default <P> String onPayload( Class<P> aType, Enum<?> aAction, String aChannel, Observer<PayloadBusEvent<P>> aObserver ) {
		// Do it manually as generic types give up here:
		return subscribe( and( isPayloadTypeOf( aType ), isEventTypeOf( PayloadBusEvent.class ), actionEqualWith( aAction ), channelEqualWith( aChannel ) ), ( ApplicationBusEvent aEvent ) -> aObserver.onEvent( (PayloadBusEvent<P>) aEvent ) );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link PayloadBusEvent} instances with the given attributes.
	 *
	 * @param <P> The type of the payload.
	 * @param aType The type of the payload to be carried by the event.
	 * @param aAction The action property.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	@SuppressWarnings("unchecked")
	default <P> String onPayload( Class<P> aType, Enum<?> aAction, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, Observer<PayloadBusEvent<P>> aObserver ) {
		// Do it manually as generic types give up here: 
		return subscribe( and( isPayloadTypeOf( aType ), isEventTypeOf( PayloadBusEvent.class ), actionEqualWith( aAction ), aliasEqualWith( aAlias ), groupEqualWith( aGroup ), channelEqualWith( aChannel ), uidIdEqualWith( aUid ), isPublisherTypeOf( aPublisherType ) ), ( ApplicationBusEvent aEvent ) -> aObserver.onEvent( (PayloadBusEvent<P>) aEvent ) );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link PayloadBusEvent} instances.
	 *
	 * @param <P> The type of the payload.
	 * @param aType The type of the payload to be carried by the event.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	@SuppressWarnings("unchecked")
	default <P> String onPayload( Class<P> aType, Observer<PayloadBusEvent<P>> aObserver ) {
		// Do it manually as generic types give up here:
		return subscribe( and( isPayloadTypeOf( aType ), isEventTypeOf( PayloadBusEvent.class ) ), ( ApplicationBusEvent aEvent ) -> aObserver.onEvent( (PayloadBusEvent<P>) aEvent ) );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link PayloadBusEvent} instances with the given attributes.
	 *
	 * @param <P> The type of the payload.
	 * @param aType The type of the payload to be carried by the event.
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	@SuppressWarnings("unchecked")
	default <P> String onPayload( Class<P> aType, String aChannel, Observer<PayloadBusEvent<P>> aObserver ) {
		// Do it manually as generic types give up here:
		return subscribe( and( isPayloadTypeOf( aType ), isEventTypeOf( PayloadBusEvent.class ), channelEqualWith( aChannel ) ), ( ApplicationBusEvent aEvent ) -> aObserver.onEvent( (PayloadBusEvent<P>) aEvent ) );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link PayloadBusEvent} instances with the given attributes.
	 *
	 * @param <P> The type of the payload.
	 * @param aType The type of the payload to be carried by the event.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	@SuppressWarnings("unchecked")
	default <P> String onPayload( Class<P> aType, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, Observer<PayloadBusEvent<P>> aObserver ) {
		// Do it manually as generic types give up here:
		return subscribe( and( isPayloadTypeOf( aType ), isEventTypeOf( PayloadBusEvent.class ), aliasEqualWith( aAlias ), groupEqualWith( aGroup ), channelEqualWith( aChannel ), uidIdEqualWith( aUid ), isPublisherTypeOf( aPublisherType ) ), ( ApplicationBusEvent aEvent ) -> aObserver.onEvent( (PayloadBusEvent<P>) aEvent ) );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link PayloadBusEvent} instances with the given attributes.
	 *
	 * @param aAction The action property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onPayload( Enum<?> aAction, Class<?> aPublisherType, Observer<PayloadBusEvent<?>> aObserver ) {
		return subscribe( and( isEventTypeOf( PayloadBusEvent.class ), actionEqualWith( aAction ), isPublisherTypeOf( aPublisherType ) ), new Observer<ApplicationBusEvent>() {
			@Override
			public void onEvent( ApplicationBusEvent aEvent ) {
				// Do it manually as generic types give up here:
				aObserver.onEvent( (PayloadBusEvent<?>) aEvent );
			}
		} );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link PayloadBusEvent} instances with the given attributes. Your
	 * {@link Observable} may be of the required type!
	 *
	 * @param aAction The action property.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onPayload( Enum<?> aAction, Observer<PayloadBusEvent<?>> aObserver ) {
		return subscribe( and( isEventTypeOf( PayloadBusEvent.class ), actionEqualWith( aAction ) ), new Observer<ApplicationBusEvent>() {
			@Override
			public void onEvent( ApplicationBusEvent aEvent ) {
				// Do it manually as generic types give up here:
				aObserver.onEvent( (PayloadBusEvent<?>) aEvent );
			}
		} );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link PayloadBusEvent} instances with the given attributes.
	 *
	 * @param aAction The action property.
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onPayload( Enum<?> aAction, String aChannel, Observer<PayloadBusEvent<?>> aObserver ) {
		return subscribe( and( isEventTypeOf( PayloadBusEvent.class ), actionEqualWith( aAction ), channelEqualWith( aChannel ) ), new Observer<ApplicationBusEvent>() {
			@Override
			public void onEvent( ApplicationBusEvent aEvent ) {
				// Do it manually as generic types give up here:
				aObserver.onEvent( (PayloadBusEvent<?>) aEvent );
			}
		} );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link PayloadBusEvent} instances with the given attributes.
	 *
	 * @param aAction The action property.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onPayload( Enum<?> aAction, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, Observer<PayloadBusEvent<?>> aObserver ) {
		return subscribe( and( isEventTypeOf( PayloadBusEvent.class ), actionEqualWith( aAction ), aliasEqualWith( aAlias ), groupEqualWith( aGroup ), channelEqualWith( aChannel ), uidIdEqualWith( aUid ), isPublisherTypeOf( aPublisherType ) ), new Observer<ApplicationBusEvent>() {
			@Override
			public void onEvent( ApplicationBusEvent aEvent ) {
				// Do it manually as generic types give up here: 
				aObserver.onEvent( (PayloadBusEvent<?>) aEvent );
			}
		} );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link PayloadBusEvent} instances.
	 *
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onPayload( Observer<PayloadBusEvent<?>> aObserver ) {
		return subscribe( isEventTypeOf( PayloadBusEvent.class ), new Observer<ApplicationBusEvent>() {
			@Override
			public void onEvent( ApplicationBusEvent aEvent ) {
				// Do it manually as generic types give up here:
				aObserver.onEvent( (PayloadBusEvent<?>) aEvent );
			}
		} );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link PayloadBusEvent} instances with the given attributes.
	 *
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onPayload( String aChannel, Observer<PayloadBusEvent<?>> aObserver ) {
		return subscribe( and( isEventTypeOf( PayloadBusEvent.class ), channelEqualWith( aChannel ) ), new Observer<ApplicationBusEvent>() {
			@Override
			public void onEvent( ApplicationBusEvent aEvent ) {
				// Do it manually as generic types give up here:
				aObserver.onEvent( (PayloadBusEvent<?>) aEvent );
			}
		} );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link PayloadBusEvent} instances with the given attributes.
	 *
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onPayload( String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, Observer<PayloadBusEvent<?>> aObserver ) {
		return subscribe( and( isEventTypeOf( PayloadBusEvent.class ), aliasEqualWith( aAlias ), groupEqualWith( aGroup ), channelEqualWith( aChannel ), uidIdEqualWith( aUid ), isPublisherTypeOf( aPublisherType ) ), new Observer<ApplicationBusEvent>() {
			@Override
			public void onEvent( ApplicationBusEvent aEvent ) {
				// Do it manually as generic types give up here:
				aObserver.onEvent( (PayloadBusEvent<?>) aEvent );
			}
		} );
	}

	/**
	 * Fires an event with the properties set for the
	 * {@link PayloadBusEvent.Builder} instance upon finishing of by invoking
	 * {@link PayloadBusEvent.Builder#build()}.
	 * 
	 * @param <P> The type of the payload being carried by this event.
	 * 
	 * @return The {@link PayloadBusEvent} class' builder.
	 */
	default <P> PayloadBusEvent.Builder<P> publishPayload() {
		PayloadBusEvent.Builder<P> theBuilder = new PayloadBusEvent.Builder<P>() {
			@Override
			public PayloadBusEvent<P> build() {
				PayloadBusEvent<P> theEvent = super.build();
				publishEvent( theEvent );
				return theEvent;
			}
		};
		return theBuilder;
	}

	/**
	 * Fires an event with the properties set for the
	 * {@link PayloadBusEvent.Builder} instance upon finishing of by invoking
	 * {@link PayloadBusEvent.Builder#build()}.
	 * 
	 * @param <P> The type of the payload being carried by this event.
	 * 
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 * 
	 * @return The {@link PayloadBusEvent} class' builder.
	 */
	default <P> PayloadBusEvent.Builder<P> publishPayload( DispatchStrategy aStrategy ) {
		PayloadBusEvent.Builder<P> theBuilder = new PayloadBusEvent.Builder<P>() {
			@Override
			public PayloadBusEvent<P> build() {
				PayloadBusEvent<P> theEvent = super.build();
				publishEvent( theEvent, aStrategy );
				return theEvent;
			}
		};
		return theBuilder;
	}

	/**
	 * Publishes an event with the provided payload and the given attributes.
	 *
	 * @param <P> The type of the payload to carry.
	 * @param aAction The action which the event represents.
	 * @param aPayload the payload
	 * @param aType The type of the payload to be carried by the event.
	 */
	default <P> void publishPayload( Enum<?> aAction, P aPayload, Class<P> aType ) {
		publishEvent( new PayloadBusEvent<P>( aAction, aPayload, aType, getClass(), (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided payload and the given attributes.
	 *
	 * @param <P> The type of the payload to carry.
	 * @param aAction The action which this represents.
	 * @param aPayload the payload
	 * @param aType The type of the payload to be carried by the event.
	 * @param aPublisherType The type of the event publisher.
	 */
	default <P> void publishPayload( Enum<?> aAction, P aPayload, Class<P> aType, Class<?> aPublisherType ) {
		publishEvent( new PayloadBusEvent<P>( aAction, aPayload, aType, aPublisherType, (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided payload and the given attributes.
	 *
	 * @param <P> The type of the payload to carry.
	 * @param aAction The action which this represents.
	 * @param aPayload the payload
	 * @param aType The type of the payload to be carried by the event.
	 * @param aPublisherType The type of the event publisher.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default <P> void publishPayload( Enum<?> aAction, P aPayload, Class<P> aType, Class<?> aPublisherType, DispatchStrategy aStrategy ) {
		publishEvent( new PayloadBusEvent<P>( aAction, aPayload, aType, aPublisherType, (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided payload and the given attributes.
	 *
	 * @param <P> The type of the payload to carry.
	 * @param aAction The action which the event represents.
	 * @param aPayload the payload
	 * @param aType The type of the payload to be carried by the event.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default <P> void publishPayload( Enum<?> aAction, P aPayload, Class<P> aType, DispatchStrategy aStrategy ) {
		publishEvent( new PayloadBusEvent<P>( aAction, aPayload, aType, getClass(), (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided payload and the given attributes.
	 *
	 * @param <P> The type of the payload to carry.
	 * @param aAction The action which the event represents.
	 * @param aPayload the payload
	 * @param aType The type of the payload to be carried by the event.
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 */
	default <P> void publishPayload( Enum<?> aAction, P aPayload, Class<P> aType, EventMetaData aEventMetaData ) {
		publishEvent( new PayloadBusEvent<P>( aAction, aPayload, aType, aEventMetaData, (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided payload and the given attributes.
	 *
	 * @param <P> The type of the payload to carry.
	 * @param aAction The action which the event represents.
	 * @param aPayload the payload
	 * @param aType The type of the payload to be carried by the event.
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default <P> void publishPayload( Enum<?> aAction, P aPayload, Class<P> aType, EventMetaData aEventMetaData, DispatchStrategy aStrategy ) {
		publishEvent( new PayloadBusEvent<P>( aAction, aPayload, aType, aEventMetaData, (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided payload and the given attributes.
	 *
	 * @param <P> The type of the payload to carry.
	 * @param aAction The action which this represents.
	 * @param aPayload the payload
	 * @param aType The type of the payload to be carried by the event.
	 * @param aChannel The channel name on which the event is receivable.
	 */
	default <P> void publishPayload( Enum<?> aAction, P aPayload, Class<P> aType, String aChannel ) {
		publishEvent( new PayloadBusEvent<P>( aAction, aPayload, aType, aChannel, getClass(), (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided payload and the given attributes.
	 *
	 * @param <P> The type of the payload to carry.
	 * @param aAction The action which this represents.
	 * @param aPayload the payload
	 * @param aType The type of the payload to be carried by the event.
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default <P> void publishPayload( Enum<?> aAction, P aPayload, Class<P> aType, String aChannel, DispatchStrategy aStrategy ) {
		publishEvent( new PayloadBusEvent<P>( aAction, aPayload, aType, aChannel, getClass(), (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided payload and the given attributes.
	 *
	 * @param <P> The type of the payload to carry.
	 * @param aAction The action which this represents.
	 * @param aPayload the payload
	 * @param aType The type of the payload to be carried by the event.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 */
	default <P> void publishPayload( Enum<?> aAction, P aPayload, Class<P> aType, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType ) {
		publishEvent( new PayloadBusEvent<P>( aAction, aPayload, aType, aAlias, aGroup, aChannel, aUid, aPublisherType, (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided payload and the given attributes.
	 *
	 * @param <P> The type of the payload to carry.
	 * @param aAction The action which this represents.
	 * @param aPayload the payload
	 * @param aType The type of the payload to be carried by the event.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default <P> void publishPayload( Enum<?> aAction, P aPayload, Class<P> aType, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, DispatchStrategy aStrategy ) {
		publishEvent( new PayloadBusEvent<P>( aAction, aPayload, aType, aAlias, aGroup, aChannel, aUid, aPublisherType, (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided payload and the given attributes.
	 * 
	 * @param <P> The type of the payload to carry.
	 * @param aAction The action which the event represents.
	 * @param aPayload The payload to be carried by the event.
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 */
	default <P> void publishPayload( Enum<?> aAction, P aPayload, EventMetaData aEventMetaData ) {
		publishEvent( new PayloadBusEvent<P>( aAction, aPayload, aEventMetaData, (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided payload and the given attributes.
	 * 
	 * @param <P> The type of the payload to carry.
	 * @param aAction The action which the event represents.
	 * @param aPayload The payload to be carried by the event.
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default <P> void publishPayload( Enum<?> aAction, P aPayload, EventMetaData aEventMetaData, DispatchStrategy aStrategy ) {
		publishEvent( new PayloadBusEvent<P>( aAction, aPayload, aEventMetaData, (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided payload and the given attributes.
	 * 
	 * @param <P> The type of the payload to carry.
	 * @param aAction The action which this represents.
	 * @param aPayload The payload to be carried by the event.
	 * @param aChannel The channel name on which the event is receivable.
	 */
	default <P> void publishPayload( Enum<?> aAction, P aPayload, String aChannel ) {
		publishEvent( new PayloadBusEvent<P>( aAction, aPayload, aChannel, getClass(), (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided payload and the given attributes.
	 * 
	 * @param <P> The type of the payload to carry.
	 * @param aAction The action which this represents.
	 * @param aPayload The payload to be carried by the event.
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default <P> void publishPayload( Enum<?> aAction, P aPayload, String aChannel, DispatchStrategy aStrategy ) {
		publishEvent( new PayloadBusEvent<P>( aAction, aPayload, aChannel, getClass(), (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided payload and the given attributes.
	 * 
	 * @param <P> The type of the payload to carry.
	 * @param aAction The action which this represents.
	 * @param aPayload The payload to be carried by the event.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 */
	default <P> void publishPayload( Enum<?> aAction, P aPayload, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType ) {
		publishEvent( new PayloadBusEvent<P>( aAction, aPayload, aAlias, aGroup, aChannel, aUid, aPublisherType, (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided payload and the given attributes.
	 * 
	 * @param <P> The type of the payload to carry.
	 * @param aAction The action which this represents.
	 * @param aPayload The payload to be carried by the event.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default <P> void publishPayload( Enum<?> aAction, P aPayload, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, DispatchStrategy aStrategy ) {
		publishEvent( new PayloadBusEvent<P>( aAction, aPayload, aAlias, aGroup, aChannel, aUid, aPublisherType, (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided payload and the given attributes.
	 * 
	 * @param <P> The type of the payload to carry.
	 * @param aPayload The payload to be carried by the event.
	 */
	default <P> void publishPayload( P aPayload ) {
		publishEvent( new PayloadBusEvent<P>( aPayload, (Class<P>) null, getClass(), (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided payload and the given attributes.
	 *
	 * @param <P> The type of the payload to carry.
	 * @param aPayload the payload
	 * @param aType The type of the payload to be carried by the event.
	 */
	default <P> void publishPayload( P aPayload, Class<P> aType ) {
		publishEvent( new PayloadBusEvent<P>( aPayload, aType, getClass(), (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided payload and the given attributes.
	 *
	 * @param <P> The type of the payload to carry.
	 * @param aPayload the payload
	 * @param aType The type of the payload to be carried by the event.
	 * @param aPublisherType The type of the event publisher.
	 */
	default <P> void publishPayload( P aPayload, Class<P> aType, Class<?> aPublisherType ) {
		publishEvent( new PayloadBusEvent<P>( aPayload, aType, aPublisherType, (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided payload and the given attributes.
	 *
	 * @param <P> The type of the payload to carry.
	 * @param aPayload the payload
	 * @param aType The type of the payload to be carried by the event.
	 * @param aPublisherType The type of the event publisher.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default <P> void publishPayload( P aPayload, Class<P> aType, Class<?> aPublisherType, DispatchStrategy aStrategy ) {
		publishEvent( new PayloadBusEvent<P>( aPayload, aType, aPublisherType, (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided payload and the given attributes.
	 *
	 * @param <P> The type of the payload to carry.
	 * @param aPayload the payload
	 * @param aType The type of the payload to be carried by the event.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default <P> void publishPayload( P aPayload, Class<P> aType, DispatchStrategy aStrategy ) {
		publishEvent( new PayloadBusEvent<P>( aPayload, aType, getClass(), (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided payload and the given attributes.
	 *
	 * @param <P> The type of the payload to carry.
	 * @param aPayload the payload
	 * @param aType The type of the payload to be carried by the event.
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 */
	default <P> void publishPayload( P aPayload, Class<P> aType, EventMetaData aEventMetaData ) {
		publishEvent( new PayloadBusEvent<P>( aPayload, aType, aEventMetaData, (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided payload and the given attributes.
	 *
	 * @param <P> The type of the payload to carry.
	 * @param aPayload the payload
	 * @param aType The type of the payload to be carried by the event.
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default <P> void publishPayload( P aPayload, Class<P> aType, EventMetaData aEventMetaData, DispatchStrategy aStrategy ) {
		publishEvent( new PayloadBusEvent<P>( aPayload, aType, aEventMetaData, (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided payload and the given attributes.
	 *
	 * @param <P> The type of the payload to carry.
	 * @param aPayload the payload
	 * @param aType The type of the payload to be carried by the event.
	 * @param aChannel The channel name on which the event is receivable.
	 */
	default <P> void publishPayload( P aPayload, Class<P> aType, String aChannel ) {
		publishEvent( new PayloadBusEvent<P>( aPayload, aType, aChannel, getClass(), (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided payload and the given attributes.
	 *
	 * @param <P> The type of the payload to carry.
	 * @param aPayload the payload
	 * @param aType The type of the payload to be carried by the event.
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default <P> void publishPayload( P aPayload, Class<P> aType, String aChannel, DispatchStrategy aStrategy ) {
		publishEvent( new PayloadBusEvent<P>( aPayload, aType, aChannel, getClass(), (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided payload and the given attributes.
	 *
	 * @param <P> The type of the payload to carry.
	 * @param aPayload the payload
	 * @param aType The type of the payload to be carried by the event.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 */
	default <P> void publishPayload( P aPayload, Class<P> aType, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType ) {
		publishEvent( new PayloadBusEvent<P>( aPayload, aType, aAlias, aGroup, aChannel, aUid, aPublisherType, (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided payload and the given attributes.
	 *
	 * @param <P> The type of the payload to carry.
	 * @param aPayload the payload
	 * @param aType The type of the payload to be carried by the event.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default <P> void publishPayload( P aPayload, Class<P> aType, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, DispatchStrategy aStrategy ) {
		publishEvent( new PayloadBusEvent<P>( aPayload, aType, aAlias, aGroup, aChannel, aUid, aPublisherType, (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided payload and the given attributes.
	 * 
	 * @param <P> The type of the payload to carry.
	 * @param aPayload The payload to be carried by the event.
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 */
	default <P> void publishPayload( P aPayload, EventMetaData aEventMetaData ) {
		publishEvent( new PayloadBusEvent<P>( aPayload, aEventMetaData, (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided payload and the given attributes.
	 * 
	 * @param <P> The type of the payload to carry.
	 * @param aPayload The payload to be carried by the event.
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default <P> void publishPayload( P aPayload, EventMetaData aEventMetaData, DispatchStrategy aStrategy ) {
		publishEvent( new PayloadBusEvent<P>( aPayload, aEventMetaData, (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided payload and the given attributes.
	 * 
	 * @param <P> The type of the payload to carry.
	 * @param aPayload The payload to be carried by the event.
	 * @param aChannel The channel name on which the event is receivable.
	 */
	default <P> void publishPayload( P aPayload, String aChannel ) {
		publishEvent( new PayloadBusEvent<P>( aPayload, aChannel, getClass(), (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided payload and the given attributes.
	 * 
	 * @param <P> The type of the payload to carry.
	 * @param aPayload The payload to be carried by the event.
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default <P> void publishPayload( P aPayload, String aChannel, DispatchStrategy aStrategy ) {
		publishEvent( new PayloadBusEvent<P>( aPayload, aChannel, getClass(), (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes an event with the provided payload and the given attributes.
	 * 
	 * @param <P> The type of the payload to carry.
	 * @param aPayload The payload to be carried by the event.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 */
	default <P> void publishPayload( P aPayload, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType ) {
		publishEvent( new PayloadBusEvent<P>( aPayload, aAlias, aGroup, aChannel, aUid, aPublisherType, (ApplicationBus) this ) );
	}

	/**
	 * Publishes an event with the provided payload and the given attributes.
	 * 
	 * @param <P> The type of the payload to carry.
	 * @param aPayload The payload to be carried by the event.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default <P> void publishPayload( P aPayload, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, DispatchStrategy aStrategy ) {
		publishEvent( new PayloadBusEvent<P>( aPayload, aAlias, aGroup, aChannel, aUid, aPublisherType, (ApplicationBus) this ), aStrategy );
	}
}
