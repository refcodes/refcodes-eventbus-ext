// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.eventbus.ext.application;

import static org.refcodes.eventbus.ext.application.ApplicationBusSugar.*;

import org.refcodes.eventbus.DispatchStrategy;
import org.refcodes.eventbus.EventDispatcher;
import org.refcodes.observer.EventMatcher;
import org.refcodes.observer.EventMetaData;
import org.refcodes.observer.Observable;
import org.refcodes.observer.Observer;

/**
 * The {@link ResumeBus} extends the {@link ApplicationBus} with convenience
 * functionality common to everyday application and service development
 * regarding dispatching of {@link ResumeBusEvent} instances.
 */
public interface ResumeBus extends EventDispatcher<ApplicationBusEvent, Observer<ApplicationBusEvent>, ApplicationBusEventMatcher, EventMetaData, String> {

	/**
	 * Publishes a resume event with the given attributes. This method uses the
	 * {@link DispatchStrategy#PARALLEL} as we assume all observers already
	 * being initialized.
	 * 
	 * @param aPublisherType The type of the event publisher.
	 */
	default void publishResume( Class<?> aPublisherType ) {
		publishEvent( new ResumeBusEvent( aPublisherType, (ApplicationBus) this ), DispatchStrategy.PARALLEL );
	}

	/**
	 * Publishes a resume event with the given attributes. This method uses the
	 * {@link DispatchStrategy#PARALLEL} as we assume all observers already
	 * being initialized.
	 * 
	 * @param aChannel The channel name on which the event is receivable.
	 */
	default void publishResume( String aChannel ) {
		publishEvent( new ResumeBusEvent( aChannel, getClass(), (ApplicationBus) this ), DispatchStrategy.PARALLEL );
	}

	/**
	 * Publishes a resume event with the given attributes. This method uses the
	 * {@link DispatchStrategy#PARALLEL} as we assume all observers already
	 * being initialized.
	 * 
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 */
	default void publishResume( String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType ) {
		publishEvent( new ResumeBusEvent( aAlias, aGroup, aChannel, aUid, aPublisherType, (ApplicationBus) this ), DispatchStrategy.PARALLEL );
	}

	/**
	 * Publishes a resume event with the given attributes. This method uses the
	 * {@link DispatchStrategy#PARALLEL} as we assume all observers already
	 * being initialized.
	 * 
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 */
	default void publishResume( EventMetaData aEventMetaData ) {
		publishEvent( new ResumeBusEvent( aEventMetaData, (ApplicationBus) this ), DispatchStrategy.PARALLEL );
	}

	/**
	 * Publishes a resume event with the given attributes. This method uses the
	 * {@link DispatchStrategy#PARALLEL} as we assume all observers already
	 * being initialized.
	 */
	default void publishResume() {
		publishEvent( new ResumeBusEvent( getClass(), (ApplicationBus) this ), DispatchStrategy.PARALLEL );
	}

	/**
	 * Publishes a resume event with the given attributes.
	 * 
	 * @param aPublisherType The type of the event publisher.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default void publishResume( Class<?> aPublisherType, DispatchStrategy aStrategy ) {
		publishEvent( new ResumeBusEvent( aPublisherType, (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes a resume event with the given attributes.
	 * 
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default void publishResume( String aChannel, DispatchStrategy aStrategy ) {
		publishEvent( new ResumeBusEvent( aChannel, getClass(), (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes a resume event with the given attributes.
	 * 
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default void publishResume( String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, DispatchStrategy aStrategy ) {
		publishEvent( new ResumeBusEvent( aAlias, aGroup, aChannel, aUid, aPublisherType, (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes a resume event with the given attributes.
	 * 
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default void publishResume( EventMetaData aEventMetaData, DispatchStrategy aStrategy ) {
		publishEvent( new ResumeBusEvent( aEventMetaData, (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Publishes a resume event with the given attributes.
	 * 
	 * @param aStrategy The {@link DispatchStrategy} to use when dispatching the
	 *        event.
	 */
	default void publishResume( DispatchStrategy aStrategy ) {
		publishEvent( new ResumeBusEvent( getClass(), (ApplicationBus) this ), aStrategy );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link ResumeBusEvent} instances.
	 *
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onResume( Observer<ResumeBusEvent> aObserver ) {
		return subscribe( ResumeBusEvent.class, aObserver );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link ResumeBusEvent} instances with the given attributes.
	 *
	 * @param aPublisherType The type of the event publisher.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onResume( Class<?> aPublisherType, Observer<ResumeBusEvent> aObserver ) {
		return subscribe( ResumeBusEvent.class, isPublisherTypeOf( aPublisherType ), aObserver );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link ResumeBusEvent} instances with the given attributes.
	 *
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onResume( String aChannel, Observer<ResumeBusEvent> aObserver ) {
		return subscribe( ResumeBusEvent.class, channelEqualWith( aChannel ), aObserver );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link ResumeBusEvent} instances with the given attributes.
	 *
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onResume( String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, Observer<ResumeBusEvent> aObserver ) {
		return subscribe( ResumeBusEvent.class, and( aliasEqualWith( aAlias ), groupEqualWith( aGroup ), channelEqualWith( aChannel ), uidIdEqualWith( aUid ), isPublisherTypeOf( aPublisherType ) ), aObserver );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link ResumeBusEvent} instances with the given attributes.
	 *
	 * @param aAction The action property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onResume( Enum<?> aAction, Class<?> aPublisherType, Observer<ResumeBusEvent> aObserver ) {
		return subscribe( ResumeBusEvent.class, and( actionEqualWith( aAction ), isPublisherTypeOf( aPublisherType ) ), aObserver );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link ResumeBusEvent} instances with the given attributes.
	 *
	 * @param aAction The action property.
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onResume( Enum<?> aAction, String aChannel, Observer<ResumeBusEvent> aObserver ) {
		return subscribe( ResumeBusEvent.class, and( actionEqualWith( aAction ), channelEqualWith( aChannel ) ), aObserver );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link ResumeBusEvent} instances with the given attributes.
	 *
	 * @param aAction The action property.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onResume( Enum<?> aAction, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, Observer<ResumeBusEvent> aObserver ) {
		return subscribe( ResumeBusEvent.class, and( actionEqualWith( aAction ), aliasEqualWith( aAlias ), groupEqualWith( aGroup ), channelEqualWith( aChannel ), uidIdEqualWith( aUid ), isPublisherTypeOf( aPublisherType ) ), aObserver );
	}

	/**
	 * Similar to the more generic method
	 * {@link #subscribe(EventMatcher, Observer)} THOUGH subscribes for
	 * {@link ResumeBusEvent} instances with the given attributes. Your
	 * {@link Observable} may be of the required type!
	 *
	 * @param aAction The action property.
	 * @param aObserver The observer to be notified.
	 * 
	 * @return A handle to unsubscribe this combination.
	 */
	default String onResume( Enum<?> aAction, Observer<ResumeBusEvent> aObserver ) {
		return subscribe( ResumeBusEvent.class, actionEqualWith( aAction ), aObserver );
	}
}
