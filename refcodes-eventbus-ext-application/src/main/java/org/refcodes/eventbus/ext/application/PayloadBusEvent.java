// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.eventbus.ext.application;

import org.refcodes.mixin.PayloadAccessor;
import org.refcodes.mixin.TypeAccessor;
import org.refcodes.observer.EventMetaData;

/**
 * The {@link PayloadBusEvent} represents an event indication a payload.
 * 
 * @param <P> The type of the payload being carried by this event.
 */
public class PayloadBusEvent<P> extends ApplicationBusEvent implements PayloadAccessor<P>, TypeAccessor<P> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected P _payload;
	protected Class<P> _type;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	private PayloadBusEvent( Builder<P> builder ) {
		this( builder.action, builder.payload, builder.type, builder.eventMetaData.build(), builder.source );
	}

	/**
	 * Constructs an event with the given Meta-Data.
	 * 
	 * @param aAction The action which this represents.
	 * @param aPayload The payload to carry.
	 * @param aSource The source from which this event originated.
	 */
	public PayloadBusEvent( Enum<?> aAction, P aPayload, ApplicationBus aSource ) {
		this( aAction, aPayload, null, null, null, null, null, null, aSource );
	}

	/**
	 * Constructs an event with the given Meta-Data.
	 *
	 * @param aAction The action which this represents.
	 * @param aPayload The payload to carry.
	 * @param aType The type of the payload.
	 * @param aSource The source from which this event originated.
	 */
	public PayloadBusEvent( Enum<?> aAction, P aPayload, Class<P> aType, ApplicationBus aSource ) {
		this( aAction, aPayload, aType, null, null, null, null, null, aSource );
	}

	/**
	 * Constructs an event with predefined values for the according properties
	 * retrieved from the caller's class.
	 *
	 * @param aAction The action which this represents.
	 * @param aPayload The payload to carry.
	 * @param aType The type of the payload.
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source from which this event originated.
	 */
	public PayloadBusEvent( Enum<?> aAction, P aPayload, Class<P> aType, Class<?> aPublisherType, ApplicationBus aSource ) {
		this( aAction, aPayload, aType, null, null, null, null, aPublisherType, aSource );
	}

	/**
	 * Constructs an event with predefined values for the according properties
	 * retrieved from the caller's class.
	 *
	 * @param aAction The action which this represents.
	 * @param aPayload The payload to carry.
	 * @param aType The type of the payload.
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aSource The source from which this event originated.
	 */
	public PayloadBusEvent( Enum<?> aAction, P aPayload, Class<P> aType, String aChannel, ApplicationBus aSource ) {
		this( aAction, aPayload, aType, null, null, aChannel, null, null, aSource );
	}

	/**
	 * Constructs an event with the given values for the according properties.
	 *
	 * @param aAction The action which this represents.
	 * @param aPayload The payload to carry.
	 * @param aType The type of the payload.
	 * @param aChannel The channel property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source from which this event originated.
	 */
	public PayloadBusEvent( Enum<?> aAction, P aPayload, Class<P> aType, String aChannel, Class<?> aPublisherType, ApplicationBus aSource ) {
		this( aAction, aPayload, aType, null, null, aChannel, null, aPublisherType, aSource );
	}

	/**
	 * Constructs an event with the given Meta-Data.
	 * 
	 * @param aAction The action which this represents.
	 * @param aPayload The payload to carry.
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 * @param aSource The source from which this event originated.
	 */
	public PayloadBusEvent( Enum<?> aAction, P aPayload, EventMetaData aEventMetaData, ApplicationBus aSource ) {
		this( aAction, aPayload, null, aEventMetaData, aSource );
	}

	/**
	 * Constructs an event with predefined values for the according properties
	 * retrieved from the caller's class.
	 * 
	 * @param aAction The action which this represents.
	 * @param aPayload The payload to carry.
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aSource The source from which this event originated.
	 */
	public PayloadBusEvent( Enum<?> aAction, P aPayload, String aChannel, ApplicationBus aSource ) {
		this( aAction, aPayload, null, null, null, aChannel, null, null, aSource );
	}

	/**
	 * Constructs an event with the given values for the according properties.
	 * 
	 * @param aAction The action which this represents.
	 * @param aPayload The payload to carry.
	 * @param aChannel The channel property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source from which this event originated.
	 */
	public PayloadBusEvent( Enum<?> aAction, P aPayload, String aChannel, Class<?> aPublisherType, ApplicationBus aSource ) {
		this( aAction, aPayload, null, null, null, aChannel, null, aPublisherType, aSource );
	}

	/**
	 * Constructs an event with the given values for the according properties.
	 * 
	 * @param aAction The action which this represents.
	 * @param aPayload The payload to carry.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source from which this event originated.
	 */
	public PayloadBusEvent( Enum<?> aAction, P aPayload, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, ApplicationBus aSource ) {
		this( aAction, aPayload, null, aAlias, aGroup, aChannel, aUid, aPublisherType, aSource );
	}

	/**
	 * Constructs an event with the given Meta-Data.
	 *
	 * @param aPayload The payload to carry.
	 * @param aSource The source from which this event originated.
	 */
	public PayloadBusEvent( P aPayload, ApplicationBus aSource ) {
		this( null, aPayload, null, null, null, null, null, null, aSource );
	}

	/**
	 * Constructs an event with the given Meta-Data.
	 *
	 * @param aPayload The payload to carry.
	 * @param aType The type of the payload.
	 * @param aSource The source from which this event originated.
	 */
	public PayloadBusEvent( P aPayload, Class<P> aType, ApplicationBus aSource ) {
		this( null, aPayload, aType, null, null, null, null, null, aSource );
	}

	/**
	 * Constructs an event with predefined values for the according properties
	 * retrieved from the caller's class.
	 *
	 * @param aPayload The payload to carry.
	 * @param aType The type of the payload.
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source from which this event originated.
	 */
	public PayloadBusEvent( P aPayload, Class<P> aType, Class<?> aPublisherType, ApplicationBus aSource ) {
		this( null, aPayload, aType, null, null, null, null, aPublisherType, aSource );

	}

	/**
	 * Constructs an event with the given Meta-Data.
	 *
	 * @param aPayload The payload to carry.
	 * @param aType The type of the payload.
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 * @param aSource The source from which this event originated.
	 */
	public PayloadBusEvent( P aPayload, Class<P> aType, EventMetaData aEventMetaData, ApplicationBus aSource ) {
		this( null, aPayload, aType, aEventMetaData, aSource );
	}

	/**
	 * Constructs an event with predefined values for the according properties
	 * retrieved from the caller's class.
	 *
	 * @param aPayload The payload to carry.
	 * @param aType The type of the payload.
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aSource The source from which this event originated.
	 */
	public PayloadBusEvent( P aPayload, Class<P> aType, String aChannel, ApplicationBus aSource ) {
		this( null, aPayload, aType, null, null, aChannel, null, null, aSource );
	}

	/**
	 * Constructs an event with the given values for the according properties.
	 *
	 * @param aPayload The payload to carry.
	 * @param aType The type of the payload.
	 * @param aChannel The channel for the {@link EventMetaData}.
	 * @param aPublisherType The publisher type for the {@link EventMetaData}.
	 * @param aSource The according source (origin).
	 */
	public PayloadBusEvent( P aPayload, Class<P> aType, String aChannel, Class<?> aPublisherType, ApplicationBus aSource ) {
		this( null, aPayload, aType, null, null, aChannel, null, aPublisherType, aSource );
	}

	/**
	 * Constructs an event with the given values for the according properties.
	 *
	 * @param aPayload The payload to carry.
	 * @param aType The type of the payload.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source from which this event originated.
	 */
	public PayloadBusEvent( P aPayload, Class<P> aType, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, ApplicationBus aSource ) {
		this( null, aPayload, aType, aAlias, aGroup, aChannel, aUid, aPublisherType, aSource );
	}

	/**
	 * Constructs an event with the given Meta-Data.
	 * 
	 * @param aPayload The payload to carry.
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 * @param aSource The source from which this event originated.
	 */
	public PayloadBusEvent( P aPayload, EventMetaData aEventMetaData, ApplicationBus aSource ) {
		this( null, aPayload, null, aEventMetaData, aSource );
	}

	/**
	 * Constructs an event with predefined values for the according properties
	 * retrieved from the caller's class.
	 * 
	 * @param aPayload The payload to carry.
	 * @param aChannel The channel name on which the event is receivable.
	 * @param aSource The source from which this event originated.
	 */
	public PayloadBusEvent( P aPayload, String aChannel, ApplicationBus aSource ) {
		this( null, aPayload, null, null, null, aChannel, null, null, aSource );
	}

	/**
	 * Constructs an event with the given values for the according properties.
	 * 
	 * @param aPayload The payload to carry.
	 * @param aChannel The channel for the {@link EventMetaData}.
	 * @param aPublisherType The publisher type for the {@link EventMetaData}.
	 * @param aSource The according source (origin).
	 */
	public PayloadBusEvent( P aPayload, String aChannel, Class<?> aPublisherType, ApplicationBus aSource ) {
		this( null, aPayload, null, null, null, aChannel, null, aPublisherType, aSource );
	}

	/**
	 * Constructs an event with the given values for the according properties.
	 * 
	 * @param aPayload The payload to carry.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source from which this event originated.
	 */
	public PayloadBusEvent( P aPayload, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, ApplicationBus aSource ) {
		this( null, aPayload, null, aAlias, aGroup, aChannel, aUid, aPublisherType, aSource );
	}

	/**
	 * Constructs an event with the given values for the according properties.
	 *
	 * @param aAction The action which this represents.
	 * @param aPayload The payload to carry.
	 * @param aType The type of the payload.
	 * @param aEventMetaData The Meta-Data to by supplied by the event.
	 * @param aSource The source from which this event originated.
	 */
	@SuppressWarnings("unchecked")
	public PayloadBusEvent( Enum<?> aAction, P aPayload, Class<P> aType, EventMetaData aEventMetaData, ApplicationBus aSource ) {
		super( aAction, aEventMetaData, aSource );
		_payload = aPayload;
		_type = aType != null ? aType : ( aPayload != null ? (Class<P>) aPayload.getClass() : null );
	}

	/**
	 * Constructs an event with the given values for the according properties.
	 *
	 * @param aAction The action which this represents.
	 * @param aPayload The payload to carry.
	 * @param aType The type of the payload.
	 * @param aAlias The alias property.
	 * @param aGroup The group property.
	 * @param aChannel The channel property.
	 * @param aUid The UID (Universal-TID) property.
	 * @param aPublisherType The type of the event publisher.
	 * @param aSource The source from which this event originated.
	 */
	@SuppressWarnings("unchecked")
	public PayloadBusEvent( Enum<?> aAction, P aPayload, Class<P> aType, String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, ApplicationBus aSource ) {
		super( aAction, aAlias, aGroup, aChannel, aUid, aPublisherType, aSource );
		_payload = aPayload;
		_type = aType != null ? aType : ( aPayload != null ? (Class<P>) aPayload.getClass() : null );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public P getPayload() {
		return _payload;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Class<P> getType() {
		return _type;
	}

	/**
	 * Creates builder to build {@link ApplicationBusEvent}.
	 *
	 * @param <P> The type of the payload.
	 * 
	 * @return created builder
	 */
	public static <P> Builder<P> builder() {
		return new Builder<P>();
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////
	/**
	 * Builder to build {@link PayloadBusEvent}.
	 * 
	 * @param <P> The type of the payload being carried by this event.
	 */
	public static class Builder<P> extends ApplicationBusEvent.Builder implements PayloadBuilder<P, Builder<P>>, TypeBuilder<P, Builder<P>> {

		private P payload;
		private Class<P> type;

		/**
		 * Instantiates a new builder.
		 */
		protected Builder() {}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder<P> withAction( Enum<?> aAction ) {
			action = aAction;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder<P> withAlias( String aAlias ) {
			eventMetaData.withAlias( aAlias );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder<P> withChannel( String aChannel ) {
			eventMetaData.withChannel( aChannel );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder<P> withGroup( String aGroup ) {
			eventMetaData.withGroup( aGroup );
			return this;
		}

		/**
		 * {@inheritDoc}
		 * 
		 * Merges all not-null values of the provided {@link EventMetaData}
		 * instance into this {@link Builder} instance.
		 * 
		 * @param aEventMetaData The {@link EventMetaData} instance to be merged
		 *        into this {@link Builder} instance.
		 * 
		 * @return This {@link Builder} instance as of the builder pattern.
		 */
		@Override
		public Builder<P> withMetaData( EventMetaData aEventMetaData ) {
			eventMetaData.withMetaData( aEventMetaData );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder<P> withPayload( P aPayload ) {
			payload = aPayload;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder<P> withPublisherType( Class<?> aPublisherType ) {
			eventMetaData.withPublisherType( aPublisherType );
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder<P> withSource( ApplicationBus aSource ) {
			source = aSource;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder<P> withType( Class<P> aType ) {
			type = aType;
			return this;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Builder<P> withUniversalId( String aUid ) {
			eventMetaData.withUniversalId( aUid );
			return this;
		}

		/**
		 * Builder method of the builder.
		 * 
		 * @return The built instance.
		 */
		@Override
		public PayloadBusEvent<P> build() {
			return new PayloadBusEvent<>( this );
		}
	}
}
