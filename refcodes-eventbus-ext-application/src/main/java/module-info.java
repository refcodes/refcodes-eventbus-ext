module org.refcodes.eventbus.ext.application {
	requires org.refcodes.component;
	requires org.refcodes.matcher;
	requires transitive org.refcodes.component.ext.observer;
	requires transitive org.refcodes.properties;
	requires transitive org.refcodes.eventbus;
	requires transitive org.refcodes.exception;
	requires transitive org.refcodes.mixin;
	requires transitive org.refcodes.observer;

	exports org.refcodes.eventbus.ext.application;
}
